$(document).ready(function () {
	// if ($(".homeSecondCoverSwiper").length) {
	// 	var swiper = new Swiper(".homeSecondCoverSwiper", {
	// 		autoplay: 20000,
	// 		speed: 500,
	// 		loop: true,
	// 		direction: "horizontal",
	// 		grabCursor: "true",
	// 		pagination: ".swiper-pagination",
	// 		paginationClickable: true
	// 	});
	// }

	if ($(".blogInsideOtherSwiper").length) {
		$(".blogInsideOtherSwiper .swiper-wrapper").find(".swiper-slide:odd").addClass("odd");
		$(".blogInsideOtherSwiper .swiper-wrapper").find(".swiper-slide:even").addClass("even");
		var blogInsideOtherSwiper = new Swiper(".blogInsideOtherSwiper", {
			slidesPerView: 2,
			slidesPerGroup: 2,
			effect: "slide",
			speed: 500,
			loop: true,
			direction: "horizontal",
			grabCursor: "true",
			// Navigation arrows
			nextButton: ".blogInsideOtherSwiperPrev",
			prevButton: ".blogInsideOtherSwiperNext",
			breakpoints: {
				767: {
					slidesPerView: 1,
					slidesPerGroup: 1
				}
			}
		});
	}

	var androidPopup = $(".androidPopup");
	var androidPopupClose = $(".androidPopupClose");

	function getMobileOperatingSystem() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;

		if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {

		}
		else if (userAgent.match(/Android/i)) {
			if (localStorage.getItem("popState") != "shown") {
				androidPopup.addClass("open");
				localStorage.setItem("popState", "shown");
			}
		}
		else {

		}
	}

	getMobileOperatingSystem();

	androidPopupClose.click(function (e) {
		e.preventDefault();
		androidPopup.removeClass("open");
	});

	var windowHeight = $(window).height();
	var popupHeight = windowHeight * 0.9;
	var mtPopupIn = $(".mt-popup-in");

	function popupSize() {
		if (mtPopupIn[0]) {
			mtPopupIn.css({height: popupHeight + "px"});
		}
		else {
		}
	}

	var headerSearch = $(".headerSearch");

	function searchSize() {
		var headerRightWidth = $(".headerRight").width();
		headerSearch.css("right", headerRightWidth);
	}

	var mtSearchSubmit = $("#mtSearchSubmit");
	var mtSearchInput = $("#mtSearchInput");
	mtSearchSubmit.click(function (e) {

		if (!headerSearch.hasClass("open")) {
			e.preventDefault();
			headerSearch.addClass("open");
			setTimeout(function () {
				mtSearchInput.focus();
			}, 300);
		}
		else if (headerSearch.hasClass("open") && mtSearchInput.val() == "") {
			e.preventDefault();
			headerSearch.removeClass("open");
		}
	});
	$("body").on("click", function (e) {
		if (!headerSearch.is(e.target) && headerSearch.has(e.target).length === 0 && headerSearch.has(e.target).length === 0) {
			headerSearch.removeClass("open");
		}
	});

	$(document).keyup(function (e) {
		if (e.keyCode === 27) { // esc
			headerSearch.removeClass("open");
		}
	});

	popupSize();

	$(document).ready(function () {
		setTimeout(function () {
			searchSize();
		}, 100);
	});

	$(window).resize(function () {
		popupSize();
		searchSize();
	});
});