$(document).ready(function () {
	if ($(".homeSecondCoverSwiper").length) {
		var swiper = new Swiper(".homeSecondCoverSwiper", {
			autoplay: 20000,
			speed: 500,
			loop: true,
			direction: "horizontal",
			grabCursor: "true",
			pagination: ".swiper-pagination",
			paginationClickable: true
		});
	}

	if ($(".blogInsideOtherSwiper").length) {
		$(".blogInsideOtherSwiper .swiper-wrapper").find(".swiper-slide:odd").addClass("odd");
		$(".blogInsideOtherSwiper .swiper-wrapper").find(".swiper-slide:even").addClass("even");
		var blogInsideOtherSwiper = new Swiper(".blogInsideOtherSwiper", {
			slidesPerView: 2,
			slidesPerGroup: 2,
			effect: "slide",
			speed: 500,
			loop: true,
			direction: "horizontal",
			grabCursor: "true",
			// Navigation arrows
			nextButton: ".blogInsideOtherSwiperPrev",
			prevButton: ".blogInsideOtherSwiperNext",
			breakpoints: {
				767: {
					slidesPerView: 1,
					slidesPerGroup: 1
				}
			}
		});
	}

	var androidPopup = $(".androidPopup");
	var androidPopupClose = $(".androidPopupClose");

	function getMobileOperatingSystem() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;

		if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {

		}
		else if (userAgent.match(/Android/i)) {
			if (localStorage.getItem("popState") != "shown") {
				androidPopup.addClass("open");
				localStorage.setItem("popState", "shown");
			}
		}
		else {

		}
	}

	getMobileOperatingSystem();

	androidPopupClose.click(function (e) {
		e.preventDefault();
		androidPopup.removeClass("open");
	});

	var windowHeight = $(window).height();
	var popupHeight = windowHeight * 0.9;
	var mtPopupIn = $(".mt-popup-in");

	function popupSize() {
		if (mtPopupIn[0]) {
			mtPopupIn.css({height: popupHeight + "px"});
		}
		else {
		}
	}

	var headerSearch = $(".headerSearch");

	var mtSearchSubmit = $("#mtSearchSubmit");
	var mtSearchInput = $("#mtSearchInput");
	mtSearchSubmit.click(function (e) {
		if (!headerSearch.hasClass("open")) {
			e.preventDefault();
			headerSearch.addClass("open");
			setTimeout(function () {
				mtSearchInput.focus();
			}, 300);
		}
		else if (headerSearch.hasClass("open") && mtSearchInput.val() == "") {
			e.preventDefault();
			headerSearch.removeClass("open");
		}
	});
	$("body").on("click", function (e) {
		if (!headerSearch.is(e.target) && headerSearch.has(e.target).length === 0 && headerSearch.has(e.target).length === 0) {
			headerSearch.removeClass("open");
		}
	});

	$(document).keyup(function (e) {
		if (e.keyCode === 27) { // esc
			headerSearch.removeClass("open");
		}
	});

	popupSize();

	$.adaptiveBackground.run({
		parent: ".adaptive-parent",
		exclude: ["rgb(0,0,0)", "rgb(220,220,220)"]
	});
	$(document).ready(function () {
		$("[data-adaptive-background]").on("ab-color-found", function () {
			$(this).parents(".adaptive-parent").adaptiveColor();
		});
	});

	function socialAffix() {
		var social = $(".blogInsideBodySocial");
		social.width($(".blogInsideBody").width() * 0.1666667);
		social.affix({
			offset: {
				top: function () {
					return (this.top = social.offset().top - social.outerHeight(true) + 100)
				},
				bottom: function () {
					return (this.bottom = $("footer").outerHeight(true) + 60)
				}
			}
		});
	}

	socialAffix();

	function searchSize() {
		headerSearch.css("right", $(".headerRight").width());
	}

	$(document).ready(function () {
		setTimeout(function () {
			searchSize();
		}, 100);
	});

	var resizeId;
	$(window).resize(function() {
		clearTimeout(resizeId);
		resizeId = setTimeout(function(){
			socialAffix();
			searchSize();
			popupSize();
		}, 100);
	});
});