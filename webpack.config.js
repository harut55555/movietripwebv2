"use strict";

var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const HOST = process.env.HOST || "0.0.0.0";
const PORT = process.env.PORT || "8888";

module.exports =  {
    context: __dirname,
    entry: {
      jsx: "./src/app.jsx",
      // css: "../_apps/" + APP_NAME + "/src/main.css",
      html: "./src/index.html"
    },

    output: {
      path: "./static",
      filename: "app.js",
    },
    module: {
      preLoaders: [
        //Eslint loader
        {test: /\.jsx?$/, exclude: /node_modules/, loader: "eslint-loader"},
      ],
      loaders: [
        {test: /\.html$/, loader: "file?name=[name].[ext]"},
        {test: /\.css$/, loader: "file?name=[name].[ext]"},
        {test: /\.jsx?$/, exclude: /node_modules/, loaders: ["babel-loader"]},
          // {
          //     test: /\.scss$/,
          //     loader: ExtractTextPlugin.extract('css!sass')
          // }
          // {
          //     test: /\.scss$/,
          //     loaders: ['style', 'css', 'sass']
          // }
      ],
    },
    // plugins: [
    //     new ExtractTextPlugin('./style.css', {
    //         allChunks: true
    //     })
    // ],
    resolve: {
      extensions: ['', '.js', '.jsx']
    },
    eslint: {
      configFile: './.eslintrc'
    },
    devServer: {
        contentBase: "./static/",
        // do not print bundle build stats
        noInfo: true,
        // enable HMR
        hot: true,
        // embed the webpack-dev-server runtime into the bundle
        inline: true,
        // serve index.html in place of 404 responses to allow HTML5 history
        historyApiFallback: true,
        port: PORT,
        host: HOST
    },
};