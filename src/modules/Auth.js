import { instanceOf } from 'prop-types';
import cookie from 'react-cookies'

class Auth {

  /**
   * Authenticate a user. Save a token string in Local Storage
   *
   * @param {string} token
   */
  static authenticateUser(data) {
console.log("DATA==",data);
    if (data !== undefined && data !== null) {
        // localStorage.setItem('access_token', data.access_token);
        // localStorage.setItem('Access_TokExpTime', data.Access_TokExpTime);
        // localStorage.setItem('refresh_token', data.refresh_token);
        // localStorage.setItem('Ref_TokExpTime', data.Ref_TokExpTime);

        Auth.setCookie('token', JSON.stringify(data));

        return true;
    }

    return false;
  }

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
  static isUserAuthenticated() {
    return (this.getRefreshToken() !== null && this.getRefreshTokenTimeToken() !== null && (new Date(this.getRefreshTokenTimeToken()) > new Date()));
  }

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  static deauthenticateUser() {

      cookie.remove('token',{ path: '/' });
    //
    // localStorage.removeItem('access_token');
    // localStorage.removeItem('Access_TokExpTime');
    // localStorage.removeItem('refresh_token');
    // localStorage.removeItem('Ref_TokExpTime');
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  static getRefreshToken() {
    return Auth.getItem('refresh_token');
  }

  static getAccessToken() {
      return Auth.getItem('access_token');
  }

  static getAccessTokenTimeToken() {
      return Auth.getItem('Access_TokExpTime');
  }

  static getRefreshTokenTimeToken() {
      return Auth.getItem('Ref_TokExpTime');
  }

  static getItem(name) {
      let cookie = Auth.getCookie('token');
      if (cookie !== '') {
        try {
            let json = JSON.parse(cookie);
            return json[name];
        } catch(e) {
            return null;
        }
      }

      return null;
  }

  static setCookie(cname, cvalue) {
      cookie.save(cname, cvalue, { path: '/' });
       // let exdays = 1;
       //  let d = new Date();
       //  d.setTime(d.getTime() + (exdays*24*60*60*1000));
       //  let expires = "expires="+ d.toUTCString();
       //  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    static getCookie(cname) {
        cookie.load(cname);
        // let name = cname + "=";
        // let decodedCookie = decodeURIComponent(document.cookie);
        // let ca = decodedCookie.split(';');
        // for(let i = 0; i <ca.length; i++) {
        //     let c = ca[i];
        //     while (c.charAt(0) === ' ') {
        //         c = c.substring(1);
        //     }
        //     if (c.indexOf(name) === 0) {
        //         return c.substring(name.length, c.length);
        //     }
        // }
        // return "";
    }
}

export default Auth;
