import React from 'react';
import Auth from '../modules/Auth';
import MyScenesAdd from '../components/MyScenesAdd.jsx';
import MyMovieAdd from '../components/MyMovieAdd.jsx';

class MyScenesAddPage extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {

                    "image":"",
                    "address":"",
                    "moviename":"",
                    "description":"",
                    "part":true


        };
        
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleImage=this.handleImage.bind(this);
    }
    
    handleChangeAddress(event)
    {
        this.setState({address: event.target.value});
        console.log(this.state.address); 
    }
    handleChangeDescription(event)
    {
        this.setState({description: event.target.value});
        console.log(this.state.description);
    }
    handleImage(event)
    {
        let input=event.target;
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#imga')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            console.log(input.files[0]);
        }
         this.setState({image: ""});
         console.log(this.state.image);
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);
        /*    const xhr = new XMLHttpRequest();
         let host = location.protocol + '//' + location.hostname + ':' + '3000/api/get_products';
         xhr.open('get', host);
         xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
         // set the authorization HTTP header
         xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
         xhr.responseType = 'json';
         xhr.addEventListener('load', () => {
         if (xhr.status === 200) {
         this.setState({
         scenes: xhr.response.list
         });
         }
         });
         xhr.send();*/
    }
    /**
     * Render the component.
     */
    render() {
        return (<MyScenesAdd address={this.state.address} part={this.state.part} description={this.state.description} handleChangeAddress={this.handleChangeAddress} handleChangeDescription={this.handleChangeDescription} handleImage={this.handleImage}/>);
    }

}

export default MyScenesAddPage;
