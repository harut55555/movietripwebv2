import React from 'react';
import Auth from '../modules/Auth';
import Locations from '../components/Locations.jsx';
import Location from '../models/Location';
import XhrService from '../services/XhrService'
import Config from '../Config'; 

class LocationsPage extends React.Component {

    xhr = null;
    static defaultProps = {
        path: Config.getPath().location
    };

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            getFrom: 1,
            getNum: Config.getLimit().locations,
            locations: []
        };

        this.handleMore = this.handleMore.bind(this);
        // this.handleImageLoad = this.handleImageLoad.bind(this);
        // this.handleImageError = this.handleImageError.bind(this);

        this.xhr = new XhrService();
    }

    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {

        // scroll to top
        window.scrollTo(0, 0);

        // add loader
        $('.scenesInside').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.state.getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.scenesInside').removeClass('loadingSection');
        });
    }

    // // img load
    // handleImageLoad(dom) {
    //     $(dom.currentTarget).removeClass('loading');
    // }
    //
    // // img load
    // handleImageError(dom) {
    //     $(dom.currentTarget).removeClass('loading');
    //     $(dom.currentTarget).addClass('notfound');
    // }

    // get data
   

    handleImageLoad(status, ref, location) {

        let locations = ref.state.locations;

        /*for(let i in locations) {
            if (locations[i].id === location.id) {
                locations[i].imgLoaded = status;
                locations[i].imgLoadedError = !status;

                break;
            }
        }*/

        // console.log(location.index);
        locations[location.index].imgLoaded = status;
        locations[location.index].imgLoadedError = !status;

        ref.setState({
            locations: locations
        })
    }

    // handle more data load
    handleMore() {
        // next page
        let getFrom = this.state.getFrom + 1;

        // loader class
        $('.moreButton').addClass('loading');

        // save page
        this.setState({
            getFrom: getFrom
        });

        // get data
        this.getData({path: this.props.path + '/' + getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.moreButton').removeClass('loading');
        });
    }
    
    /**
     * Render the component.
     */
    render() {
        return (<Locations locations={this.state.locations} handleMore={this.handleMore} />);
    }

}

export default LocationsPage;
