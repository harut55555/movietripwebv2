import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm.jsx';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx';
import { browserHistory } from 'react-router';

class LoginPage extends React.Component {

  /**
   * Class constructor.
   */
  xhr = null;
  static defaultProps = {
      login: Config.getPath().login,
      loginPath: Config.getPath().login
  };

  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    // set the initial component state
    this.state = {
      errors: {
          summary: ''
      },
      successMessage,
      user: {
      },
      email : '',
      password : '',
      isAuthenticated: Auth.isUserAuthenticated()
    };

    this.xhr = new XhrService();
    this.handleLogin = this.handleLogin.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.changeUser = this.changeUser.bind(this);
  }

  /**
   * Process the form.
   *
   * @param {object} event - the JavaScript event object
   */

  handleLogin(event) {
      event.preventDefault();

      // create a string for an HTTP body message
      let email = this.state.email;
      let password = this.state.password;
      let dataSend = { email: email, password: password };

      XhrService.postMtData({path: this.props.loginPath, auth: false}, dataSend, this, function(data, ref){
          if (data.status === 400) {
              let oldState = ref.state;
              oldState.errors.summary = data.response;
          }
          else if (data.status === 200) {
              if(Auth.authenticateUser(data.response)) {

                  ref.setState({
                      isAuthenticated: Auth.isUserAuthenticated()
                  });

                  // redirect to my
                  browserHistory.push('/my');
              }
              else {
                  let oldState = ref.state;
                  oldState.errors.summary = Config.getError(500);
              }
          }
      })
  }

  /**
   * Change the user object.
   *
   * @param {object} event - the JavaScript event object
   */
  changeUser(event) {
    event.preventDefault();
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  handleEmailChange(event) {
      this.setState({email: event.target.value});
  }

  handlePasswordChange(event) {
      this.setState({password: event.target.value});
  }

  /**
   * Render the component.
   */
  render() {
    return (
      <LoginForm
        isAuthenticated={this.state.isAuthenticated}
        handleEmailChange={this.handleEmailChange}
        handlePasswordChange={this.handlePasswordChange}
        onSubmit={this.handleLogin}
        errors={this.state.errors}
        successMessage={this.state.successMessage}
        user={this.state.user}
      />
    );
  }

}

LoginPage.contextTypes = {
  router: PropTypes.object.isRequired
};

export default LoginPage;
