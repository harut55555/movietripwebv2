import My from '../components/My.jsx';
import React, { PropTypes } from 'react';


class MyPage extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        
    }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        /*const xhr = new XMLHttpRequest();
         let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
         xhr.open('get', host);
         xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
         // set the authorization HTTP header
         xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
         xhr.responseType = 'json';
         xhr.addEventListener('load', () => {
         if (xhr.status === 200) {
         this.setState({
         location: xhr.response.list
         });
         }
         });
         xhr.send();*/
    }

    /**
     * Render the component.
     */
    render() {
        return (<My children={this.props.children} />);
    }

}

export default MyPage;
