import React from 'react';
import Auth from '../modules/Auth';
import Scenes from '../components/Scenes.jsx';
import Scene from '../models/Scene';
import XhrService from '../services/XhrService'
import Config from '../Config';

class ScenesPage extends React.Component {

    xhr = null;
    static defaultProps = {
        path: Config.getPath().scene
    };

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            getFrom: 1,
            getNum: Config.getLimit().scenes,
            scenes: []
        };

        this.handleMore = this.handleMore.bind(this);
        // this.handleImageLoad = this.handleImageLoad.bind(this);
        // this.handleImageError = this.handleImageError.bind(this);

        this.xhr = new XhrService();
    }

    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {

        // scroll to top
        window.scrollTo(0, 0);

        // add loader
        $('.scenesInside').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.state.getFrom + '/' + this.state.getNum, auth: false},function(){
            $('.scenesInside').removeClass('loadingSection');
        });
    }

    // // img load
    // handleImageLoad(dom) {
    //     $(dom.currentTarget).removeClass('loading');
    // }
    //
    // // img load
    // handleImageError(dom) {
    //     $(dom.currentTarget).removeClass('loading');
    //     $(dom.currentTarget).addClass('notfound');
    // }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                let scenes = [];
                for(let i in data.response) {
                    let currentScene = new Scene();
                    let curData = data.response[i];
                    curData['index'] = parseInt(ref.state.scenes.length) + parseInt(i);
                    currentScene.initImageLoad(curData, ref, ref.handleImageLoad);
                    scenes.push(currentScene);
                }
                ref.setState({
                    scenes: ref.state.scenes.concat(scenes)
                });
            }
        })
    }

    handleImageLoad(status, ref, scene) {

        let scenes = ref.state.scenes;

        /*for(let i in scenes) {
         if (scenes[i].id === scene.id) {
         scenes[i].imgLoaded = status;
         scenes[i].imgLoadedError = !status;

         break;
         }
         }*/

        // console.log(scene.index);
        scenes[scene.index].imgLoaded = status;
        scenes[scene.index].imgLoadedError = !status;

        ref.setState({
            scenes: scenes
        })
    }

    // handle more data load
    handleMore() {
        // next page
        let getFrom = this.state.getFrom + 1;

        // loader class
        $('.moreButton').addClass('loading');

        // save page
        this.setState({
            getFrom: getFrom
        });

        // get data
        this.getData({path: this.props.path + '/' + getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.moreButton').removeClass('loading');
        });
    }

    /**
     * Render the component.
     */
    render() {
        return (<Scenes scenes={this.state.scenes} handleMore={this.handleMore} />);
    }

}

export default ScenesPage;
