import React from 'react';
import Auth from '../modules/Auth';
import Home from '../components/Home.jsx';
import Config from '../Config.jsx';
import XhrService from '../services/XhrService.jsx';
import Slider from '../models/Slider';
import Movie from '../models/Movie';
import Location from '../models/Location';
import Trip from '../models/Trip';
import Scene from '../models/Scene';


class HomeBoard extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().home,
        moviePath: Config.getPath().movie,
        locationPath: Config.getPath().location
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            home:{
                topSlider: [],
                topLatestBlock: {
                    movies: [],
                    locations: []
                },
                middleBlockLocations: [],
                bottomUpMovies: [],
                bottomTrips: [],
                bottomLocations: {
                    First: [],
                    Second: []
                },
                promoBlockLocation: {},
                promoBlockSceneFirst: {},
                promoBlockSceneSecond: {},
                middleBlockText:{},
                middleBlockSceneFirst: {},
                middleBlockSceneSecond: {}
            }
        };

        this.xhr = new XhrService();

        this.handleImageError = this.handleImageError.bind(this);
        this.handleImageLoad = this.handleImageLoad.bind(this);
    }

    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        // add loader
        $('.homeSecondCover').addClass('loadingSection');
        $('.homePopular').addClass('loadingSection');
        $('.homeMovies').addClass('loadingSection');
        $('.homeSpecialLocations').addClass('loadingSection');
        $('.homePopularSecond').addClass('loadingSection');

        // get data
        this.getData({path: this.props.path + '/GetSlider/2', auth: false,_call: 'GetSlider'}, function(){
            $('.homeSecondCover').removeClass('loadingSection');
        });
        this.getData({path: this.props.locationPath + '/1/5', auth: false,_call: 'GetLatestLoc'}, function(){
            $('.homePopularSecond').removeClass('loadingSection');
        });
        this.getData({path: this.props.path + '/GetRandLoc/5', auth: false, _call: 'GetRandLoc'}, function(){
            $('.homePopular').removeClass('loadingSection');
        });
        this.getData({path: this.props.path + '/GetRandScene/4', auth: false, _call: 'GetRandScene'}, function(){
            $('.homePopular').removeClass('loadingSection');
        });
        this.getData({path: this.props.moviePath + '/1/8', auth: false, _call: 'GetLatestMovie'}, function(){
            $('.homeMovies').removeClass('loadingSection');
        });;
        this.getData({path: this.props.locationPath + '/1/10?type=' + Config.getLocationType('restourant'), auth: false,_call: 'GetMiddleLocations'}, function(){
            $('.homePopularSecond').removeClass('loadingSection');
        });
        this.getData({path: this.props.path + '/GetRandTrip/4', auth: false, _call: 'GetRandTrip'}, function(){
            $('.homeSpecialLocations').removeClass('loadingSection');
        });
        this.getData({path: this.props.path + '/GetInformer', auth: false, _call: 'GetInformer'}, function(){
            $('.homeSpecialLocations').removeClass('loadingSection');
        });
    }

    componentDidUpdate() {
        if ($(".homeSecondCoverSwiper").length) {
            let swiper = new Swiper(".homeSecondCoverSwiper", {
                autoplay: 20000,
                speed: 500,
                loop: true,
                direction: "horizontal",
                grabCursor: "true",
                pagination: ".swiper-pagination",
                paginationClickable: true
            });
        }
    }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback();

            // set state
            if (data.status === 200) {

                let myArray = [];

                switch (props._call) {
                    case 'GetSlider':
                        let topSlider = data.response;
                        myArray = [];
                        for (let i in topSlider) {
                            myArray.push(new Slider(topSlider[i]));
                        }
                        ref.state.home.topSlider = myArray;
                        break;
                    case 'GetLatestLoc':
                        let location = data.response;
                        myArray=[];
                        for (let i in location) {
                            myArray.push(new Location(location[i]));
                        }
                        ref.state.home.topLatestBlock.locations = myArray;
                        break;
                    case 'GetRandLoc':
                        let locations = data.response;
                        myArray=[];
                        for (let i in locations) {
                            myArray.push(new Location(locations[i]));
                        }
                        ref.state.home.bottomLocations.First = [myArray[1], myArray[2]];
                        ref.state.home.bottomLocations.Second = [myArray[3], myArray[4]];
                        ref.state.home.promoBlockLocation = myArray[0];
                        break;
                    case 'GetRandScene':
                        let scenes = data.response;
                        myArray=[];
                        for (let i in scenes) {
                            myArray.push(new Scene(scenes[i]));
                        }
                        ref.state.home.promoBlockSceneFirst = myArray[0];
                        ref.state.home.promoBlockSceneSecond = myArray[1];
                        ref.state.home.middleBlockSceneFirst = myArray[2];
                        ref.state.home.middleBlockSceneSecond = myArray[3];
                        break;
                    case 'GetMiddleLocations':
                        let middleLocations = data.response;
                        myArray=[];
                        for (let i in middleLocations) {
                            myArray.push(new Location(middleLocations[i]));
                        }
                        ref.state.home.middleBlockLocations = myArray;
                        break;
                    case 'GetLatestMovie':
                        let movies = [];
                        for(let i in data.response) {
                            let currentMovie = new Movie(data.response[i]);
                            movies.push(currentMovie);
                        }
                        ref.state.home.topLatestBlock.movies = movies;
                        break;
                    case 'GetRandTrip':
                        let trips = data.response;
                        myArray=[];
                        for (let i in trips) {
                            myArray.push(new Trip(trips[i]));
                        }
                        ref.state.home.bottomTrips = myArray;
                        break;
                    case 'GetInformer':
                        let informerdata = data.response;
                        ref.state.home.middleBlockText = informerdata;
                        break;
                }

                // set state
                ref.setState({
                    home:ref.state.home
                });
            }
        })
    }

    // img load
    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    // img load
    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    /**
     * Render the component.
     */
    render() {
        return (<Home home={this.state.home}/>);
    }

}

export default HomeBoard;
