/**
 * Created by Arsen on 23.02.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import BlogInside from '../components/BlogInside.jsx';
import Blog from '../models/Blog.js'
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx'

class BlogInsidePage extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().scene
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            blog: {}
        };
        this.xhr = new XhrService();
    }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        // add loader
        $('.sceneInsideOther').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.props.params.id, auth: false}, function(){
            $('.sceneInsideOther').removeClass('loadingSection');
        });
    }
    handleImageLoad(status, ref) {

        let blog = ref.state.blog;

        /*for(let i in scenes) {
         if (scenes[i].id === scene.id) {
         scenes[i].imgLoaded = status;
         scenes[i].imgLoadedError = !status;

         break;
         }
         }*/

        // console.log(scene.index);
        blog.imgLoaded = status;
        blog.imgLoadedError = !status;

        ref.setState({
            blog: blog
        })
    }
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                let currentBlog = new Blog();
                let curData = data.response;
                currentBlog.initImageLoad(curData, ref, ref.handleImageLoad);

                ref.setState({
                    blog: currentBlog
                });
            }
        })
    }
    /**
     * Render the component.
     */
    render() {
        return (<BlogInside  blog={this.state. blog} id={this.props.params.id} />);
    }

}

export default BlogInsidePage;