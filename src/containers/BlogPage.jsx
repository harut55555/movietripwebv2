import React from 'react';
import Auth from '../modules/Auth';
import Blog from '../components/Blog.jsx';
import XhrService from '../services/XhrService'
import Config from '../Config';


class BlogPage extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().blog
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);
        this.state = {
            getPage: 1,
            getNum: Config.getLimit().blogs,
            blogs: []
        };

        this.handleMore = this.handleMore.bind(this);
        this.handleImageLoad = this.handleImageLoad.bind(this);
        this.handleImageError = this.handleImageError.bind(this);

        this.xhr = new XhrService();
       
    }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        // add loader
        $('.blogsInside').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.state.getPage + '/' + this.state.getNum, auth: false}, function(){
            $('.blogsInside').removeClass('loadingSection');
        });
    }

    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    // img load
    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, callObj){

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                callObj.setState({
                    getFrom: callObj.state.getPage,
                    getNum: Config.getLimit().blogs,
                    blogs: data.response
                });
            }
        })
    }

    // handle more data load
    handleMore() {
        // next page
        let getPage = this.state.getPage + 1;

        // loader class
        $('.moreButton').addClass('loading');

        // save page
        this.setState({
            getFrom: getPage
        });

        // get data
        this.getData({path: this.props.path + '/' + getPage + '/' + this.state.getNum, auth: false}, function(){
            $('.moreButton').removeClass('loading');
        });
    }
    /**
     * Render the component.
     */
    render() {
        
        return (<Blog blogs={this.state.blogs} handleMore={this.handleMore} handleImageLoad={this.handleImageLoad} handleImageError={this.handleImageError} />);
    }

}

export default BlogPage;
