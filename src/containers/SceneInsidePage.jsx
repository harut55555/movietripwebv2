import React from 'react';
import Auth from '../modules/Auth';
import SceneInside from '../components/SceneInside.jsx';
import Scene from '../models/Scene.js';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx'
class SceneInsidePage extends React.Component {

    xhr = null;
    static defaultProps = {
        path: Config.getPath().scene
    };
    /**
     * Class constructor.
     */
    constructor(props) {

        super(props);

        this.state = {
            scene:{},
            relatedScenes:[],
            otherScenes:[]
        };
        this.onFbClick = this.onFbClick.bind(this);

        this.xhr = new XhrService();
    }

    // on props change
    componentWillReceiveProps(nextProps) {

        this.setState({
            scene:{},
            relatedScenes:[],
            otherScenes:[]
        });

        this.loadPage(nextProps);
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        this.loadPage(this.props);
    }

    // load page
    loadPage(props){

        // scroll to top
        window.scrollTo(0, 0);

        // add loader
        $('.sceneInsideOther').addClass('loadingSection');
        // get data
        this.getData({path: props.path + '/' + props.params.id, auth: false}, 'Scene', function(ref,data){
            $('.sceneInsideOther').removeClass('loadingSection');
        });

        $('.relatedWrap').addClass('loadingSection');
        // get related scenes
        this.getData({path: props.path + '/GetRelated/'  + props.params.id + '/4', auth: false}, 'RelatedScenes', function(){
            $('.relatedWrap').removeClass('loadingSection');
        });

        // get other scenes data
        this.getData({path: props.path + '/GetOtherScenes/'  + props.params.id, auth: false}, 'OtherScenes', function(){

        });
    }

    handleImageLoad(status, ref) {

        let scene = ref.state.scene;
        scene.imgLoaded = status;
        scene.imgLoadedError = !status;

        ref.setState({
            scene: scene
        })
    }

    handleImageLoad(status, ref) {

        let scene = ref.state.scene;

        scene.imgLoaded = status;
        scene.imgLoadedError = !status;

        ref.setState({
            scene: scene
        })
    }

    handleImageLoadRelated(status, ref, scene) {

        let scenes = ref.state.relatedScenes;

        // console.log(location.index);
        scenes[scene.index].imgLoaded = status;
        scenes[scene.index].imgLoadedError = !status;

        //
        ref.setState({
            relatedScenes: scenes
        })
    }

    handleImageLoadOther(status, ref, scene) {

        let scenes = ref.state.otherScenes;

        // console.log(location.index);
        scenes[scene.index].imgLoaded = status;
        scenes[scene.index].imgLoadedError = !status;

        //
        ref.setState({
            otherScenes: scenes
        })
    }

    // get data
    getData(props, _call, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback(ref,data);

            // set state
            if (data.status === 200) {
                switch(_call) {
                    case 'Scene':
                        let currentScene = new Scene();
                        let curData = data.response;
                        currentScene.initImageLoad(curData, ref, ref.handleImageLoad);
                        ref.setState({
                            scene: currentScene
                        });
                        break;
                    case 'RelatedScenes':
                        let related = [];
                        for(let i in data.response) {
                            let curData = data.response[i];
                            let currentSc = new Scene();
                            curData['index'] = parseInt(ref.state.relatedScenes.length) + parseInt(i);
                            currentSc.initImageLoad(curData, ref, ref.handleImageLoadRelated);
                            related.push(currentSc);
                        }
                        ref.setState({
                            relatedScenes: related
                        });
                        break;
                    case 'OtherScenes':
                        let other = [];
                        for(let i in data.response) {
                            let curData = data.response[i];
                            let currentSc = new Scene();
                            curData['index'] = parseInt(ref.state.otherScenes.length) + parseInt(i);
                            currentSc.initImageLoad(curData, ref, ref.handleImageLoadOther);
                            other.push(currentSc);
                        }
                        ref.setState({
                            otherScenes: other
                        });
                        break;
                }
            }
        })
    }

    onFbClick(){
        window.open('http://www.facebook.com/sharer.php?t=scene', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    }

    /**
     * Render the component.
     */

    render() {
        
        return (<SceneInside  otherScenes={this.state.otherScenes} relatedScenes={this.state.relatedScenes} onFbClick={this.onFbClick} scene={this.state.scene} />);
    }

}



export default SceneInsidePage;
