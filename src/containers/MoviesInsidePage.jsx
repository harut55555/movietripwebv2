/**
 * Created by Arsen on 20.02.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import MoviesInside from '../components/MoviesInside.jsx';
import Movie from '../models/Movie.js';
import Scene from '../models/Scene.js';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx'

class MoviesInsidePage extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().movie
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            movie: {},
            relatedScenes:[]
        };
        this.xhr = new XhrService();
        this.onFbClick = this.onFbClick.bind(this);
    }


    // on props change
    componentWillReceiveProps(nextProps) {
        this.setState({
            location: {},
            relatedScenes:[]
        });
        this.loadPage(nextProps);
    }

    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        this.loadPage(this.props);
    }

    loadPage(props) {
        window.scrollTo(0, 0);
        // add loader
        $('.sceneInsideOther').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + props.params.id, auth: false}, 'Movie', function(){
            $('.sceneInsideOther').removeClass('loadingSection');
        });
        $('.relatedWrap').addClass('loadingSection');
        // get data
        this.getData({path: props.path + '/GetMovieScenes/'+props.params.id, auth: false}, 'RelatedScenes', function(){
            $('.relatedWrap').removeClass('loadingSection');
        });

    }

    getData(props, _call, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){
            // job is done
            callback(ref, data);
            // set state
            if (data.status === 200) {

                switch(_call) {
                    case 'Movie':
                        let currentMovie = new Movie();
                        let curData = data.response;
                        currentMovie.initImageLoad(curData, ref, ref.handleImageLoadMain);

                        ref.setState({
                            movie: currentMovie
                        });
                        break;
                    case 'RelatedScenes':
                        let related = [];
                        for(let i in data.response) {
                            let currentScene = new Scene();
                            let curData = data.response[i];
                            curData['index'] = parseInt(ref.state.relatedScenes.length) + parseInt(i);
                            currentScene.initImageLoad(curData, ref, ref.handleImageLoadRelatedScenes);
                            related.push(currentScene);
                        }
                        ref.setState({
                            relatedScenes: related
                        });
                        break;
                }
            }
        })
    }

    handleImageLoadMain(status, ref) {
        let movie = ref.state.movie;

        movie.imgLoaded = status;
        movie.imgLoadedError = !status;

        ref.setState({
            movie: movie
        })
    }

    handleImageLoadRelatedScenes(status, ref, scene) {
        let scenes = ref.state.relatedScenes;
        for(let i in scenes) {
             if (scenes[i].id === scene.id) {
                 scenes[i].imgLoaded = status;
                 scenes[i].imgLoadedError = !status;
                 break;
             }
         }

        ref.setState({
            relatedScenes: scenes
        })
    }

    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    // img load
    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    onFbClick(){
        window.open('http://www.facebook.com/sharer.php?t=movie', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    }
    /**
     * Render the component.
     */
    render() {
        return (<MoviesInside movie={this.state.movie} relatedScenes={this.state.relatedScenes} onFbClick={this.onFbClick} handleImageLoad={this.handleImageLoad} handleImageError={this.handleImageError} />);
    }

}

export default MoviesInsidePage;
