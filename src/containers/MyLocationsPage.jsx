
import React from 'react';
import Auth from '../modules/Auth';
import MyLocation from '../components/MyLocation.jsx';


class MyLocationsPage extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            mylocations: [
                {   
                    "image":"/img/frontend/img-2.png",
                     "h2":"Rejected",
                     "h3":"The Packet Hotel",
                     "h4":"The Packet Hotel",
                     "id":"1234"
                }
            ]
        };
        this.addClick= this.addClick.bind(this);
        this.edithandleClick = this.edithandleClick.bind(this);
        this.deletehandleClick = this.deletehandleClick.bind(this);
    }
    addClick()
    {
        console.log("add");

    }
    edithandleClick()
    {
        console.log("edit");
    }
    deletehandleClick()
    {

        console.log("delete");
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);
        /*    const xhr = new XMLHttpRequest();
         let host = location.protocol + '//' + location.hostname + ':' + '3000/api/get_products';
         xhr.open('get', host);
         xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
         // set the authorization HTTP header
         xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
         xhr.responseType = 'json';
         xhr.addEventListener('load', () => {
         if (xhr.status === 200) {
         this.setState({
         scenes: xhr.response.list
         });
         }
         });
         xhr.send();*/
    }
    /**
     * Render the component.
     */
    render() {
        return (<MyLocation mylocations={this.state.mylocations} addClick={this.addClick} edithandleClick={this.edithandleClick} deletehandleClick={this.deletehandleClick}/>);
    }

}

export default MyLocationsPage;
