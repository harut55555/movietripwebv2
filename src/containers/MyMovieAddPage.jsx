import React from 'react';
import Auth from '../modules/Auth';
import MyMovieAdd from '../components/MyMovieAdd.jsx';


class MyMovieAddPage extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
                    "image":"/img/frontend/movie-1.jpg",
                    "name":"",
                    "description":"",
                    "part":this.props.part
        };
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleImage=this.handleImage.bind(this);
       
    }
   
    
    handleChangeName(event)
    {
        this.setState({name: event.target.value});
        console.log(this.state.name);
    }
    handleChangeDescription(event)
    {
        this.setState({description: event.target.value});
        console.log(this.state.description);
    }
    handleImage(event)
    {
        let input=event.target;
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#imga')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            console.log(input.files[0]);
        }
        this.setState({image: ""});
        console.log(this.state.image);
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);
        /*    const xhr = new XMLHttpRequest();
         let host = location.protocol + '//' + location.hostname + ':' + '3000/api/get_products';
         xhr.open('get', host);
         xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
         // set the authorization HTTP header
         xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
         xhr.responseType = 'json';
         xhr.addEventListener('load', () => {
         if (xhr.status === 200) {
         this.setState({
         scenes: xhr.response.list
         });
         }
         });
         xhr.send();*/
    }
    /**
     * Render the component.
     */
    render() {
        return (<MyMovieAdd name={this.state.name} description={this.state.description} part={this.state.part} handleChangeName={this.handleChangeName} handleChangeDescription={this.handleChangeDescription} handleImage={this.handleImage}/>);
    }

}

export default MyMovieAddPage;
