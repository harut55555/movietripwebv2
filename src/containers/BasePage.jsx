/**
 * Created by Arsen on 13.03.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import Base from '../components/Base.jsx';
import User from '../models/User';
import XhrService from '../services/XhrService'
import Config from '../Config';
import ImgService from '../services/ImgService';

class BasePage extends React.Component {

    /**
     * Class constructor.
     */
    xhr = null;
    static defaultProps = {
        path: Config.getPath().home,
        loginPath: Config.getPath().login,
        fbLoginPath: Config.getPath().fbLogin
    };

    constructor(props) {
        super(props);

        this.state = {
            statistics: [],
            user: {
                email: '',
                password: ''
            },
            errors:{
                summary: ''
            },
            email : '',
            password : '',
            isAuthenticated: Auth.isUserAuthenticated()
        };
        this.onSearch = this.onSearch.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
        this.xhr = new XhrService();
        // XhrService.postMtData({path: "/api/v1/auth/VerifyEmail"},{email:"harut555551@mail.ru"}, null,function(data, obj){
        //     console.log(data.response);
        // })
    }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        let newUser = new User();
        newUser.init({id:0, name:"Harut"});

        this.setState({
            user: newUser
        });

        classList.add("anotherclass");

        $('.moviesInside').addClass('loadingSection');
        // get data
        this.getDataCount({path: this.props.path + '/getcount', auth: false}, function(){
            $('.moviesInside').removeClass('loadingSection');
        });
        /*const xhr = new XMLHttpRequest();
         let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
         xhr.open('get', host);
         xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
         // set the authorization HTTP header
         xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);n
         xhr.responseType = 'json';
         xhr.addEventListener('load', () => {
         if (xhr.status === 200) {
         this.setState({
            location: xhr.response.list
         });
         }
         });
         xhr.send();*/
    }

    // update state on every children change
    componentWillReceiveProps(){
        this.setState({
            isAuthenticated: Auth.isUserAuthenticated()
        });
    }

    getDataCount(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function(data, ref) {
            // job is done
            callback();
            if (data.status === 200) {
                let statistics = [];
                for(let i in data.response) {
                    let currentSc = data.response[i];
                    statistics.push(currentSc);
                }
                ref.setState({

                    statistics: ref.state.statistics.concat(statistics)
                });


            }
        });
    }

    responseFacebook(res){

        if (typeof res !== undefined && res.accessToken !== undefined && res.accessToken !== null && res.accessToken !== '') {

                let access_token = res.accessToken;
                let dataSend = { access_token: access_token };

                let params = {path: this.props.fbLoginPath, auth: false};

                this.authenticateUser(access_token, params);
        }

    }

    handleLogin(event) {
        event.preventDefault();

        // create a string for an HTTP body message
        let email = this.state.email;
        let password = this.state.password;
        let dataSend = { email: email, password: password };

        let params = {path: this.props.loginPath, auth: false};

        // authenticate
        this.authenticateUser(dataSend, params);
    }

    authenticateUser(dataSend, params) {
        XhrService.postMtData(params, dataSend, this, function(data, ref){
            if (data.status === 400) {
                let oldState = ref.state;
                oldState.errors.summary = data.response;
            }
            else if (data.status === 200) {
                if(Auth.authenticateUser(data.response)) {

                    ref.setState({
                        isAuthenticated: true
                    });
                }
                else {
                    let oldState = ref.state;
                    oldState.errors.summary = Config.getError(500);
                }
            }
        })
    }

    handleLogout(event){
        event.preventDefault();
        Auth.deauthenticateUser();

        this.setState({
            isAuthenticated: false
        });
    }

    onSearch(){
        const inputValue = document.getElementById("mtSearchInput1").value;

        console.log(inputValue);
    }


    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    /**
     * Render the component.
     */
    render() {
       
        return (<Base children={this.props.children}
                      isAuthenticated={this.state.isAuthenticated}
                      handleEmailChange={this.handleEmailChange}
                      handlePasswordChange={this.handlePasswordChange}
                      onSearch={this.onSearch}
                      handleLogin={this.handleLogin}
                      handleLogout={this.handleLogout}
                      statistics={this.state.statistics}
                      user={this.state.user}
                      responseFacebook={this.responseFacebook}
        />);
    }

}

export default BasePage;