import React from 'react';
import Auth from '../modules/Auth';
import LocationInside from '../components/LocationInside.jsx';
import Location from '../models/Location.js'
import Scene from '../models/Scene.js'
import XhrService from '../services/XhrService'
import Config from '../Config';

class LocationInsidePage extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().location
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            location: {},
            relatedScenes:[],
            relatedMovies:[]
        };

        this.handleImageLoad = this.handleImageLoad.bind(this);
        this.handleImageError = this.handleImageError.bind(this);
        this.xhr = new XhrService();
    }

    // on props change
    componentWillReceiveProps(nextProps) {
        this.setState({
            location: {},
            relatedScenes:[],
            relatedMovies:[]
        });
        this.loadPage(nextProps);
    }
    
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        this.loadPage(this.props);
    }

    loadPage(props){

        // scroll to top
        window.scrollTo(0, 0);
        // add loader
        $('.locationInsideTopInfoAddress').addClass('loadingSection');
        // get data
        this.getData({path: props.path + '/'+ props.params.id, auth: false}, 'Location', function(){
            $('.locationInsideTopInfoAddress').removeClass('loadingSection');
        });

        $('.relatedWrap').addClass('loadingSection');
        // get data
        this.getData({path: props.path + '/GetRelateLocScenes/'+ props.params.id + '/4', auth: false}, 'RelatedScenes', function(){
            $('.relatedWrap').removeClass('loadingSection');
        });
    }

    // get data
    getData(props, _call, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){
            // job is done
            callback(ref,data);

            // set state
            if (data.status === 200) {
                switch(_call) {
                    case 'Location':
                        let currentLocation = new Location();
                        let curData = data.response;
                        currentLocation.initImageLoad(curData, ref, ref.handleMainImageLoad);
                        ref.setState({
                            location: currentLocation
                        });
                        break;
                    case 'RelatedScenes':
                        let related = [];
                        for(let i in data.response) {
                            let curData = data.response[i];
                            let currentSc = new Scene();
                            curData['index'] = parseInt(ref.state.relatedScenes.length) + parseInt(i);
                            currentSc.initImageLoad(curData, ref, ref.handleImageLoadRelated);
                            related.push(currentSc);
                        }
                        ref.setState({
                            relatedScenes: related
                        });
                        break;
                    case 'RelatedMovies':
                        let movies = [];
                        for(let i in data.response) {
                            let curData = data.response[i];
                            let currentMv = new Movie();
                            curData['index'] = parseInt(ref.state.relatedMovies.length) + parseInt(i);
                            currentMv.initImageLoad(curData, ref, ref.handleImageLoadRelatedMovies);
                            related.push(currentMv);
                        }
                        ref.setState({
                            relatedMovies: movies
                        });
                        break;
                }
            }
        })
        
    }

    handleMainImageLoad(status, ref) {

        let location = ref.state.location;

        location.imgLoaded = status;
        location.imgLoadedError = !status;
        ref.setState({
            location: location
        })
    }

    handleImageLoadRelated(status, ref, scene) {

        let scenes = ref.state.relatedScenes;

        // console.log(location.index);
        scenes[scene.index].imgLoaded = status;
        scenes[scene.index].imgLoadedError = !status;

        //
        ref.setState({
            relatedScenes: scenes
        })
    }

    handleImageLoadRelatedMovies(status, ref, scene) {

        let scenes = ref.state.relatedScenes;

        // console.log(location.index);
        scenes[scene.index].imgLoaded = status;
        scenes[scene.index].imgLoadedError = !status;

        //
        ref.setState({
            relatedScenes: scenes
        })
    }

    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    onFbClick(){
        window.open('http://www.facebook.com/sharer.php?t=location', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    }
    /**
     * Render the component.
     */
    render() {
   
        return (<LocationInside location={this.state.location} relatedScenes={this.state.relatedScenes} relatedMovies={this.state.relatedMovies} handleImageLoad={this.handleImageLoad} handleImageError={this.handleImageError}  onFbClick={this.onFbClick}/>);
    }

}

export default LocationInsidePage;
