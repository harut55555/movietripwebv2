/**
 * Created by Arsen on 06.03.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import TripView from '../components/Trip.jsx';
import Trip from '../models/Trip.js'
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx'


class TripPage extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().trip
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            trip:{}
        };
       

        this.xhr = new XhrService();
    }


    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        // add loader
        $('.sceneInsideOther').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.props.params.id, auth: false}, function(){
            $('.sceneInsideOther').removeClass('loadingSection');
        });
    }

    handleImageLoad(status, ref) {

        let trip = ref.state.trip;

        /*for(let i in scenes) {
         if (scenes[i].id === scene.id) {
         scenes[i].imgLoaded = status;
         scenes[i].imgLoadedError = !status;

         break;
         }
         }*/

        // console.log(scene.index);
        trip.imgLoaded = status;
        trip.imgLoadedError = !status;

        ref.setState({
            trip: trip
        })
    }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                let currentTrip = new Trip();
                let curData = data.response;
                currentTrip.initImageLoad(curData, ref, ref.handleImageLoad);

                ref.setState({
                    trip: currentTrip
                });
            }
        })
    }


    /**
     * Render the component.
     */
    render() {
       
        return (<TripView trip={this.state.trip} id={this.props.params.id}/>);
    }
}

export default TripPage;