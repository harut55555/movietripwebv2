/**
 * Created by Arsen on 23.02.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import TripsComponent from '../components/Trips.jsx';
import Config from '../Config.jsx';
import XhrService from '../services/XhrService.jsx';

class TripsPage extends React.Component {

    /**
     * Class constructor.
     */
    xhr = null;
    static defaultProps = {
        path: Config.getPath().trip
    };

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            getFrom: 1,
            getNum: Config.getLimit().trips,
            trips: []
        };

        this.handleMore = this.handleMore.bind(this);
        this.handleImageLoad = this.handleImageLoad.bind(this);
        this.handleImageError = this.handleImageError.bind(this);

        this.xhr = new XhrService();
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {

        // scroll to top
        window.scrollTo(0, 0);

        // add loader
        $('.tripsInside').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.state.getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.tripsInside').removeClass('loadingSection');
        });
    }

    // img load
    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    // img load
    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, callObj){

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                callObj.setState({
                    trips: callObj.state.trips.concat(data.response)
                });
            }
        })
    }

    // handle more data load
    handleMore() {
        // next page
        let getFrom = this.state.getFrom + 1;

        // loader class
        $('.moreButton').addClass('loading');

        // save page
        this.setState({
            getFrom: getFrom
        });

        // get data
        this.getData({path: this.props.path + '/' + getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.moreButton').removeClass('loading');
        });
    }

    render() { 
        return (<TripsComponent trips={this.state.trips} handleMore={this.handleMore} handleImageLoad={this.handleImageLoad} handleImageError={this.handleImageError}  />);
    }

}

export default TripsPage;