/**
 * Created by Arsen on 16.02.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import MoviesComponent from '../components/Movies.jsx';
import Movie from '../models/Movie';
import XhrService from '../services/XhrService'
import Config from '../Config';
import ImgService from '../services/ImgService';

class MoviesPage extends React.Component {

    xhr = null;
    static defaultProps = {
        path: Config.getPath().movie
    };

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            getFrom: 1,
            getNum: Config.getLimit().movies,
            movies: []
        };

        this.handleMore = this.handleMore.bind(this);
        this.handleImageLoad = this.handleImageLoad.bind(this);
        this.handleImageError = this.handleImageError.bind(this);

        this.xhr = new XhrService();
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        // scroll to top
        window.scrollTo(0, 0);
        
        // add loader
        $('.moviesInside').addClass('loadingSection');
        // get data
        this.getData({path: this.props.path + '/' + this.state.getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.moviesInside').removeClass('loadingSection');
        });
    }

    // img load
    handleImageLoad(dom) {
        $(dom.currentTarget).removeClass('loading');
    }

    // img load
    handleImageError(dom) {
        $(dom.currentTarget).removeClass('loading');
        $(dom.currentTarget).addClass('notfound');
    }

    // get data
    getData(props, callback) {

        // call xhr service
        XhrService.getMtData(props, this, function(data, ref){

            // job is done
            callback();
            console.log(props);
            // set state
            if (data.status === 200) {
                let movies = [];
                for(let i in data.response) {
                    let currentMovie = new Movie(data.response[i]);
                    movies.push(currentMovie);
                }

                ref.setState({
                    movies: ref.state.movies.concat(movies)
                });
            }
        })
    }

    // handle more data load
    handleMore() {
        // next page
        let getFrom = this.state.getFrom + 1;

        // loader class
        $('.moreButton').addClass('loading');

        // save page
        this.setState({
            getFrom: getFrom
        });

        // get data
        this.getData({path: this.props.path + '/' + getFrom + '/' + this.state.getNum, auth: false}, function(){
            $('.moreButton').removeClass('loading');
        });
    }

    /**
     * Render the component.
     */
    render() {
        return (<MoviesComponent movies={this.state.movies} handleMore={this.handleMore} handleImageLoad={this.handleImageLoad} handleImageError={this.handleImageError} />);
    }

}

export default MoviesPage;
