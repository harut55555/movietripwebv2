import BasePage from './containers/BasePage.jsx';
import HomeBoard from './containers/HomePage.jsx';
import LoginPage from './containers/LoginPage.jsx';
import ScenesPage from './containers/ScenesPage.jsx';
import SceneInsidePage from './containers/SceneInsidePage.jsx';
import LocationsPage from './containers/LocationsPage.jsx';
import BlogPage from './containers/BlogPage.jsx';
import BlogInsidePage from './containers/BlogInsidePage.jsx';
import LocationInsidePage from './containers/LocationInsidePage.jsx';
import MoviesPage from './containers/MoviesPage.jsx';
import MovieInsidepage from './containers/MoviesInsidePage.jsx';
import TripsPage from './containers/TripsPage.jsx';
import TripPage from './containers/TripPage.jsx';
import MyPage from './containers/MyPage.jsx';
import MyLocationsPage from './containers/MyLocationsPage.jsx';
import MyScenesPage from './containers/MyScenesPage.jsx';
import MyScenesAddPage from './containers/MyScenesAddPage.jsx'
import MyTripsPage from './containers/MyTripsPage.jsx';
import MyMoviesPage from './containers/MyMoviesPage.jsx';
import MyMovieAddPage from './containers/MyMovieAddPage.jsx';
import MyLocationAddPage from './containers/MyLocationAddPage.jsx'
import Auth from './modules/Auth';

const routes = {
  //base component (wrapper for the whole application).
    component: BasePage,
    childRoutes: [
    {
      path: '/',
      getComponent: (location, callback) => {
              callback(null, HomeBoard)
      }
    },
      {
          path: '/scenes',
          getComponent: (location, callback) => {
              callback(null, ScenesPage);
          }
      },
      {
          path: '/scene/:id',
          getComponent: (location, callback) => {
              callback(null, SceneInsidePage);
          }
      },
      {
          path: '/locations',
          getComponent: (location, callback) => {
              callback(null, LocationsPage);
          }
      },
      {
          path: '/location/:id',
          getComponent: (location, callback) => {
              callback(null, LocationInsidePage);
          }
      },
      {
          path: '/movies',
          getComponent: (location, callback) => {
              callback(null, MoviesPage);
          }
      },
      {
          path: '/movie/:id',
          getComponent: (location, callback) => {
              callback(null, MovieInsidepage);
          }
      },
      {
          path: '/blog',
          getComponent: (location, callback) => {
              callback(null, BlogPage);
          }
      },
      {
          path: '/blog/:id',
          getComponent: (location, callback) => {
              callback(null, BlogInsidePage);
          }
      },
      {
          path: '/trips',
          getComponent: (location, callback) => {
              callback(null, TripsPage);
          }
      },
      {
          path: '/trip/:id',
          getComponent: (location, callback) => {
              callback(null, TripPage);
          }
      },
      {
          path: '/my',
          onEnter: requireAuth,
          getComponent: (location, callback) => {
              callback(null, MyPage);
          },
          childRoutes: [
              {
                  path: '/my/locations',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyLocationsPage);
                  }
              },
              {
                  path: '/my/scenes',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyScenesPage);
                  }
              },
              {
                  path: '/my/scenes/add',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                      callback(null, MyScenesAddPage);
                  }
              },
              {
                  path: '/my/trips',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyTripsPage);
                  }
              }
              ,
              {
                  path: '/my/movies',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyMoviesPage);
                  }
              },
              {
                  path: '/my/movies/add',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyMovieAddPage);
                  }
              },
              {
                  path: '/my/locations/add',
                  onEnter: requireAuth,
                  getComponent: (location, callback) => {
                          callback(null, MyLocationAddPage);
                  }
              }
          ]
    },
    {
        path: '/login',
        onEnter: requireUnAuth,
        getComponent: (location, callback) => {
            callback(null, LoginPage);
        }
    }
  ]
};

function requireAuth(nextState, replaceState) {
    if (!Auth.isUserAuthenticated()) {
        replaceState('/login')
    }
}

function requireUnAuth(nextState, replaceState) {
    if (Auth.isUserAuthenticated()) {
        // replaceState({ nextPathname: nextState.location.pathname }, '/my')
        replaceState('/my')
    }
}

export default routes;
