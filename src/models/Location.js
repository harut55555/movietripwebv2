import ImgService from '../services/ImgService';

class Location {

  id = null;
  user_id = null;
  user_name = null;
  user_group = null;
  name = null;
  type = null;
  cover = null;
  coverImg = null;
  country = null;
  long = null;
  lat = null;
  city = null;
  address = null;
  availability = null;
  tags = null;
  status = null;
  date = null;
  imgLoaded = false;
  imgLoadedError = false;
  index = false;

  constructor(obj) {
      if(obj !== null && typeof obj !== "undefined") {
          this.init(obj);
      }
  }

  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.user_group = obj.user_group;
      this.name = obj.name;
      this.type = obj.type;
      this.cover = obj.cover;
      this.coverImg = obj.coverImg;
      this.country = obj.country;
      this.long = obj.long;
      this.lat = obj.lat;
      this.city = obj.city;
      this.address = obj.address;
      this.availability = obj.availability;
      this.tags = obj.tags;
      this.status = obj.status;
      this.date = obj.date;
      this.imgLoaded = obj.imgLoaded;
      this.imgLoadedError = obj.imgLoadedError;
      this.index = obj.index;
  }

  // init object with image background loading
  initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
  }

  static initRandom() {

  }
}

export default Location;