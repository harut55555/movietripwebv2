/**
 * Created by Arsen on 15.03.2017.
 */
import ImgService from '../services/ImgService';

class Blog {

    id = null;
    user_id = null;
    user_group = null;
    genre = null;
    name = null;
    year = null;
    cover = null;
    coverImg = null;
    country = null;
    director = null;
    actor = null;
    description = null;
    status = null;
    date = null;
    imgLoaded = false;
    imgLoadedError = false;
    index = null;

    constructor() {

    }

    init(obj) {
        this.id = obj.id;
        this.user_id = obj.user_id;
        this.user_group = obj.user_group;
        this.genre = obj.genre;
        this.name = obj.name;
        this.year = obj.year;
        this.cover = obj.cover;
        this.coverImg = obj.coverImg;
        this.country = obj.country;
        this.director = obj.director;
        this.actor = obj.actor;
        this.description = obj.description;
        this.status = obj.status;
        this.date = obj.date;
        this.imgLoaded = obj.imgLoaded;
        this.imgLoadedError = obj.imgLoadedError;
        this.index = obj.index;
    }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

    static initRandom() {

    }
}

export default Blog;