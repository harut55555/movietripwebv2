import ImgService from '../services/ImgService';

class Scene {

  id = null;
  user_id = null;
  user_name = null;
  user_group = null;
  title = null;
  cover = null;
  coverImg = null;
  genre = null;
  film = null;
  film_name = null;
  country = null;
  location = null;
  movie = null;
  city = null;
  address = null;
  brief = null;
  copyright = null;
  long = null;
  lat = null;
  check_in_text = null;
  editors_choice = null;
  status = null;
  date = null;
  imgLoaded = false;
  imgLoadedError = false;
  index = null;

    constructor(obj) {
        if(obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.user_group = obj.user_group;
      this.title = obj.title;
      this.cover = obj.cover;
      this.coverImg = obj.coverImg;
      this.genre = obj.genre;
      this.film = obj.film;
      this.film_name = obj.film_name;
      this.country = obj.country;
      this.location = obj.location;
      this.movie = obj.movie;
      this.city = obj.city;
      this.address = obj.address;
      this.brief = obj.brief;
      this.copyright = obj.copyright;
      this.long = obj.long;
      this.lat = obj.lat;
      this.check_in_text = obj.check_in_text;
      this.editors_choice = obj.editors_choice;
      this.status = obj.status;
      this.date = obj.date;
      this.imgLoaded = obj.imgLoaded;
      this.imgLoadedError = obj.imgLoadedError;
      this.index = obj.index;
  }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));

        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

  static initRandom() {

  }
}

export default Scene;