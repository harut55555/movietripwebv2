class Slider {

  id = null;
  user_id = null;
  user_name = null;
  title = null;
  cover = null;
  link = null;
  status = null;
  target= null;
  target_name=null;
  date = null;

    constructor(obj) {
        if(obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }


  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.title = obj.title;
      this.cover = obj.cover;
      this.link = obj.link;
      this.status = obj.status;
      this.target = obj.target;

      switch(this.target) {
          case 0:
              this.target_name="_blank";
              break;
          case 1:
              this.target_name="_self";
              break;
          case 2:
              this.target_name="_parent";
              break;
          case 3:
              this.target_name="_top";
              break;

      }
      this.target_name="_blank";

      this.date = obj.date;
  }

  static initRandom() {

  }
}

export default Slider;