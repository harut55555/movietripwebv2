import ImgService from '../services/ImgService';

class Trip {

  id = null;
  user_id = null;
  user_name = null;
  user_group = null;
  title = null;
  description = null;
  cover = null;
  coverImg = null;
  editors_choice = null;
  status = null;
  date = null;
  trip_point = null;
    imgLoaded = false;
    imgLoadedError = false;
  index = null;

  constructor(obj) {

      if(obj !== null && typeof obj !== "undefined") {
          this.init(obj);
      }
  }

  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.user_group = obj.user_group;
      this.title = obj.title;
      this.description = obj.description;
      this.cover = obj.cover;
      this.coverImg = obj.coverImg;
      this.editors_choice = obj.editors_choice;
      this.status = obj.status;
      this.date = obj.date;
      this.trip_point = obj.trip_point;
      this.imgLoaded = obj.imgLoaded;
      this.imgLoadedError = obj.imgLoadedError;
      this.index = obj.index;
  }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

  static initRandom() {}
}

export default Trip;