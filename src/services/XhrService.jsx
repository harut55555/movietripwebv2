import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import { browserHistory } from 'react-router';

class XhrService
{
    responseType = 'json';
    hostname = Config.getHostname();
    protocol = Config.getProtocol();
    port = Config.getPort();
    checkLoginPath = Config.getPath().refreshTokenCheck;

    /**
     * Class constructor.
     */
    constructor(props) {

    }
    
    
    static neeeeeew()
    {
        console.log("Xhr try");
    }
    // get MovieTrip Data
    static getMtData(props, callObj, callback) {
        let self = new XhrService();

        let data = null;

        // xhr request
        const xhr = new XMLHttpRequest();
        let host = self.protocol + '//' + self.hostname + ':' +  self.port + props.path;
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        // set the authorization HTTP header
        if (props.auth) {
            xhr.setRequestHeader('Authorization', `Bearer ${Auth.getAccessToken()}`);
        }
        xhr.responseType = self.responseType;
        xhr.addEventListener('load', () => {

            if (xhr.status === 401) {
                if (Auth.isUserAuthenticated()) {
                    XhrService.checkRefreshToken(props, data, callObj, callback, function(status, props, data, callObj, _callback) {
                        if(status) {
                            console.log('got new access token');
                            XhrService.getMtData(props, callObj, _callback);
                        }
                        else {
                            XhrService._handleLogin();
                        }
                    })
                }
                else {
                    XhrService._handleLogin();
                }
            }
            else {
                // callback the response
                callback(xhr, callObj);
            }
        });
        xhr.send();
    }

    // post MovieTrip Data
    static postMtData(props, data, callObj, callback) {
        let self = new XhrService();

        // xhr request
        const xhr = new XMLHttpRequest();
        let host = self.protocol + '//' + self.hostname + ':' +  self.port + props.path;

        xhr.open('post', host);
        xhr.setRequestHeader('Content-type', 'application/json');

        // set the authorization HTTP header
        if (props.auth) {
            xhr.setRequestHeader('refresh_token', `${Auth.getRefreshToken()}`);
        }
        xhr.responseType = self.responseType;
        xhr.addEventListener('load', () => {

            if (xhr.status === 401) {
                if (Auth.isUserAuthenticated()) {
                    XhrService.checkRefreshToken(props, data, callObj, callback, function (status, props, data, callObj, _callback) {
                        if (status) {
                            console.log('got new access token');
                            XhrService.postMtData(props, data, callObj, _callback);
                        }
                        else {
                            XhrService._handleLogin();
                        }
                    })
                }
                else {
                    XhrService._handleLogin();
                }
            }
            else {
                // callback the response
                callback(xhr, callObj);
            }
        });
        xhr.send(JSON.stringify(data));
    }

    static _handleLogin() {

        // redirect to login
        browserHistory.push('/login');
    }

    static checkRefreshToken(props, data, callObj, _callback, callback) {
        let self = new XhrService();
        // xhr request
        const xhr = new XMLHttpRequest();
        let host = self.protocol + '//' + self.hostname + ':' +  self.port + self.checkLoginPath;
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.responseType = self.responseType;
        // set the authorization HTTP header
        xhr.setRequestHeader('refresh_token', `${Auth.getRefreshToken()}`);
        xhr.addEventListener('load', () => {
            console.log("STATUS==",xhr.status);
            try {
                if (xhr.status === 200)
                    Auth.authenticateUser(xhr.response);

            } catch(e) {
                console.error(e);
            }
            callback((xhr.status === 200), props, data, callObj, _callback);
        });
        xhr.send();
    }
}

export default XhrService;
