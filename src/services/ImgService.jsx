import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';


class ImgService {

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    static getLink(img) {
        let path = (img !== undefined && img !== null) ? img.bigPath : null;
        console.log("IMgepaaaa=",path);
        path = this.fixPath(path);
        let protocol = Config.getProtocol();
        let host = Config.getHostname();
        let port = Config.getPort();
        return protocol + '//' + host + ':' + port + path;
    }

    static fixPath(path) {
        if (path !== null) {
            path = encodeURI(path);
        }

        return path;
    }
}

export default ImgService;
