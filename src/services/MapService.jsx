import React from 'react';
import Auth from '../modules/Auth';
import GoogleMap from 'google-map-react';

class MapService extends React.Component {

    static defaultProps = {};

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);
        this.state = {
            mapProps: {
                center: {lat: 59.938043, lng: 30.337157},
                zoom: 9
            }
        };

        this.handleEvents = this.handleEvents.bind(this);
    }


    /**
     * This method will be executed after initial rendering.
     */
  /* componentDidMount() {
        const xhr = new XMLHttpRequest();
        let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        // set the authorization HTTP header
        xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
        xhr.responseType = 'json';
        xhr.addEventListener('load', () => {
            if (xhr.status === 200) {
                this.setState({
                    location: xhr.response.list
                });
            }
        });
        xhr.send();
    }
*/

    handleEvents(map) {

        map.maps.event.addListener(map.map, 'click', function(event){
            this.setOptions({scrollwheel:true});
        });

        map.maps.event.addListener(map.map, 'mouseout', function(event){
            this.setOptions({scrollwheel:false});
        });
    }

    render() {
        return (
            <GoogleMap
                bootstrapURLKeys={{
                key: "AIzaSyCmFJ5zhFfnWNo8WSmqZd5CLERccPR-Esk",
                language: 'en'
            }}
                onGoogleApiLoaded={(map) => {
                    this.handleEvents(map);
                }}

                yesIWantToUseGoogleMapApiInternals

                options={{
                    scrollwheel: false
                }}

                defaultCenter={this.state.mapProps.center}
                defaultZoom={this.state.mapProps.zoom}>

            </GoogleMap>);
    }
}

export default MapService;