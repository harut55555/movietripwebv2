import React from 'react';
import Auth from './modules/Auth';

class Config {

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    static isProd() {
        return false;
    }

    static getHostname(){
       
        return (this.isProd() ? location.hostname : '13.81.67.155');
    }

    static getPort() {
        return 90;
    }

    static getProtocol(){
        return (this.isProd() ? location.protocol : 'http:');
    }

    static getLimit(){
        return {
            movies: 16,
            scenes: 16,
            locations: 16,
            trips: 12,
            blogs:16
            
        }
    }

    static getPath(){
        let host = Config.getProtocol() + '//' + Config.getHostname() + ':' +  Config.getPort();
        let api = '/api/v1/';
        return {
            'home': api + 'home',
            'movie': api + 'movie',
            'scene': api + 'scene',
            'location': api + 'location',
            'trip': api + 'trip',
            'blog': api + 'blog',
            'login': api + 'auth/login',
            'fbLogin': api + 'auth/fblogin',
            'refreshTokenCheck': api + 'auth/RefreshTokenCheck',
            'imagehost' : host
        };
    }

    static getError(num) {

        let errors = {
            500: 'there was some serious problem, please contact to admin'
        };

        return errors[num];
    }

    static getLocationType(name) {

        let locatinos = {
            'restourant': 2
        };

        return locatinos[name];
    }

    static fbLink = "https://www.facebook.com/v2.6/dialog/oauth?client_id=165399480626657&scope=public_profile,email&response_type=code&redirect_uri=http://13.81.67.155:90/signin-facebook&state=true";

    static fbParams = {
        appId            : '165399480626657',
        autoLogAppEvents : false,
        status           : false,
        xfbml            : false,
        return_scopes: true,
        response_type:  "code",
        state:  true,
        version          : 'v2.9' // or v2.8, v2.7, v2.6, v2.5, v2.4, v2.3,
    }
}

export default Config;
