/**
 * Created by harut on 7/4/17.
 */

import React from 'react';
import routes from "./routes";
import { createMemoryHistory } from 'history'

import BasePage from './containers/BasePage'
import HomeBoard from './containers/HomePage'
import { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom'


module.exports = React.createClass({
    getInitialState: function () {
        return {wishes: ['Peace on earth', 'Drinkable water for everyone', 'Free ice cream for kids']};
    },
    render: function () {
        const history = createMemoryHistory();
            return (
                <Switch>
                    <Route path="/" component={HomeBoard}/>
                </Switch>
                );
    }
});