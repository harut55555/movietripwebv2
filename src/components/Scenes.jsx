/**
 * Created by Arsen on 10.02.2017.
 */
import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';

const Scenes = ({scenes, handleMore}) => (
    <div>
        {/*<section className="filterSection">*/}
    {/*<div className="panel-group" id="filterSectionAccordion" role="tablist" aria-multiselectable="true">*/}
        {/*<div className="panel panel-default">*/}
            {/*<div className="panel-heading" role="tab" id="filterSectionAccordionHeading">*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*<div className="col-xs-12">*/}
                            {/*<h4 className="panel-title">*/}
                                {/*<a role="button" data-toggle="collapse" data-parent="#filterSectionAccordion" href="#filterSectionPanel" aria-expanded="false" aria-controls="filterSectionPanel" className="collapsed">*/}
                                    {/*Filter*/}
                                {/*</a>*/}
                            {/*</h4>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
            {/*</div>*/}
            {/*<div id="filterSectionPanel" className="panel-collapse collapse" role="tabpanel" aria-labelledby="filterSectionAccordionHeading" aria-expanded="false">*/}
                {/*<div className="panel-body">*/}
                    {/*<form>*/}
                        {/*<div className="container">*/}
                            {/*<div className="row">*/}
                                {/*<div className="col-xs-12">*/}
                                    {/*<section className="filterSectionIn">*/}
                                        {/*<div className="filterSectionInside">*/}
                                            {/*<div className="row">*/}
                                                {/*<div className="col-lg-6">*/}
                                                    {/*<label>*/}
                                                        {/*<select name="filterMovieGenre" id="filterMovieGenre" multiple title="Choose the genre"></select>*/}
                                                    {/*</label>*/}
                                                {/*</div>*/}
                                                {/*<div className="col-lg-6">*/}
                                                    {/*<label>*/}
                                                        {/*<select name="filterLocationType" id="filterLocationType" multiple title="Choose location type"></select>*/}
                                                    {/*</label>*/}
                                                {/*</div>*/}
                                                {/*<div className="col-xs-12">*/}
                                                    {/*<div className="filterSectionSubmit">*/}
                                                        {/*<input type="submit" value="Submit" />*/}
                                                    {/*</div>*/}
                                                {/*</div>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                    {/*</section>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</form>*/}
                {/*</div>*/}
            {/*</div>*/}
        {/*</div>*/}
    {/*</div>*/}
{/*</section>*/}
        <section className="scenesInside">
            <div className="container">
                <div className="row">
                    {scenes.map(function(object, i){
                            return  <div className="col-xs-12 col-sm-4" key={i}>
                                <div className={"promoBlock sceneBlock " + ((!scenes[i].imgLoaded && !scenes[i].imgLoadedError) ? 'loading' : '') + (scenes[i].imgLoadedError ? 'notfound' : '')}  style={{'backgroundImage':'url('+ ImgService.getLink(scenes[i].coverImg) + ')'}}>
                                    <Link to={"/scene/" + scenes[i].id} className="promoBlockInfo" >
                                        <div>
                                            <h3>{scenes[i].movie === undefined? '': scenes[i].movie.name}</h3>
                                            <h4>{scenes[i].location === undefined? '' : scenes[i].location.name}</h4>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        })
                    }
                    <div className="pageNext">
                        <button className="moreButton" onClick={handleMore}>More</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
);


Scenes.propTypes = {
    scenes: PropTypes.array.isRequired
};

export default Scenes;
