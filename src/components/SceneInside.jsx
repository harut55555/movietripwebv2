import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import MapService from '../services/MapService';
import ImgService from'../services/ImgService';

const SceneInside = ({scene, onFbClick, otherScenes, relatedScenes}) => (
    <div>
        <section className="sceneInsideTop" itemScope itemType="http://schema.org/Article">
            <meta itemScope itemProp="mainEntityOfPage" itemType="https://schema.org/WebPage" itemID={"/scene/" + scene.id} />
            <meta itemProp="headline" content={scene.name} />
            <meta itemProp="datePublished" content={scene.date} />
            <meta itemProp="dateModified" content={scene.date} />
            <div className="hidden" itemProp="author" itemScope itemType="http://schema.org/Organization">
                <meta itemProp="name" content="Movietrip LLC" />
                <meta itemProp="URL" content="http://www.movietrip.me" />
                <div itemProp="logo" itemScope itemType="http://schema.org/ImageObject">
                    <meta itemProp="url" content="http://www.movietrip.me/img/movietrip-logo.png" />
                </div>
            </div>
            <div className="hidden" itemProp="publisher" itemScope itemType="http://schema.org/Organization">
                <meta itemProp="name" content="Movietrip LLC" />
                <meta itemProp="URL" content="http://www.movietrip.me" />
                <div itemProp="logo" itemScope itemType="http://schema.org/ImageObject">
                    <meta itemProp="url" content="http://www.movietrip.me/img/movietrip-logo.png" />
                </div>
            </div>
            <section className="sceneInsideTopImg">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="sceneInsideTopImgIn">
                                <div className="sceneInsideTopImgLeft" itemProp="image" itemScope itemType="http://schema.org/ImageObject" style={{'backgroundImage':'url(' + (scene.movie === undefined? "": ImgService.getLink(scene.movie.coverImg)) + ')'}}>
                                    <meta itemProp="url" content={ImgService.getLink(scene.coverImg)} />
                                    <meta itemProp="contentUrl" content={ImgService.getLink(scene.coverImg)} />
                                    <meta itemProp="width" content="800"/>
                                    <meta itemProp="height" content="450"/>
                                    <h1>
                                        <span>{(scene.movie === undefined )? "": scene.movie.name}{(scene.location !== undefined && scene.location !== null) ? <span itemProp="name"> - <Link to={"/location/" + scene.location.id}>{scene.location.name}</Link></span> : ''}</span>
                                    </h1>
                                      <span className="cprt">{scene.copyright}</span>
                                </div>
                                <div className="sceneInsideTopImgRight" itemScope itemType="http://schema.org/Movie">
                                    <meta itemProp="name" content={((scene.movie !== undefined && scene.move !== null) ? scene.movie.name : '')} />
                                    <meta itemProp="dateCreated" content={scene.date} />
                                    <div className="hidden" itemProp="director" itemScope itemType="http://schema.org/Person">
                                        <span itemProp="name">{(scene.movie !== undefined && scene.movie !== null) ? scene.movie.directors : ''}</span>
                                    </div>
                                    <Link itemProp="image" itemScope itemType="http://schema.org/ImageObject" className="movieThumb" to={'/movie/' + ((scene.movie !== undefined && scene.movie !== null) ? scene.movie.id : null)}>
                                        <meta itemProp="url" content={ImgService.getLink(scene.coverImg)} />
                                        <img itemProp="contentUrl" src={ImgService.getLink(scene.coverImg)} alt={((scene.movie !== undefined && scene.move !== null) ? scene.movie.name : '')} title={((scene.movie !== undefined && scene.move !== null) ? scene.movie.name : '')} />
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="sceneInsideTopInfo" >
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div className="sceneInsideTopInfoText">
                                <i className={'mticon-' + ((scene.location !== undefined && scene.location !== null && scene.location.type !== undefined && scene.location.type !== null) ? scene.location.type.name : '')}></i>
                                <p itemProp="description">{scene.description}</p>
                            </div>
                        </div>
                       
                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-push-1">
                            <div className="sceneInsideTopInfoAddress" itemScope itemType="http://schema.org/Place">
                                <ul itemProp="address" itemScope itemType="http://schema.org/PostalAddress">
                                    <li><strong>City:</strong> <span itemProp="addressLocality">{(scene.location === undefined || scene.location === null || scene.location.city === undefined || scene.location.city === null) ? "": scene.location.city.name}</span></li>
                                    <li><strong>Country:</strong>{(scene.location === undefined || scene.location === null || scene.location.country === undefined || scene.location.country === null)? "": scene.location.country.name}</li>
                                    <li><strong>Location:</strong> <a href="#">{(scene.location === undefined || scene.location === null)? "": scene.location.name}</a></li>
                                    <li><strong>Address:</strong> <span itemProp="streetAddress">{(scene.address === undefined || scene.address === null) ? "": scene.address.name}</span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3 col-lg-push-1">
                            <div className="sceneInsideTopInfoSocial">
                                <a href="#"><i className="fnicon-pinterest"></i></a>
                                <a onClick={onFbClick} href="#" className="fb">
                                    <i className="fnicon-facebook-f"></i>
                                </a>
                                <a href={'https://twitter.com/intent/tweet?text=' + ((scene.movie === undefined )? "": scene.movie.name) + ' - ' + ((scene.location !== undefined && scene.location !== null) ? scene.location.name : '') + '&url=' + window.location.href} target="_blank"><i className="fnicon-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <section className="sceneInsideMap">
            <div id="map_canvas">
                <MapService />
            </div>
        </section>
        <section className="sceneInsideOther">
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        Here will be facebook comments
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <h2>Other scenes from movie</h2>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    {otherScenes.map(function(object, i){
                        return  <div className="col-xs-12 col-sm-4" key={i}>
                            <div className={"promoBlock sceneBlock " + ((!otherScenes[i].imgLoaded && !otherScenes[i].imgLoadedError) ? 'loading' : '') + (otherScenes[i].imgLoadedError ? 'notfound' : '')}  style={{'backgroundImage':'url('+ ImgService.getLink(otherScenes[i].coverImg) + ')'}}>
                                <Link to={"/scene/" + otherScenes[i].id} className="promoBlockInfo" >
                                    <div>
                                        <h3>{otherScenes[i].movie === undefined? '': otherScenes[i].movie.name}</h3>
                                        <h4>{otherScenes[i].location === undefined? '' : otherScenes[i].location.name}</h4>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    })
                    }
                </div>
            </div>
        </section>
        <section className="relatedWrap">
            <section className="relatedIn relatedScenes">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <h2>Related Scenes</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">

                        {relatedScenes.map(function(object, i){
                            return  <div className="col-xs-12 col-sm-4" key={i}>
                                <div className={"promoBlock sceneBlock " + ((!relatedScenes[i].imgLoaded && !relatedScenes[i].imgLoadedError) ? 'loading' : '') + (relatedScenes[i].imgLoadedError ? 'notfound' : '')}  style={{'backgroundImage':'url('+ ImgService.getLink(relatedScenes[i].coverImg) + ')'}}>
                                    <Link to={"/scene/" + relatedScenes[i].id} className="promoBlockInfo" >
                                        <div>
                                            <h3>{relatedScenes[i].movie === undefined? '': relatedScenes[i].movie.name}</h3>
                                            <h4>{relatedScenes[i].location === undefined? '' : relatedScenes[i].location.name}</h4>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        })
                        }
                    </div>
                </div>
            </section>
        </section>

    </div>
    
);




SceneInside.propTypes = {
    scene: React.PropTypes.object,
    onFbClick: React.PropTypes.func,
    id: React.PropTypes.string
};


export default SceneInside;
