/**
 * Created by Arsen on 20.02.2017.
 */
import React from 'react';
import { Link, IndexLink } from 'react-router';
import PropTypes from 'prop-types';
import MapService from '../services/MapService';
import ImgService from'../services/ImgService'

const MoviesInside = ({movie, onFbClick, relatedScenes, handleImageLoad, handleImageError}) => (
    <div>
        <section className="movieInsideTop" itemScope itemType="http://schema.org/Movie">
            <section className="movieInsideTopImg">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="movieInsideTopImgIn adaptive-parent gradient-wrap">
                                <div className="movieInsideTopImgLeft" itemProp="image" itemScope itemType="http://schema.org/ImageObject">
                                    <div >
                                        
                                    <meta itemProp="url" content={ImgService.getLink(movie.coverImg)}/>
                                    <img itemProp="contentUrl" src={ImgService.getLink(movie.coverImg)}
                                         data-adaptive-background="1" alt={movie.name} title={movie.name}/>
	                                    {
		                                    (function () {
			                                    $.adaptiveBackground.run({
				                                    parent: ".adaptive-parent",
				                                    exclude: ["rgb(0,0,0)", "rgb(220,220,220)"]
			                                    });
			                                    $(document).ready(function () {
				                                    $("[data-adaptive-background]").on("ab-color-found", function () {
					                                    $(this).parents(".adaptive-parent").adaptiveColor();
				                                    });
			                                    });
		                                    })()
	                                    }
                                    </div>
                                </div>
                                <div className="movieInsideTopImgRight">
                                    <div className="movieInsideTopImgRightIn">
                                        <div className="movieInsideTopImgRightInside">
                                            <h1 itemProp="name">{movie.name}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="movieInsideTopInfo">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-4">
                            <div className="movieInsideTopInfoText" itemProp="description">
                                <p dangerouslySetInnerHTML={{__html: movie.description}}></p>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4">
                            <div className="movieInsideTopInfoAddress">
                                
                                <ul>
                                    <li><strong>Year:</strong> <span itemProp="dateCreated">{movie.year}</span></li>
                                    <li>
                                        <strong>Director:</strong>
                                        <span itemProp="director" itemScope itemType="http://schema.org/Person"><span itemProp="name">{movie.directors}</span></span>
                                    </li>
                                    <li><strong>With:</strong> <span itemProp="actor" itemScope itemType="http://schema.org/Person"><span itemProp="name">{movie.actors}</span></span></li>
                                    <li><strong>Genre:</strong> <span itemProp="genre">{(movie.genres === undefined || movie.genres === null) ? "": (movie.genres.map(function(object, i){return (movie.genres[i].name + ((i < (movie.genres.length - 1)) ? ", " : ""))}))}</span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="movieInsideTopInfoSocialWrap col-xs-12 col-sm-4 col-lg-3 col-lg-push-1">
                            <div className="movieInsideTopInfoSocial">
                                <a href="#"><i className="fnicon-pinterest"></i></a>
                                <a onClick={onFbClick} href="#" className="fb">
                                    <i className="fnicon-facebook-f"></i>
                                </a>
                                <a href={'https://twitter.com/intent/tweet?text=' + movie.name + '&url=' + window.location.href} target="_blank"><i className="fnicon-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

        <section className="movieInsideMap">
            <div id="map_canvas">
                <MapService />
            </div>
        </section>
        <section className="relatedWrap">
            <section className="relatedIn relatedScenes">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            here will be facebook comments
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <h2>Scenes From The Film</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {relatedScenes.map(function(object, i){
                            return  <div className="col-xs-12 col-sm-4" key={i}>
                                <div className={"promoBlock sceneBlock " + ((!relatedScenes[i].imgLoaded && !relatedScenes[i].imgLoadedError) ? 'loading' : '') + (relatedScenes[i].imgLoadedError ? 'notfound' : '')}  style={{'backgroundImage':'url('+ ImgService.getLink(relatedScenes[i].coverImg) + ')'}}>
                                    <Link to={"/scene/" + relatedScenes[i].id} className="promoBlockInfo" >
                                        <div>
                                            <h3>{relatedScenes[i].movie === undefined? '': relatedScenes[i].movie.name}</h3>
                                            <h4>{relatedScenes[i].location === undefined? '' : relatedScenes[i].location.name}</h4>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        })
                        }
                    </div>
                </div>
            </section>
            {/*<section className="relatedIn relatedMovies">*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*<div className="col-xs-12">*/}
                            {/*<h2>Related Movies</h2>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*{relatedScenes.map(function(object, i){*/}
                            {/*return  <div className="col-xs-12 col-sm-4 col-md-3" key={i}>*/}
                                {/*<Link to="#" onLoad={handleImageLoad} onError={handleImageError}>*/}
                                    {/*<figure>*/}
                                        {/*<img src={ImgService.getLink(relatedScenes[i].coverImg)}  title=""  alt="" />*/}
                                    {/*</figure>*/}
                                {/*</Link>*/}
                            {/*</div>*/}
                        {/*})}*/}
                    {/*</div>*/}
                {/*</div>*/}
            {/*</section>*/}
            {/*<section className="relatedIn relatedLocations">*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*<div className="col-xs-12">*/}
                            {/*<h2>Related Locations</h2>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*/!*<div className="col-xs-12 col-sm-4">*!/*/}
                            {/*/!*<div className="promoBlock locationBlock" style={{'backgroundImage':'url(/img/frontend/img-1.jpg)'}}>*!/*/}
                                {/*/!*<a href="#" className="promoBlock-topLink"><span>Locations</span></a>*!/*/}
                                {/*/!*<span className="promoBlockAuthor">by <a href="#">Vasya Pupkin</a></span>*!/*/}
                                {/*/!*<a href="#" className="promoBlockInfo">*!/*/}
                                    {/*/!*<div>*!/*/}
                                        {/*/!*<h3>The hustler scene with Jack</h3>*!/*/}
                                        {/*/!*<h4>Bitov garaji mej vra en talis Jackin Jack lrvcrats atori vra nayume lusapaytsar mijavayrin</h4>*!/*/}
                                    {/*/!*</div>*!/*/}
                                {/*/!*</a>*!/*/}
                            {/*/!*</div>*!/*/}
                        {/*/!*</div>*!/*/}
                    {/*</div>*/}
                {/*</div>*/}
            {/*</section>*/}
        </section>

    </div>
);


MoviesInside.propTypes = {
    movie: React.PropTypes.object
};


export default MoviesInside;