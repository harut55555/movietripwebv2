import React from 'react';
import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';

const Blog = ({blogs, handleMore, handleImageLoad, handleImageError}) => (
    <div>
        <section className="blogsInside">
            <div className="container">
                <div className="row">
                    {blogs.map(function (object, i) {
                        return <div className="col-xs-12 col-sm-6">
                            <div className={"promoBlock blogBlock" + ((!blogs[i].imgLoaded && !blogs[i].imgLoadedError) ? 'loading' : '') + (blogs[i].imgLoadedError ? 'notfound' : '')} key={i} style={{'backgroundImage':'url('+ImgService.getLink(blogs[i].cover)+')'}}>
                                <Link to="#" className="promoBlock-topLink"><span>Blog</span></Link>
                                <span className="promoBlockAuthor">by <Link to="#">mareeshok</Link></span>
                                <Link to={'blog/' + blogs[i].id} className="promoBlockInfo">
                                    <div>
                                        <h3>{blogs[i].name}</h3>
                                        <h4></h4>
                                    </div>
                                </Link>
                            </div>
                        </div>;
                    })}
                    <div className="pageNext">
                        <button className="moreButton" onClick={handleMore}>More</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
);



Blog.propTypes = {
    blogs: React.PropTypes.array
};


export default Blog;
