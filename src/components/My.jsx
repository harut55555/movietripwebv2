import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import Auth from '../modules/Auth';


const My = ({ children  }) => (

    <div>
        <section className="profile">
            <section className="profileTop" style={{backgroundImage: 'url(/img/frontend/img-3.png)'}}>
                <section className="profileTopMain">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="profilePic" style={{backgroundImage: 'url(https://graph.facebook.com/994280820628803/picture?width=150&height=150)'}}>
                                    <Link to="#" id="profilePhotoLink">
                                        <i className="fnicon-photo" />
                                    </Link>
                                    <input type="file" id="avatarphoto" name="avatarphoto" style={{display: 'none'}} accept=".jpg,.jpeg,.png" />
                                </div>
                            </div>
                            <div className="col-lg-4 col-lg-offset-1">
                                <div className="profileDesc">
                                    <h1><span>Arsen Mkrtchyan<Link to="#"><i className="fnicon-edit" /></Link></span></h1>
                                    <h2>ars.mkrtchyan@mail.ru</h2>
                                    <p>
                                        After Treadstone is being closed, Bourne walks away over the Pont des Arts and disappears.
                                        Bourne and Marie arrive in Paris. Bourne watches Conklin from the roof of.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="profileTopMenu">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="profileMenu dropdown">
                                    <a className="dropdown-toggle hidden-sm hidden-md hidden-lg" id="profileMenuDrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                                    <ul className="dropdown-menu visible-sm visible-md visible-lg" aria-labelledby="profileMenuDrop">
                                        <li><Link to="/my/scenes">Scenes</Link></li>
                                        <li><Link to="/my/locations">Locations</Link></li>
                                        <li><Link to="/my/movies">Movies</Link></li>
                                        <li><Link to="/my/trips">Trips</Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <section className="profileBottom">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                           
                            {children}
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
);

My.propTypes = {
    children: React.PropTypes.object
};




export default My;
