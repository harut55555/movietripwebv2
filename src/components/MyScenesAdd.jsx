import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import MapService from '../services/MapService';
import MyMovieAddPart from '../components/MyMovieAddPart.jsx';
import MyLocationAddPage from '../containers/MyLocationAddPage.jsx';
import MyMovieAddPage from '../containers/MyMovieAddPage.jsx';
const MyScenesAdd = ({address,description,part,handleChangeAddress,handleChangeDescription,handleImage}) => (
    <div>
        <section className="profileInsideBlock profileScenes">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Add Scene</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12">
                    <section className="addForm">
                        <form action="">
                            <div className="row">
                                <div className="col-xs-12">
                                    <h3>1. Insert first step information</h3>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneMovieName">Movie name</label>
                                        <div className="mtInputWrap">
                                            <input type="text" id="addSceneMovieName" className="mtInput" />
                                            <button type="button" className="mtModalBtn" data-toggle="modal" data-target="#addMovieModal">+</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneLocation">Location</label>
                                        <div className="mtInputWrap">
                                            <input type="text" id="addSceneLocation" className="mtInput" />
                                            <button type="button" className="mtModalBtn" data-toggle="modal" data-target="#addLocationModal">+</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneAddress">Address</label>
                                        <input type="text" id="addSceneAddress" className="mtInput" value={address} onChange={handleChangeAddress}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <h3>2. Insert second step information</h3>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneDescription">Description</label>
                                        <textarea name="addSceneDescription" id="addSceneDescription" className="mtInput" cols="" rows={6} defaultValue="" value={description} onChange={handleChangeDescription} />
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneTags">Tags</label>
                                        <div className="mtTags">
                                            <h2>
                                                <span>Tag 1</span>
                                                <a href="#"><i className="fnicon-close" /></a>
                                            </h2>
                                            <h2>
                                                <span>Tag 3</span>
                                                <a href="#"><i className="fnicon-close" /></a>
                                            </h2>
                                            <h2>
                                                <span>Al Pacino</span>
                                                <a href="#"><i className="fnicon-close" /></a>
                                            </h2>
                                            <label>
                                                <input type="text" placeholder="Add Tag" name="addSceneTags" id="addSceneTags" />
                                                <button type="button"><span>+</span></button>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <div className="mapWrap">
                                            <div id="map_canvas">
                                                        <MapService />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="addSceneImage">Image</label>
                                        <div className="mtImage">
                                            <input type="file" id="addSceneImage" className="hidden" hidden=" " onChange={handleImage}/>
                                            <div className="mtImageLeft">
                                                <div className="mtImageLeftIn">
                                                    <div className="mtImageLeftInside">
                                                        <h2>No image</h2>
                                                        <figure>
                                                            <img id="imga" src="img/frontend/img-3.png" alt =" "/>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="mtImageRight">
                                                <div className="mtImageRightIn">
                                                    <div className="mtImageRightInside">
                                                        <label htmlFor="addSceneImage">
                                                            <span>Upload image</span>
                                                        </label>
                                                        <span>or select from <a href="#">existing</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
                <div className="pageNext">
                    <a href="#">Send for approval</a>
                </div>
            </div>
        </section>
        {/* Add Movie Modal */}
        <div className="modal fade mtModal" id="addMovieModal" tabIndex={-1} role="dialog" aria-labelledby="addMovieModalToggle">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i className="fnicon-close" /></span></button>
                        <h4 className="modal-title" id="addMovieModalLabel">Add Movie</h4>
                    </div>
                    <div className="modal-body">
                        <MyMovieAddPage part={part}/>
                        
                    </div>
                    <div className="modal-footer">
                        <div className="mtModalFooterBtns">
                            <div className="pageNext">
                                <button type="button" data-dismiss="modal">Send for approval</button>
                                <button type="button" data-dismiss="modal" className="closeModal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Add Movie Modal End */}
        {/* Add Location Modal */}
        <div className="modal fade mtModal" id="addLocationModal" tabIndex={-1} role="dialog" aria-labelledby="addLocationModalToggle">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i className="fnicon-close" /></span></button>
                        <h4 className="modal-title" id="addLocationModalLabel">Add Location</h4>
                    </div>
                    <div className="modal-body">
                        <MyLocationAddPage part={part}/> 
                    </div>
                    <div className="modal-footer">
                        <div className="mtModalFooterBtns">
                            <div className="pageNext">
                                <button type="button" data-dismiss="modal">Send for approval</button>
                                <button type="button" data-dismiss="modal" className="closeModal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Add Location Modal End */}
    </div>
);

MyScenesAdd.propTypes = {
    handleChangeAddress: React.PropTypes.func,
    handleChangeDescription: React.PropTypes.func
};


export default MyScenesAdd;