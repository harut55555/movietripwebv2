/**
 * Created by Arsen on 23.02.2017.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService.jsx'


const Trips = ({trips, handleMore, handleImageLoad, handleImageError}) => (
    <div>
        <section className="tripsInside">
            <div className="container">
                <div className="row">
                    {trips.map(function(object, i){
                        const tripID = trips[i].id;

                        return   <div className="col-xs-12 col-sm-4 col-md-3" key={i}>
                            <Link to={"/trip/" + tripID} className="specialLocation" onLoad={handleMore} onError={handleImageError}>
                            <div className="specialLocationType"/>
                                <div className="specialLocationBg" style={{'backgroundImage':'url('+ ImgService.getLink(trips[i].coverImg)+ ')'}}/>
                                <div className="specialLocationCaption">
                                    <h2>{trips[i].title}</h2>
                                </div>
                            </Link>
                        </div>
                    })}
                    <div className="pageNext">
                        <button className="moreButton" onClick={handleMore}>More</button>
                    </div>
                </div>
            </div>
        </section>

    </div>
);

Trips.propTypes = {
    trips: React.PropTypes.array
};


export default Trips;
