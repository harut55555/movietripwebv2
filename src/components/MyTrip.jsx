import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';

const MyTrip = ({mytrips,addClick,edithandleClick,deletehandleClick}) => (
    <div>
        <section className="profileInsideBlock profileTrips">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Trips</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-3">
                    <div className="promoBlock createBlock">
                        <a href="add">
                            <h5>Create Trip</h5>
                        </a>
                    </div>
                </div>
                <div className="col-xs-12 col-sm-3">
                    <div className="thumbBtns">
                        <a className="editThumb" href="add" title="Edit"><i className="fnicon-edit" /></a>
                        <a className="deleteThumb" href="#" title="Delete" onClick={deletehandleClick}><i className="fnicon-close" /></a>
                    </div>
                    <a href="trip-inside.html" className="specialLocation">
                        <div className="specialLocationType" />
                        <div className="specialLocationBg" style={{backgroundImage: 'url(/img/frontend/trip-1.jpg)'}} />
                        <div className="specialLocationCaption">
                            <h3>Gossip Girl season 2 trip in New York - 8 Locations</h3>
                            <h4>John Wick</h4>
                        </div>
                    </a>
                    <div className="profilePostStatus pending">
                        <h2>Pending</h2>
                    </div>
                </div>
                <div className="pageNext">
                    <a href="#">View All</a>
                </div>
            </div>
        </section>
    </div>
);




MyTrip.propTypes = {
    movies: React.PropTypes.array,
    deletehandleClick: React.PropTypes.func,
    edithandleClick: React.PropTypes.func,
    addClick: React.PropTypes.func
};


export default MyTrip;