/**
 * Created by Arsen on 23.02.2017.
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';
import MapService from '../services/MapService';
const BlogInside = ({ blog,id}) => (
    <div>
        <section itemScope itemType="http://schema.org/BlogPosting">
            <meta itemScope itemProp="mainEntityOfPage" itemType="https://schema.org/WebPage" itemID="http://www.movietrip.me/blog/view/98" />
            {/* must be a valid url starting with http:// */}
            <meta itemProp="inLanguage" content="en-US" />
            <meta itemProp="dateModified" content="2017-02-03" />
            {/* must match format */}
            <section className="blogInsideTopImg">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className={"blogInsideTopImgIn"+ ((!blog.imgLoaded && !blog.imgLoadedError) ? 'loading' : '')
                             + (blog.imgLoadedError ? 'notfound' : '')} itemprop="image" itemscope itemtype="http://schema.org/ImageObject"
                                 style={{'backgroundImage':'url('+ ImgService.getLink(blog.coverImg) + ')'}}>
                                <meta itemProp="url" content="http://www.movietrip.me//mda.php?id=5318&size" />
                                {/* must be a valid url starting with http:// */}
                                <meta itemProp="contentUrl" content="http://www.movietrip.me//mda.php?id=5318&size" />
                                {/* must be a valid url starting with http:// */}
                                <meta itemProp="width" content={1170} />
                                {/* must match img width */}
                                <meta itemProp="height" content={560} />
                                {/* must match img height */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="blogInside">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="blogInsideDate" itemProp="datePublished" content="2017-02-03">{/* must match format */}03
                                February 2017
                            </div>
                            <div className="blogInsideTitle">
                                <h1 itemProp="headline">Your ultimate movie guide to the Valentine’s Day</h1>
                            </div>
                            <div className="blogInsideAuthor">
                                <div className="blogInsideAuthorIn">
                                    <a href="#">
                                        <span className="blogInsideAuthorInLeft" style={{backgroundImage: 'url(img/blog_insideUser.png)'}} />
                                        <span className="blogInsideAuthorInRight" itemProp="author">mareeshok</span>
                                        <div className="hidden" itemProp="publisher" itemScope itemType="http://schema.org/Organization">
                                            <meta itemProp="name" content="Movietrip LLC" />
                                            <meta itemProp="URL" content="http://www.movietrip.me" />
                                            <div itemProp="logo" itemScope itemType="http://schema.org/ImageObject">
                                                <meta itemProp="url" content="http://www.movietrip.me/img/movietrip-logo.png" />
                                                {/* must be a valid url starting with http:// */}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="blogInsideBody" itemProp="articleBody">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <div className="blogInsideBody">
                                            <p>
                                                Last month, Google suggested that this driverless utopia may actually be much further
                                                away than many people may realize. In a speech at SXSW in Austin, Google's
                                                car project director Chris Urmson explained that the day when fully autonomous
                                                vehicles are widely available, going anywhere that regular cars can, might be as
                                                much as 30 years away. There are still serious technical and safety challenges to
                                                overcome. In the near term, self-driving cars may be limited to more narrow situations
                                                and clearer weather.
                                            </p>
                                            <p>
                                                As Lee Gomes pointed out at IEEE Spectrum, this was the most conservative roadmap
                                                yet offered by Google, which has been operating and tweaking autonomous
                                                cars for years on private and public roads. If they're saying it's hard, we ought to listen.
                                            </p>
                                            <p>
                                                So what are the big hold-ups, anyway? After watching Urmson's presentation, I
                                                called two experts — Edwin Olson of the University of Michigan and Nidhi Kalra of
                                                the RAND Corporation — to dive more into the obstacles that stand between us
                                                and our glorious self-driving future. None of these things are deal-breakers per se,
                                            </p>
                                            <div className="blogInsideBodyImg">
                                                <img src="/img/blog_insideBg.png" />
                                            </div>
                                            <p>
                                                Last month, Google suggested that this driverless utopia may actually be much further away than
                                                many people may realize. In a speech at SXSW in Austin, Google's car project director Chris
                                                Urmson explained that the day when fully autonomous vehicles are widely available, going
                                                anywhere that regular cars can, might be as much as 30 years away. There are still serious
                                                technical and safety challenges to overcome. In the near term, self-driving cars may be limited
                                                to more narrow situations and clearer weather.
                                            </p>
                                            <div className="blogInsideBodyImg">
                                                <iframe src="//www.youtube.com/embed/gglkYMGRYlE" width={425} height={350} allowFullScreen />
                                            </div>
                                            <div className="blogInsideBodyTextBlock">
                                                <div className="row">
                                                    <div className="col-xs-12 col-sm-6">
                                                        <p>
                                                            As Lee Gomes pointed out at IEEE Spectrum, this was the most conservative roadmap
                                                            yet offered by Google, which has been operating and tweaking autonomous cars for
                                                            years on private and public roads. If they're saying it's hard, we ought to listen.
                                                        </p>
                                                        <p>
                                                            So what are the big hold-ups, anyway? After watching Urmson's presentation, I called
                                                            two experts — Edwin Olson of the University of Michigan and Nidhi Kalra of the RAND
                                                            Corporation — to dive more into the obstacles that stand between us and our glorious
                                                            self-driving future. None of these things are deal-breakers
                                                        </p>
                                                    </div>
                                                    <div className="col-xs-12 col-sm-6">
                                                        <div className="blogInsideBodyTextBlockInImgWrap">
                                                            <h2>
                                                                First, a quick clarification: Lots of car companies, from GM to BMW to Tesla
                                                            </h2>
                                                            <div className="blogInsideBodyImg">
                                                                <img src="/img/blog_insideCup.png" title />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>
                                                Last month, Google suggested that this driverless utopia may actually be much further away than
                                                many people may realize. In a speech at SXSW in Austin, Google's car project director Chris
                                                Urmson explained that the day when fully autonomous vehicles are widely available, going
                                                anywhere that regular cars can, might be as much as 30 years away. There are still serious
                                                technical and safety challenges to overcome. In the near term, self-driving cars may be limited
                                                to more narrow situations and clearer weather.
                                            </p>
                                            <blockquote>
                                                <p>
                                                    As Lee Gomes pointed out at IEEE Spectrum, this was the most conservative roadmap yet
                                                    offered by Google, which has been operating and tweaking autonomous cars for years on
                                                    private and public roads. If they're saying it's hard, we ought to listen.
                                                </p>
                                            </blockquote>
                                            <p>
                                                So what are the big hold-ups, anyway? After watching Urmson's presentation, I called two experts
                                                — Edwin Olson of the University of Michigan and Nidhi Kalra of the RAND Corporation — to dive
                                                more into the obstacles that stand between us and our glorious self-driving future. None of
                                                these things are deal-breakers per se,
                                            </p>
                                            <div className="blogInsideBodySocial">
                                                <div className="blogInsideBodySocialIn dropdown">
                                                    <a id="blogInsideBodySocialDrop" className="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i className="fnicon-share" />
                                                    </a>
                                                    <ul className="dropdown-menu" aria-labelledby="blogInsideBodySocialDrop">
                                                        <li>
                                                            <a href="#"><i className="fnicon-facebook-f" /></a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><i className="fnicon-twitter" /></a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><i className="fnicon-pinterest" /></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
);




BlogInside.propTypes = {
    blog: React.PropTypes.object,
    id: React.PropTypes.string
};


export default BlogInside;
