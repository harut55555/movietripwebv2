import React from 'react';
import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';
import MapService from '../services/MapService';
const LocationInside = ({location, onFbClick, relatedMovies, relatedScenes, handleImageLoad, handleImageError}) => (
    <div>
        <section className="locationInsideTop">
            <section className="locationInsideTopImg">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                           <div className={"locationInsideTopImgIn"+ ((!location.imgLoaded && !location.imgLoadedError) ? 'loading' : '')
                             + (location.imgLoadedError ? 'notfound' : '')}  style={{'backgroundImage':'url('+ ImgService.getLink(location.coverImg) + ')'}}>
                                        <h1><span>{location.name}</span></h1>
                          </div>
                        </div>

                    </div>
                </div>
            </section>
            <section className="locationInsideTopInfo">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-4">
                            <div className="locationInsideTopInfoText">
                                <i className={'mticon-' + ((location.type !== undefined && location.type !== null) ? location.type.name : '')}></i>
                                <p itemProp="description">{location.description}</p>
                            </div>
                        </div>

                        <div className="col-xs-12 col-sm-4 col-md-push-1 col-lg-push-1">
                            <div className="locationInsideTopInfoAddress">
                                <ul>
                                    <li><strong>City:</strong>{(location.city === undefined || location.city === null)? "":location.city.name}</li>
                                    <li><strong>Country:</strong>{(location.country === undefined || location.city == null)? "":location.country.name}</li>
                                    <li><strong>Location:</strong>{location.name}</li>
                                    <li><strong>Address:</strong>{(location.address === undefined || location.address === null)? "":location.address.name}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4 col-lg-3 col-lg-push-1">
                            <div className="locationInsideTopInfoSocial">
                                <a href="#"><i className="fnicon-pinterest"></i></a>
                                <a onClick={onFbClick} href="#" className="fb">
                                    <i className="fnicon-facebook-f"></i>
                                </a>
                                <a href={'https://twitter.com/intent/tweet?text=' + location.name + '&url=' + window.location.href} target="_blank"><i className="fnicon-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <section className="locationInsideMap">
            <div id="map_canvas">
                <MapService />
            </div>
        </section>
        <section className="relatedWrap">
            {/*<section className="relatedIn relatedMovies">*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*<div className="col-xs-12">*/}
                            {/*here will be facebook comments*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*<div className="col-xs-12">*/}
                            {/*<h2>Related Movies</h2>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                {/*<div className="container">*/}
                    {/*<div className="row">*/}
                        {/*{relatedMovies.map(function(object, i){*/}
                            {/*return  <div className="col-xs-12 col-sm-3" key={i}>*/}
                                {/*<Link to={'/movie/' + relatedMovies[i].id} onLoad={handleImageLoad} onError={handleImageError}>*/}
                                    {/*<figure>*/}
                                        {/*{console.log(relatedMovies[i].coverImg)}*/}
                                        {/*<img src={ImgService.getLink(relatedMovies[i].coverImg)}  title={relatedMovies[i].title}  alt={relatedMovies[i].name} />*/}
                                    {/*</figure>*/}
                                {/*</Link>*/}
                            {/*</div>*/}
                        {/*})}*/}
                    {/*</div>*/}
                {/*</div>*/}
            {/*</section>*/}
            <section className="relatedIn relatedScenes">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            here will be facebook comments
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <h2>Related Scenes</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {relatedScenes.map(function(object, i){
                            return  <div className="col-xs-12 col-sm-3" key={i}>
                                <Link to="#" onLoad={handleImageLoad} onError={handleImageError}>
                                    <figure>
                                        {console.log(relatedScenes[i].coverImg)}
                                        <img src={ImgService.getLink(relatedScenes[i].coverImg)}  title=""  alt="" />
                                        </figure>
                                </Link>
                            </div>
                        })}
                    </div>
                </div>
            </section>
        </section>
    </div>

);

LocationInside.propTypes = {
    locationInside: React.PropTypes.array
};

export default LocationInside;
