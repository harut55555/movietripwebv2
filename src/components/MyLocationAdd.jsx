/**
 * Created by Arsen on 15.03.2017.
 */

import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import MyLocationAddPart from '../components/MyLocationAddPart.jsx';

const MyLocationAdd = ({name,address,city,part,handleChangeAddress,handleChangeName,handleChangeCity,handleImage}) => (
    <div>{  part === true ?
            <MyLocationAddPart name={name} address={address} city={city} part={part} handleChangeName={handleChangeName}
                               handleChangeCity={handleChangeCity} handleChangeAddress={handleChangeAddress}
                               handleImage={handleImage}/>
            :
            <section className="profileInsideBlock profileLocations">
                <div className="row">
                    <div className="col-xs-12">
                        <h2>Add Location</h2>
                    </div>
                </div>
                <div className="row">
                    <MyLocationAddPart name={name} address={address} city={city} part={part} handleChangeName={handleChangeName}
                                       handleChangeCity={handleChangeCity} handleChangeAddress={handleChangeAddress}
                                       handleImage={handleImage}/>
                    <div className="pageNext">
                        <a href="#">Send for approval</a>
                    </div>
                </div>
            </section>
        }
    </div>
);
MyLocationAdd.propTypes = {
    handleChangeAddress: React.PropTypes.func,
    handleChangeName: React.PropTypes.func,
    handleChangeCity: React.PropTypes.func
};


export default MyLocationAdd;