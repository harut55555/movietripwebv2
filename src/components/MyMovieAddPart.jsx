import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';

const MyMovieAddPart = ({name,description,handleChangeName,handleChangeDescription,handleImage}) => (
        <div>
            <div className="col-xs-12">
                <section className="addForm">
                    <form action="">
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>1. Insert first step information</h3>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieName">Name</label>
                                    <input type="text" id="addMovieName" className="mtInput" value={name} onChange={handleChangeName}/>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieGenre">Genre(s)</label>
                                    <div className="mtTags dropdown">
                                        <h2>
                                            <span>Action</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Adventure</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Comedy</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <button className="dropdown-toggle" type="button" id="addMovieGenreDrop" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                            Add Genre
                                        </button>
                                        <ul className="dropdown-menu" aria-labelledby="addMovieGenreDrop">
                                            <li><a className="selected" href="#">Action</a></li>
                                            <li><a className="selected" href="#">Adventure</a></li>
                                            <li><a className="selected" href="#">Comedy</a></li>
                                            <li><button type="button">Drama</button></li>
                                            <li><a href="#">Crime</a></li>
                                            <li><a href="#">Historical</a></li>
                                            <li><a href="#">Horror</a></li>
                                            <li><a href="#">Musical</a></li>
                                            <li><a href="#">Sci-Fi</a></li>
                                            <li><a href="#">War</a></li>
                                            <li><a href="#">Western</a></li>
                                            <li><a href="#">Animation</a></li>
                                            <li><a href="#">Biographical</a></li>
                                            <li><a href="#">Noir</a></li>
                                            <li><a href="#">Thriller</a></li>
                                            <li><a href="#">Fantasy</a></li>
                                            <li><a href="#">Melodrama</a></li>
                                            <li><a href="#">Suspense</a></li>
                                            <li><a href="#">Mystery</a></li>
                                            <li><a href="#">Romance</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieDirectors">Director(s)</label>
                                    <div className="mtTags">
                                        <h2>
                                            <span>Vagharshak Haroyan</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Heriqnaz Stepanyan</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Roberto Carlos</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <label>
                                            <input type="text" placeholder="Add Director" name="addMovieDirectors" id="addMovieDirectors" />
                                            <button type="button"><span>+</span></button>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieActors">Actors</label>
                                    <div className="mtTags">
                                        <h2>
                                            <span>Vagharshak Haroyan</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Heriqnaz Stepanyan</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Roberto Carlos</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <label>
                                            <input type="text" placeholder="Add Actor" name="addMovieActors" id="addMovieActors" />
                                            <button type="button"><span>+</span></button>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieDescription">Description</label>
                                    <textarea name="addMovieDescription" id="addMovieDescription" className="mtInput" cols =" " rows={6} value={description} onChange={handleChangeDescription}/>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12">
                                <h3>2. Insert second step information</h3>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieCountry">Country</label>
                                    <div className="mtTags dropdown">
                                        <h2>
                                            <span>Aruba</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Antigua and Barbuda</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>United Arab Emirates</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <button className="dropdown-toggle" type="button" id="addMovieCountryDrop" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                            Add Country
                                        </button>
                                        <ul className="dropdown-menu" aria-labelledby="addMovieCountryDrop">
                                            <li><a className="selected" href="#">Aruba</a></li>
                                            <li><a className="selected" href="#">Antigua and Barbuda</a></li>
                                            <li><a className="selected" href="#">United Arab Emirates</a></li>
                                            <li><a href="#">Afghanistan</a></li>
                                            <li><a href="#">Algeria</a></li>
                                            <li><a href="#">Azerbaijan</a></li>
                                            <li><a href="#">Albania</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-2">
                                <div className="form-group">
                                    <label htmlFor="addMovieYear">Year</label>
                                    <input type="number" id="addMovieYear" className="mtInput" min={1896} max={2017} placeholder={2017} />
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-10">
                                <div className="form-group">
                                    <label htmlFor="addMovieTags">Tags</label>
                                    <div className="mtTags">
                                        <h2>
                                            <span>Tag 1</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Tag 3</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <h2>
                                            <span>Al Pacino</span>
                                            <a href="#"><i className="fnicon-close" /></a>
                                        </h2>
                                        <label>
                                            <input type="text" placeholder="Add Tag" name="addMovieTags" id="addMovieTags" />
                                            <button type="button"><span>+</span></button>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12">
                                <div className="form-group">
                                    <label htmlFor="addMovieImage">Image</label>
                                    <div className="mtImage mtMovieImage">
                                        <input type="file" id="addMovieImage" className="hidden" hidden="" onChange={handleImage} />
                                        <div className="mtImageLeft">
                                            <div className="mtImageLeftIn">
                                                <div className="mtImageLeftInside">
                                                    <h2>No image</h2>
                                                    <figure>
                                                        <img id="imga" src="/img/frontend/movie-1.jpg" alt="" />
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mtImageRight">
                                            <div className="mtImageRightIn">
                                                <div className="mtImageRightInside">
                                                    <label htmlFor="addMovieImage">
                                                        <span>Upload image</span>
                                                    </label>
                                                    <span>or select from <a href="#">existing</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        
);


MyMovieAddPart.propTypes = {
    handleChangeName: React.PropTypes.func,
    handleChangeDescription: React.PropTypes.func
};

export default MyMovieAddPart;