import React from 'react';
import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login';

const Base = ({ children, isAuthenticated, onSearch, handleLogin, handleLogout, handlePasswordChange, handleEmailChange, statistics, user,responseFacebook }) => (
  <div>
      <header>
          <div className="container">
              <div className="row">
                  <div className="headerCenter col-xs-12">
                      <div className="headerLeft">
                          {/*<!-- Header Logo -->*/}
                          <div className="headerLogo">
                              <Link to="/">
                                  <figure>
                                      <img src="/img/movietrip-logo.png" alt="MovieTrip" title="MovieTrip" />
                                  </figure>
                              </Link>
                          </div>
                          {/*<!-- Header Logo End -->*/}
                      </div>
                      <div className="headerRight">
                          <div className="headerRightIn">
                              {/*<!-- Header Menu -->*/}
                              <div className="headerMenu dropdown">
                                  <Link to="#" className="dropdown-toggle hidden-sm hidden-md hidden-lg"  id="headerMenuDrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <span></span>
                                      <span></span>
                                      <span></span>
                                  </Link>
                                  <ul className="dropdown-menu" aria-labelledby="headerMenuDrop">
                                      <li><Link to="/scenes">Scenes</Link></li>
                                      <li><Link to="/locations">Locations</Link></li>
                                      <li><Link to="/movies">Movies</Link></li>
                                      <li><Link to="/map">Map</Link></li>
                                      <li><Link to="/trips">Trips</Link></li>
                                      <li><Link to="/blog">Blog</Link></li>
                                      {
                                          (function() {
                                              if(isAuthenticated) {
                                                  return <li><Link to="/my">My</Link></li>;
                                              }
                                          })()
                                      }
                                  </ul>
                              </div>
                              {/*<!-- Header Menu end -->*/}
                              {/*<!-- Header User -->*/}
                              <div className="headerUserBlock dropdown">
                                  <Link to="#" className="headerMenuUser" id="headerMenuLoginDrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></Link>
                                  <ul className="dropdown-menu dropdown-menu-right" aria-labelledby="headerMenuLoginDrop">
                                      <li>
                                          <div className="headerLogin">
                                              <form onSubmit={handleLogin}>
                                                  {
                                                      isAuthenticated ? (
                                                          <div>
                                                              <div className="headerLoginTitle">
                                                                  <h2>Welcome {user.name}!</h2>
                                                              </div>
                                                              <div className="headerLoginBtns">
                                                                  <div className="headerLoginBtnsIn">
                                                                      <div className="headerLoginBtnsBottom">
                                                                          <a href="#" onClick={handleLogout}>Sign Out</a>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div> )
                                                          : (
                                                              <div>
                                                                  <div className="headerLoginTitle">
                                                                      <h2>Login to <strong>MovieTrip</strong></h2>
                                                                  </div>
                                                                  <div className="headerLoginForms">
                                                                      <label>
                                                                          <input type="email" onKeyUp={handleEmailChange} onChange={handleEmailChange} placeholder="Email" id="loginEmail" name="loginEmail" required=""/>
                                                                      </label>
                                                                      <label>
                                                                          <input type="password" onKeyUp={handlePasswordChange} onChange={handlePasswordChange} placeholder="Password" id="loginPass" name="loginPass" required=""/>
                                                                      </label>
                                                                  <div className="headerLoginToolbar">
                                                                      <div className="styledCheckbox">
                                                                          <label>
                                                                              <input type="checkbox" id="loginRemember" name="loginRemember"/>
                                                                              {/*<!--span>Remember me</span-->*/}
                                                                          </label>
                                                                      </div>
                                                                      <div className="loginProblemsLink">
                                                                          {/*<!--a to="#">Login Problems</a-->*/}
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div className="headerLoginBtns">
                                                                  <div className="headerLoginBtnsIn">
                                                                      <div className="headerLoginBtnsTop">
	                                                                        {/*<!--<Link to="#"><i className="fnicon-facebook"></i></Link>-->*/}
		                                                                      <FacebookLogin
		                                                                       appId="165399480626657"
		                                                                       autoLoad={false}
		                                                                       reAuthenticate={false}
		                                                                       fields="name,email,picture"
		                                                                       size=""
		                                                                       textButton={""}
		                                                                       icon={<i className="fnicon-facebook"></i>}
		                                                                       callback={responseFacebook}
		                                                                       cssClass="MT-FB-Btn"
		                                                                       />
	                                                                      <input value="Sign In" type="submit"/>
                                                                      </div>
                                                                      <div className="headerLoginBtnsBottom">
                                                                          <Link to="/user/register">Sign Up</Link>
                                                                      </div>
                                                                  </div>
                                                              </div>

                                                          </div>
                                                      )
                                                  }
                                              </form>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                              {/*<!-- Header User End -->*/}
                              
                              {/*<!-- Header Search -->*/}
                              <div className="headerSearch">
                                  <form method="get" action="" onSubmit={onSearch}>
                                      <div className="headerSearchInner">
                                          <label htmlFor="mtSearchInput"></label>
                                          <input id="mtSearchInput1" type="search" placeholder="Type your search here" name="search" />
                                      </div>
                                      <button type="submit" id="mtSearchSubmit"><i className="fnicon-search"></i></button>
                                  </form>
                              </div>
                              {/*<!-- Header Search End -->*/}
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </header>
    { /* child component will be rendered here */ }
      {console.log(children)}
    {children}
      <footer>
          <section className="footerTop">
              <div className="container">
                  <div className="row">
                      <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                          <div className="footerTopCounter locationsCounter">
                            <h2><span>{statistics[0]}</span></h2>
                            <h3>Locations</h3>
                          </div>
                      </div>
                      <div className="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                          <div className="footerTopCounter moviesCounter">
                              <h2><span>{statistics[1]}</span></h2>
                              <h3>Movies</h3>
                          </div>
                      </div>
                      <div className="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                          <div className="footerTopCounter scenesCounter">
                              <h2><span>{statistics[2]}</span></h2>
                              <h3>Scenes</h3>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
          <section className="footerBottom">
              <div className="container">
                  <div className="row">
                      <div className="col-xs-12 col-sm-4">
                          <div className="footerBottomIn">
                              <ul>
                                  <li><Link to="/scenes">Scenes</Link></li>
                                  <li><Link to="/locations">Locations</Link></li>
                                  <li><Link to="/movies">Movies</Link></li>
                                  <li><Link to="/map">Map</Link></li>
                                  <li><Link to="/trips">Trips</Link></li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-xs-12 col-sm-4">
                          <div className="footerBottomIn">
                              <ul>
                                  <li><Link to="/ourstaff">Our staff</Link></li>
                                  <li><Link to="/policy">Privacy Policy</Link></li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-xs-12 col-sm-4">
                          <div className="footerBottomIn">
                              <div className="footerSocial">
                                  <ul>
                                      <li><Link to="https://www.facebook.com/MovieTripApp" target="_blank"><i className="fnicon-facebook-f"></i></Link></li>
                                      <li><Link to="https://www.twitter.com/MovieTripApp" target="_blank"><i className="fnicon-twitter"></i></Link></li>
                                      <li><Link to="https://www.instagram.com/movietripapp/" target="_blank"><i className="fnicon-instagram"></i></Link></li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      </footer>
      <script>

      </script>
  </div>
);

Base.propTypes = {
    children: React.PropTypes.object,
    onSubmit: React.PropTypes.func
};

export default Base;
