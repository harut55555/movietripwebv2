import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import MapService from '../services/MapService';
const MyLocationAddPart = ({name, address, city, part,handleChangeName, handleChangeAddress,handleChangeCity,handleImage}) => (
    <div className="col-xs-12">
        <section className="addForm">
            <form action="">
                <div className="row">
                    <div className="col-xs-12">
                        <h3>1. Insert first step information</h3>
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="addLocationName">Name</label>
                            <input type="text" id="addLocationName" className="mtInput" value={name} onChange={handleChangeName} />
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="addLocationType">Type</label>
                            <div className="mtSelect dropdown">
                                <button className="dropdown-toggle" type="button" id="addLocationTypeDrop" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    Select type
                                </button>
                                <ul className="dropdown-menu" aria-labelledby="addLocationTypeDrop">
                                    <li><a className="selected" href="#">Action</a></li>
                                    <li><button type="button">Drama</button></li>
                                    <li><a href="#">Crime</a></li>
                                    <li><a href="#">Historical</a></li>
                                    <li><a href="#">Horror</a></li>
                                    <li><a href="#">Musical</a></li>
                                    <li><a href="#">Sci-Fi</a></li>
                                    <li><a href="#">War</a></li>
                                    <li><a href="#">Western</a></li>
                                    <li><a href="#">Animation</a></li>
                                    <li><a href="#">Biographical</a></li>
                                    <li><a href="#">Noir</a></li>
                                    <li><a href="#">Thriller</a></li>
                                    <li><a href="#">Fantasy</a></li>
                                    <li><a href="#">Melodrama</a></li>
                                    <li><a href="#">Suspense</a></li>
                                    <li><a href="#">Mystery</a></li>
                                    <li><a href="#">Romance</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="addLocationCountry">Country</label>
                            <div className="mtSelect dropdown">
                                <button className="dropdown-toggle" type="button" id="addLocationCountryDrop" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    Select country
                                </button>
                                <ul className="dropdown-menu" aria-labelledby="addLocationCountryDrop">
                                    <li><a className="selected" href="#">Action</a></li>
                                    <li><button type="button">Drama</button></li>
                                    <li><a href="#">Crime</a></li>
                                    <li><a href="#">Historical</a></li>
                                    <li><a href="#">Horror</a></li>
                                    <li><a href="#">Musical</a></li>
                                    <li><a href="#">Sci-Fi</a></li>
                                    <li><a href="#">War</a></li>
                                    <li><a href="#">Western</a></li>
                                    <li><a href="#">Animation</a></li>
                                    <li><a href="#">Biographical</a></li>
                                    <li><a href="#">Noir</a></li>
                                    <li><a href="#">Thriller</a></li>
                                    <li><a href="#">Fantasy</a></li>
                                    <li><a href="#">Melodrama</a></li>
                                    <li><a href="#">Suspense</a></li>
                                    <li><a href="#">Mystery</a></li>
                                    <li><a href="#">Romance</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <div className="form-group">
                            <label htmlFor="addLocationCity">City</label>
                            <input type="text" id="addLocationCity" className="mtInput" value={city} onChange={handleChangeCity} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <h3>2. Insert second step information</h3>
                    </div>
                    <div className="col-xs-12">
                        <div className="form-group">
                            <label htmlFor="addLocationAddress">Address</label>
                            <input type="text" id="addLocationAddress" className="mtInput" value={address} onChange={handleChangeAddress} />
                        </div>
                    </div>
                    <div className="col-xs-12">
                        <div className="form-group">
                            <div className="mapWrap">
                                <div id="map_canvas">
                                    <MapService />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12">
                        <div className="form-group">
                            <label htmlFor="addLocationImage">Image</label>
                            <div className="mtImage">
                                <input type="file" id="addLocationImage" className="hidden" hidden/>
                                <div className="mtImageLeft">
                                    <div className="mtImageLeftIn">
                                        <div className="mtImageLeftInside">
                                            <h2>No image</h2>
                                            <figure>
                                                <img src="/img/frontend/img-3.png" alt="" />
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                                <div className="mtImageRight">
                                    <div className="mtImageRightIn">
                                        <div className="mtImageRightInside">
                                            <label htmlFor="addLocationImage">
                                                <span>Upload image</span>
                                            </label>
                                            <span>or select from <a href="#">existing</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>

);




export default MyLocationAddPart;