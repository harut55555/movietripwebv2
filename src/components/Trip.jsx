/**
 * Created by Arsen on 06.03.2017.
 */
import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';
import MapService from '../services/MapService';
import Config from '../Config.jsx'
const Trip = ({trip, id}) => {
    
  let path=Config.getPath().imagehost; 
    let obj;
    let icon;
    return (
    <div>
        {console.log("trip===",trip)}
        <section className="tripInsideTop">
            <section className="tripInsideTopImg">
                <div className="container">
                    <div className="row">
                              <div >
                                    <div className="col-xs-12">
                                        <div className="tripInsideTopImgIn">
                                            <i></i>
                                            <img src={ImgService.getLink(trip.coverImg)} alt="" title="" />
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </section>
            <section className="tripInsideTopText">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-1">
                            <div className="tripInsideTopUser">
                                <a href="#" style={{'backgroundImage':' url(/img/user-empty.png)'}}/>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-7">
                            <div className="tripInsideTopDescription">
                                <h2>'When Harry Met Sally' - 7 locations to see in New York</h2>
                                <p>A little bit long, but 100% worth visiting - enjoy 'When Harry Met Sally' movie trip in New York!</p>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4 col-lg-3 col-lg-push-1">
                            <div className="tripInsideTopSocial">
                                <a href="#"><i className="fnicon-facebook-f"></i></a>
                                <a href="#"><i className="fnicon-twitter"></i></a>
                                <a href="#"><i className="fnicon-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>{trip.trip_point!==undefined?
        <section className="tripInsideBody">
            
            {trip.trip_point.map(function(object, i){
       
                return    <div className="tripInsidePoint" key={i}>
                    <section className="tripInsidePointImg">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12">
                                  
                                    {console.log("trip.trip_point[i].scenes.coverImg=",trip.trip_point[i].scenes[0].coverImg," trip.trip_point[i].location.coverImg= ",trip.trip_point[i].locations)}
                                    {console.log("bfvbgv=",trip.trip_point[i].scenes[0].coverImg.bigPath)}
                                    {trip.trip_point[i].locations===undefined ||trip.trip_point[i].locations[0]===undefined ? (trip.trip_point[i].scenes[0].location.type===null ?
                                            (obj=trip.trip_point[i].scenes[0], icon="other" ):

                                            (obj= trip.trip_point[i].scenes[0], icon=trip.trip_point[i].scenes[0].location.type.name )
                                    )
                                        : (obj=trip.trip_point[i].locations[0], icon=trip.trip_point[i].locations[0].location.type.name)}
                                    {console.log("image=",icon)}
                                    <img src={ImgService.getLink(obj.coverImg)} alt="Trip" title="Trip" />

                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="tripInsidePointText">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12 col-sm-1">
                                    <div className="tripInsidePointIcon">
                                        <i className={"mticon-" + icon}></i>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-10">
                                    <div className="tripInsidePointDescription">
                                        <h3>{trip.trip_point[i].description} <br /> Katz's Delicatessen</h3>
                                        <h4>{obj.address.name} </h4>
                                        <p>{trip.title}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            })}

          
        </section>
        : <div></div>}
    </div>
)};

Trip.propTypes = {
    trip: React.PropTypes.array,
    id: React.PropTypes.string
};


export default Trip;