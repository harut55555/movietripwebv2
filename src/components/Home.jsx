import React from 'react';
import {Link, IndexLink} from 'react-router-dom';
import ImgService from '../services/ImgService.jsx'

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const HomePage = ({home}) => (
    <div>
        <section className="homeSecondCover">
            <div className="homeSecondCoverSwiper swiper-container">
                <div className="swiper-wrapper">
                    {home.topSlider.map(function (object, i) {
                        return <div className="swiper-slide" key={i}>
                            <a href={home.topSlider[i].link} target={home.topSlider[i].target_name}>
                                <img src={ImgService.getLink(home.topSlider[i].cover)} alt={home.topSlider[i].status}
                                     title={home.topSlider[i].status}/>
                            </a>
                        </div>
                    })}
                </div>
                <div className="swiper-pagination"></div>
            </div>
        </section>
        {/*<!-- Homepage Slider End -->*/}
        {/*<!-- Homepage Popular -->*/}
        <section className="homePopular">
            <section className="homePopularTop">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-4">
                            <div className="homePopularPromo">
                                <ul>
                                    <li><h3>Latest Movies</h3></li>
                                    {home.topLatestBlock.movies.map(function (object, i) {
                                        return <li key={i}><Link
                                            to={"/movie/" + home.topLatestBlock.movies[i].id }>{(home.topLatestBlock.movies[i] === undefined || home.topLatestBlock.movies[i] === null) ? '' : home.topLatestBlock.movies[i].name}</Link>
                                        </li>
                                    })}
                                </ul>
                                <ul>
                                    <li><h3>Latest Locations</h3></li>
                                    {home.topLatestBlock.locations.map(function (object, i) {
                                        return <li key={i}><Link
                                            to={"/location/" + home.topLatestBlock.locations[i].id }>{(home.topLatestBlock.locations[i] === undefined || home.topLatestBlock.locations[i] === null) ? '' : home.topLatestBlock.locations[i].name}</Link>
                                        </li>
                                    })}
                                </ul>
                                <div className="badgeBlock"></div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-8">
                            <div className="row">
                                <div className="col-xs-12 col-sm-6">
                                    <div itemScope itemType={'http://schema.org/Place'}
                                         className="promoBlock locationBlock"
                                         style={{'backgroundImage': 'url(' + ImgService.getLink(home.promoBlockLocation.coverImg) + ')'}}>
                                        <Link to="locations"
                                              className="promoBlock-topLink"><span>Locations</span></Link>
                                        <span className="promoBlockAuthor">by <Link
                                            to="#">{home.promoBlockLocation.user_name}</Link></span>
                                        <Link to={"/location/" + home.promoBlockLocation.id} className="promoBlockInfo">
                                            <div>
                                                <h3>{(home.promoBlockLocation === undefined || home.promoBlockLocation === null) ? '' : home.promoBlockLocation.name}</h3>
                                                <h4>{(home.promoBlockLocation.address === undefined || home.promoBlockLocation.address === null ) ? '' : home.promoBlockLocation.address.name}</h4>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-6">
                                    <div className="promoBlock sceneBlock"
                                         style={{'backgroundImage': 'url(' + ImgService.getLink(home.promoBlockSceneFirst.coverImg) + ')'}}>
                                        <Link to="scenes" className="promoBlock-topLink"><span>Scenes</span></Link>
                                        <Link to={"/scene/" + home.promoBlockSceneFirst.id} className="promoBlockInfo">
                                            <div>
                                                <h3>{home.promoBlockSceneFirst.title}</h3>
                                                <h4>{home.promoBlockSceneFirst.brief}</h4>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="promoBlock sceneBlock"
                                         style={{'backgroundImage': 'url(' + ImgService.getLink(home.promoBlockSceneSecond.coverImg) + ')'}}>
                                        <Link to="scenes" className="promoBlock-topLink"><span>Scenes</span></Link>
                                        <Link to={"/scene/" + home.promoBlockSceneSecond.id} className="promoBlockInfo">
                                            <div>
                                                <h3>{home.promoBlockSceneSecond.title}</h3>
                                                <h4>{home.promoBlockSceneSecond.brief}</h4>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="homePopularBottom">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-8">
                            <div className="row">
                                <div className="col-xs-12 col-sm-6">
                                    <div className="quoteBlock" style={{'backgroundColor': '#d5d477'}}>
                                        <div className="quoteBlockText">
                                            {/*<!--<img src="/img/frontend/quote-img.png" alt="" title="">-->*/}
                                            <h3>{home.middleBlockText.title}</h3>
                                            <h4>{home.middleBlockText.text}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-sm-6">
                                    <div className="promoBlock sceneBlock"
                                         style={{'backgroundImage': 'url(' + ImgService.getLink(home.middleBlockSceneFirst.coverImg) + ')'}}>
                                        <Link to="scenes" className="promoBlock-topLink"><span>Scenes</span></Link>
                                        <Link to={"/scene/" + home.middleBlockSceneFirst.id} className="promoBlockInfo">
                                            <div>
                                                <h3>{home.middleBlockSceneFirst.title}</h3>
                                                <h4>{home.middleBlockSceneFirst.brief}</h4>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="promoBlock sceneBlock"
                                         style={{'backgroundImage': 'url(' + ImgService.getLink(home.middleBlockSceneSecond.coverImg) + ')'}}>
                                        <Link to="scenes" className="promoBlock-topLink"><span>Scenes</span></Link>
                                        <Link to={"/scene/" + home.middleBlockSceneSecond.id}
                                              className="promoBlockInfo">
                                            <div>
                                                <h3>{home.middleBlockSceneSecond.title}</h3>
                                                <h4>{home.middleBlockSceneSecond.brief}</h4>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4">
                            <div className="homePopularCousine">
                                <div className="homePopularCousineTop">
                                    <div className="homePopularCousineTopIn">
                                        <i className="mticon-cafe"/>
                                    </div>
                                </div>
                                <div className="homePopularCousineBottom">
                                    <ul>
                                        {home.middleBlockLocations.map(function (object, i) {
                                            return <li key={i}><Link
                                                to={"/location/" + home.middleBlockLocations[i].id}>{(home.middleBlockLocations[i] === undefined || home.middleBlockLocations[i] === null) ? '' : home.middleBlockLocations[i].name}</Link>
                                            </li>
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        {/*<!-- Homepage Popular End -->*/}
        {/*<!-- Home Movies -->*/}
        <section className="homeMovies">
            <div className="container">
                <div className="row">
                    {home.bottomUpMovies.map(function (object, i) {
                        return <div className="col-xs-12 col-sm-3" key={i}>
                            <Link to={"/movie/" + home.bottomUpMovies[i].id} className="movieThumb">

                                <figure><img src={home.bottomUpMovies[i].coverImg}
                                             alt={(home.bottomUpMovies[i] === undefined || home.bottomUpMovies[i] === null) ? '' : home.bottomUpMovies[i].name}
                                             title={(home.bottomUpMovies[i] === undefined || home.bottomUpMovies[i] === null) ? '' : home.bottomUpMovies[i].name}/>
                                </figure>
                            </Link>
                        </div>
                    })}
                </div>
            </div>
        </section>
        {/*<!-- Home Movies End -->*/}
        {/*<!-- Home Special Locations -->*/}
        <section className="homeSpecialLocations">
            <div className="container">
                <div className="row">
                    {home.bottomTrips.map(function (object, i) {
                        return <div className="col-xs-12 col-sm-3" key={i}>
                            <Link to={"/trip/" + home.bottomTrips[i].id} className="specialLocation">
                                <div className="specialLocationType"/>
                                <div className="specialLocationBg"
                                     style={{'backgroundImage': 'url(' + ImgService.getLink(home.bottomTrips[i].coverImg) + ')'}}/>
                                <div className="specialLocationCaption">
                                    <h3>{home.bottomTrips[i].title}</h3>
                                    <h4>{home.bottomTrips[i].description}</h4>
                                </div>
                            </Link>
                        </div>
                    })}
                </div>
            </div>
        </section>
        {/*<!-- Home Special Locations End -->*/}
        <section className="homePopularSecond">
            <section className="homePopularSecondTop">
                <div className="container">
                    <div className="row">
                        { home.bottomLocations.First.map(function (object, i) {
                            return <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6" key={i}>
                                <div className="promoBlock locationBlock"
                                     style={{'backgroundImage': 'url(' + ImgService.getLink(home.bottomLocations.First[i].coverImg) + ')'}}>
                                    <Link to="locations" className="promoBlock-topLink"><span>Locations</span></Link>
                                    <span className="promoBlockAuthor">by <Link
                                        to="#">{home.bottomLocations.First[i].user_name}</Link></span>
                                    <Link to={"/location/" + home.bottomLocations.First[i].id}
                                          className="promoBlockInfo">
                                        <div>
                                            <h3>{(home.bottomLocations.First[i] === undefined || home.bottomLocations.First[i] === null) ? '' : home.bottomLocations.First[i].name}</h3>
                                            <h4>{(home.bottomLocations.First[i].address === undefined || home.bottomLocations.First[i].address === null) ? '' : home.bottomLocations.First[i].address.name}</h4>
                                        </div>
                                    </Link>
                                </div>
                            </div>

                        })}

                    </div>
                </div>
            </section>
            <section className="homePopularSecondBottom">
                <div className="container">
                    <div className="row">
                        { home.bottomLocations.Second.map(function (object, i) {
                            return <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6" key={i}>
                                <div className="promoBlock locationBlock"
                                     style={{'backgroundImage': 'url(' + ImgService.getLink(home.bottomLocations.Second[i].coverImg) + ')'}}>
                                    <Link to="locations" className="promoBlock-topLink"><span>Locations</span></Link>
                                    <span className="promoBlockAuthor">by <Link to="#">Vincent Vega</Link></span>
                                    <Link to={"/location/" + home.bottomLocations.Second[i].id}
                                          className="promoBlockInfo">
                                        <div>
                                            <h3>{(home.bottomLocations.Second[i] === undefined || home.bottomLocations.Second[i] === null) ? '' : home.bottomLocations.Second[i].name}</h3>

                                            <h4>{(home.bottomLocations.Second[i].address === undefined || home.bottomLocations.Second[i].address === null) ? '' : home.bottomLocations.Second[i].address.name}</h4>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        })}
                    </div>
                </div>
            </section>
        </section>
    </div>
);

export default HomePage;
