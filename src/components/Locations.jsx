import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';

const Locations = ({locations,handleMore}) => (
    <div>
        <section className="scenesInside">
            <div className="container">
                <div className="row">
                    {locations.map(function(object, i){
                        return  <div className="col-xs-12 col-sm-4" key={i}>
                            <div className={"promoBlock locationInsideBlock " + ((!locations[i].imgLoaded && !locations[i].imgLoadedError) ? 'loading' : '')
                             + (locations[i].imgLoadedError ? 'notfound' : '')}
                                 style={{'backgroundImage':'url('+ ImgService.getLink(locations[i].coverImg) + ')'}} >
                                <Link to={'location/' + locations[i].id}>
                                    <h3>{locations[i].name}</h3>
                                    <div>
                                        <h4>{locations[i].country.name}</h4>
                                        <i className={'mticon-' + locations[i].type.name}/>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    })}
                    <div className="pageNext">
                        <button className="moreButton" onClick={handleMore}>More</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
);


Locations.propTypes = {
    locations: React.PropTypes.array
};

export default Locations;
