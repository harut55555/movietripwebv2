import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import MyMovieAddPart from '../components/MyMovieAddPart.jsx';

const MyMovieAdd = ({name,description,part,handleChangeName,handleChangeDescription,handleImage}) => (
    <div>{  part===true?
        <MyMovieAddPart name={name} description={description} handleChangeName={handleChangeName} handleChangeDescription={handleChangeDescription} handleImage={handleImage}/>
        :
        <section className="profileInsideBlock profileMovies">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Add Movie</h2>
                </div>
            </div>
            <div className="row">
                <MyMovieAddPart name={name} description={description} handleChangeName={handleChangeName} handleChangeDescription={handleChangeDescription} handleImage={handleImage}/>
                <div className="pageNext">
                    <a href="/my/movies">Send for approval</a>
                </div>
            </div>
        </section>
}
    </div>
);

MyMovieAdd.propTypes = {
    handleChangeName: React.PropTypes.func,
    handleChangeDescription: React.PropTypes.func,
    handleImage:React.PropTypes.func
};


export default MyMovieAdd;