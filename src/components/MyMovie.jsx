import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';

const MyMovie = ({mymovies,addClick,edithandleClick,deletehandleClick }) => (
    <div>
        <section className="profileInsideBlock profileMovies">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Movies</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-4 col-md-3">
                    <Link to={"/my/movies/add"} className="movieThumb moviesFilter"  onClick={addClick}>
                        <div>
                            <h5> Add Movie </h5>
                        </div>
                    </Link>
                </div>
                <div className="col-xs-12 col-sm-4 col-md-3" >
                        <div className="thumbBtns">
                            <a className="editThumb" href="/my/movies/add" title="Edit" onClick={edithandleClick}><i className="fnicon-edit" /></a>
                            <a className="deleteThumb" href="#" title="Delete" onClick={deletehandleClick}><i className="fnicon-close" /></a>
                        </div>
                        <a href="movie-inside.html" className="movieThumb">
                            <figure>
                                <img src="/img/frontend/movie-1.jpg" alt=""/>
                            </figure>
                        </a>
                        <div className="profilePostStatus approved">
                            <h2>Approved</h2>
                        </div>
                    </div>
                <div className="pageNext">
                    <a href="#">Load More</a>
                </div>
            </div>
        </section>
    </div>
);
MyMovie.propTypes = {
    movies: React.PropTypes.array,
    deletehandleClick: React.PropTypes.func,
    edithandleClick: React.PropTypes.func,
    addClick: React.PropTypes.func
};


export default MyMovie;