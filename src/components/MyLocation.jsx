import React from 'react';
import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';

const MyLocation = ({mylocations,addClick,edithandleClick,deletehandleClick}) => (
    <div>
        <section className="profileInsideBlock profileLocations">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Locations</h2>
                </div>
            </div>
            <div className="row" >
                <div className="col-xs-12 col-sm-4" >
                    <div className="promoBlock createBlock">
                        <Link to="/my/locations/add" onClick={addClick}>
                            <h5>Add Location</h5>
                        </Link>
                    </div>
                </div>
            {mylocations.map(function(object, i){
                return <div className="col-xs-12 col-sm-4" key={i}>
                        <div className="thumbBtns">
                            <a className="editThumb" href="add" title="Edit" onClick={edithandleClick}><i className="fnicon-edit" /></a>
                            <a className="deleteThumb" href="#" title="Delete" onClick={deletehandleClick}><i className="fnicon-close" /></a>
                        </div>
                        <div className="promoBlock locationInsideBlock" style={{backgroundImage: 'url('+ mylocations[i].image +')'}}>

                            <a href={'locations/' + mylocations[i].id}>
                                <h3>{mylocations[i].h3}</h3>
                                <div>
                                    <h4>{mylocations[i].h4}</h4>
                                    <i className="mticon-bar" />
                                </div>
                            </a>
                        </div>
                        <div className="profilePostStatus rejected">
                            <h2>{mylocations[i].h2}</h2>
                        </div>
                    </div>
                    
               
            })}
                <div className="pageNext">
                    <a href="#">View All</a>
                </div>
            </div>
        </section>
    </div>
);




MyLocation.propTypes = {
    movies: React.PropTypes.array,
    deletehandleClick: React.PropTypes.func,
    edithandleClick: React.PropTypes.func,
    addClick: React.PropTypes.func
};

  
export default MyLocation;