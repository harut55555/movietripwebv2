/**
 * Created by Arsen on 16.02.2017.
 */
import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';
import ImgService from '../services/ImgService';

const Movies = ({movies, handleMore, handleImageLoad, handleImageError}) => (
    <div>
        <section className="moviesInside">
            <div className="container">
                <div className="row">
                    {movies.map(function(object, i){
                        return <div className="col-xs-12 col-sm-4 col-md-3" key={i}>
                            <Link to={"/movie/" + movies[i].id} title={movies[i].name} className="movieThumb loading" onLoad={handleImageLoad} onError={handleImageError}>
                                <figure>
                                    <img src={ImgService.getLink(movies[i].coverImg)} title={movies[i].name}  alt={movies[i].name}/>
                                </figure>
                                <h2>{movies[i].name}</h2>
                            </Link>
                        </div>
                    })}
                    <div className="pageNext">
                        <button className="moreButton" onClick={handleMore}>More</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
);

Movies.propTypes = {
    movies: React.PropTypes.array
};

export default Movies;