import React from 'react';import PropTypes from 'prop-types';
import { Link, IndexLink } from 'react-router';

const MyScene = ({myscenes,addClick,edithandleClick,deletehandleClick}) => (
    <div>
        <section className="profileInsideBlock profileScenes">
            <div className="row">
                <div className="col-xs-12">
                    <h2>Scenes</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-4">
                    <div className="promoBlock createBlock">
                        <a href="scenes/add" onClick={addClick}>
                            <h5>Add Scene</h5>
                        </a>
                    </div>
                </div>
                <div className="col-xs-12 col-sm-4">
                    <div className="thumbBtns">
                        
                       
                        <a href="add" className="editThumb"  title="Edit"  onClick={edithandleClick}><i className="fnicon-edit" /></a>
                        <a href="#" className="deleteThumb"  title="Delete" onClick={deletehandleClick}><i className="fnicon-close" /></a>
                    </div>
                    <div className="promoBlock sceneBlock" style={{backgroundImage: 'url(/img/frontend/img-1.jpg)'}}>
                        <a href="scene-inside.html" className="promoBlockInfo">
                            <div>
                                <h3>
                                    Sherlock season 3 (2014)
                                </h3>
                                <h4>The Packet Hotel</h4>
                            </div>
                        </a>
                    </div>
                    <div className="profilePostStatus pending">
                        <h2>Pending</h2>
                    </div>
                </div>
                <div className="pageNext">
                    <a href="#">View All</a>
                </div>
            </div>
        </section>
    </div>
);




MyScene.propTypes = {
    movies: React.PropTypes.array,
    deletehandleClick: React.PropTypes.func,
    edithandleClick: React.PropTypes.func,
    addClick: React.PropTypes.func
};


export default MyScene;