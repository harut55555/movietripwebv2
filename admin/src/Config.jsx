import React from 'react';
import Auth from './modules/Auth';

class Config {

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    static routes = {

    };

    static isProd() {
        return false;
    }
    static getHostname(){
        return (this.isProd() ? location.hostname : '13.81.67.155');
    }

    static getPort() {
        return 9090;
    }

    static getProtocol(){
        return (this.isProd() ? location.protocol : 'http:');
    }

    static getLimit(){
        return {
            movies: 10,
            scenes: 16,
            locations: 16,
            trips: 12,
            blogs:16
        }
    }

    static getPath(){

        let host = Config.getProtocol() + '//' + Config.getHostname() + ':' +  Config.getPort();
        let api = host + '/api/admin/';
        return {
            'movie': api + 'movie',
            'scene': api + 'scene',
            'location': api + 'location',
            'trip': api + 'trip',
            'post': api + 'post',
            'page': api + 'page',
            'user': api + 'user',
            'geo': api + 'geo',
            'baseRoute': api,
            'tag': api + 'tag',
            'mediaupload': host + '/media/upload/',
            'mediadelete': host + '/media/delete/',
            'mapAddressSearch': "https://maps.googleapis.com/maps/api/geocode/json?address=",
            'login': api + 'auth/login',
            'dashboardCount': api + "dashboard/getcount",
            'userProfile': api + "userprofile",
            'mediaGetById': "media"
        };
        
    }
    static getError(num) {

        let errors = {
            500: 'there was some serious problem, please contact to admin'
        };

        return errors[num];
    }
}

export default Config;
