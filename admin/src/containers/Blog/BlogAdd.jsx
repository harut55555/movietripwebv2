import React from 'react';
import Auth from '../../modules/Auth';
import BlogAddComponent from '../../components/Blog/BlogAddComponent.jsx';
 
class BlogAdd extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Add Blog Post","/blog/add","Blog","/pages/index"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		$("#blogPostDate").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			timePicker: true,
			timePicker24Hour: true,
			timePickerIncrement: 30,
			locale: {
				format: "YYYY/MM/DD HH:mm"
			},
			"buttonClasses": "btn btn-sm btn-flat",
			"applyClass": "color-success-bg color-success-hover-bg color-text-white",
			"cancelClass": "btn-default"
		});

		tinymce.init({
			selector: "#blogPostEditor",
			theme: "modern",
			height: 250,
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons"
		});

		$("#blogPostTags").selectize({
			plugins: ["remove_button", "drag_drop"],
			delimiter: ",",
			persist: false,
			create: function (input) {
				return {
					value: input,
					text: input
				}
			}
		});

		this.setState({

		});
		/*    const xhr = new XMLHttpRequest();
		 let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
		 xhr.open('get', host);
		 xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		 // set the authorization HTTP header
		 xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
		 xhr.responseType = 'json';
		 xhr.addEventListener('load', () => {
		 if (xhr.status === 200) {
		 this.setState({
		 home: xhr.response.list
		 });
		 }
		 });
		 xhr.send(); */
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<BlogAddComponent home={this.state.home} root={this.state.root} />);
	}

}

export default BlogAdd;
