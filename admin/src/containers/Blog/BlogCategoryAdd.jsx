import React from 'react';
import Auth from '../../modules/Auth';
import BlogCategoryAddComponent from '../../components/Blog/BlogCategoryAddComponent.jsx';

class BlogCategoryAdd extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Add Category","/blog/category/index","Blog","/pages/index"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		this.setState({

		});
		/*    const xhr = new XMLHttpRequest();
		 let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
		 xhr.open('get', host);
		 xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		 // set the authorization HTTP header
		 xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
		 xhr.responseType = 'json';
		 xhr.addEventListener('load', () => {
		 if (xhr.status === 200) {
		 this.setState({
		 home: xhr.response.list
		 });
		 }
		 });
		 xhr.send(); */
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<BlogCategoryAddComponent home={this.state.home} root={this.state.root} />);
	}

}

export default BlogCategoryAdd;
