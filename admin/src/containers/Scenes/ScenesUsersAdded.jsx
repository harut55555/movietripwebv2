import React from 'react';
import Auth from '../../modules/Auth';
import ScenesUsersAddedComponent from '../../components/Scenes/ScenesUsersAddedComponent.jsx';

class ScenesUsersAdded extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Users' added","/scenes/users-added","Scenes","/scenes/index"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		this.setState({

		});
		
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<ScenesUsersAddedComponent home={this.state.home} root={this.state.root} />);
	}

}

export default ScenesUsersAdded;
