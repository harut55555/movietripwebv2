import React from 'react';
import Auth from '../../modules/Auth';

import Scene from '../../models/Scene';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import ScenesListComponent from '../../components/Scenes/ScenesListComponent.jsx';

class ScenesList extends React.Component {

	xhr = null;
	static defaultProps = {
		path: Config.getPath().scene
	};

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Scenes","/scenes/index"],
			scenes: []
		};
		this.xhr = new XhrService();
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		console.log(this.props.path);
		// get data
		this.getData({path: this.props.path, auth: false}, function () {

		});

		this.setState({

		});
	}

	getData(props, callback) {


		// call xhr service
		this.xhr.getMtData(props, this, function (data, ref) {

			// job is done
			callback();

			// set state
			if (data.status === 200) {
				let scenes = [];
				for (let i in data.response) {
					let currentScene = new Scene(data.response[i]);
					scenes.push(currentScene);
					console.log(currentScene);

				}

				ref.setState({
					scenes: ref.state.scenes.concat(scenes)
				});
			}
		})
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<ScenesListComponent home={this.state.home} root={this.state.root} scenes={this.state.scenes} />);
	}

}

export default ScenesList;
