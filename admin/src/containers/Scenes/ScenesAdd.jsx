import React from 'react';
import Auth from '../../modules/Auth';
import ScenesAddComponent from '../../components/Scenes/ScenesAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import ImgService from '../../services/ImgService.jsx';
import Config from '../../Config.jsx';
import Scene from '../../models/Scene';
import {browserHistory} from 'react-router';

class ScenesAdd extends React.Component {
    xhr = null;
    img = null;
    static defaultProps = {
        path: Config.getPath().scene,
        pathlocate: Config.getPath().location,
        api: Config.getPath().baseRoute,
        tagPath: Config.getPath().tag
    };

    constructor(props) {
        super(props);
        this.state = {

            bootStrapArray: ["Add Scene", "/scenes/add", "Scenes", "/scenes/index"],
            scene: new Scene(),
            marker: false,
            disabledState: 1,
            googleMap: ""
        };
        this.xhr = new XhrService();
        this.img = new ImgService();
        this.onMapLoad = this.onMapLoad.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.onImgUpload = this.onImgUpload.bind(this);
        this.onImgDelete = this.onImgDelete.bind(this);
        this.onSelect = this.onSelect.bind(this);
        //this.onclickSave =  this.onclickSave.bind(this);
    }

    disabledStateChange() {
        if (this.state.disabledState === 0) {
            $(".selectize")[0].selectize.enable();
            $("#sceneTags")[0].selectize.enable();
        }
        else {
            $(".selectize")[0].selectize.disable();
            $("#sceneTags")[0].selectize.disable();
        }
    }

    droImage(dropzone, self) {
        ImgService.dropImage(dropzone, self);
        console.log(this.state.imgID);
    }

    onImgUpload(imgID) {
        console.log("scene image upload");
        let self = this;
        console.log("scene image upload id = ", self.state.scene.coverImg);
        if (self.state.scene.coverImg === null || self.state.scene.coverImg === undefined) {
            self.state.scene.coverImg = {};

        }
        self.state.scene.coverImg.id = imgID;
        let dataSend;
        dataSend = {
            id: self.state.scene.id,
            coverImg: {
                id: self.state.scene.coverImg.id
            }
        };
        self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
        });
    }

    onImgDelete() {
        console.log("scene image delete");
        let self = this;
        self.state.scene.coverImg.id = 0;
        let dataSend;
        dataSend = {
            id: self.state.scene.id,
            coverImg: {
                id: self.state.scene.coverImg.id
            }
        };
        self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
        });
    }

    handleChange(event) {
    //
    let scene = this.state.scene;
    if (event.target.name === "sceneCopyright") {
        scene.copyright = "";
        scene.copyright = event.target.value;
    }

    else if (event.target.name === "sceneCheckinText") {
        scene.check_in_text = "";
        scene.check_in_text = event.target.value;
    }
    this.setState({scene: scene});
}

    handleBlur(event) {
        let scene = this.state.scene;
        let dataSend;
        if (event.currentTarget.name === "sceneCopyright") {
            scene.copyright = event.currentTarget.value;
            this.setState({scene: scene});
            dataSend = {id: this.state.scene.id, copyright: this.state.scene.copyright};
        }
        else if (event.currentTarget.name === "sceneCheckinText") {
            scene.check_in_text = event.currentTarget.value;
            this.setState({scene: scene});
            dataSend = {id: this.state.scene.id, check_in_text: this.state.scene.check_in_text};
        }
        this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
        })
    }

    onSelect(event) {

        console.log("onSelect = ", event);

    }
    componentWillReceiveProps(nextProps) {
        console.log("componentWillReceiveProps = ", nextProps);
        this.loadPage(nextProps);
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        this.selectizeProperties();
        this.loadPage(this.props);

    }



    loadPage(props) {
        
        this.state.scene.id = (props.params.id === "add") ? null : props.params.id;
        console.log("this.state.scene.id = ",this.state.scene.id  );
        if (this.state.scene.id === null || this.state.scene.id ===  undefined || this.state.scene === null) {
            console.log("if =", this.state.scene.id);
            let state = {
                bootStrapArray: ["Add Scene", "/scene/add", "Scenes", "/scenes/index"],
                scene: new Scene(),
                marker: false,
                disabledState: 1,
                googleMap: ""
            };
            this.state = state;
            this.setState(state);
        }
        else if (this.state.scene.id !== null) {
            console.log("777777777777777777777");
            XhrService.getMtData({
                path: this.props.path + '/' + this.state.scene.id,
                auth: true
            }, this, function (data, ref) {
                if (data.status === 200) {
                    ref.state.scene = data.response;
                    console.log("In ref=", data.response);
                    ref.setState({scene: data.response});
                   console.log("EDIT TAG3432432R5 = ",ref.state.location.tags);
                    if (ref.state.scene.location!==null) {
               
                        // let select1 = $("#locationTags");
                        // let selectize11 = select1[0].selectize;

                        let sceneLocationSelect = $("#sceneLocation");
                        let sceneLocationSelectize = sceneLocationSelect[0].selectize;
                        sceneLocationSelectize.clearOptions();
                        console.log("777777= select edit= ", sceneLocationSelectize.setValue(parseInt(ref.state.scene.location.id)));
                        //  selectize11.setValue(ref.state.location.tags[0].id);
                        sceneLocationSelectize.setValue(parseInt(ref.state.scene.location.id));
                    }
                }
            });

            this.state.disabledState = 0;
            this.disabledStateChange();

        }



    }
   selectizeProperties(){
       let self = this;
       $(".selectize").selectize({
           plugins: ["remove_button", "drag_drop"],
           delimiter: ",",
           persist: false,
           create: function (input) {
               return {
                   value: input,
                   text: input
               }
           }
       });

       $("#sceneTags").selectize({
           plugins: ["remove_button", "drag_drop"],
           delimiter: ",",
           persist: false
       });

//scene  Movie select////////////////////////////////////////////
       let sceneMovieSelect = $("#sceneMovieName");
       let sceneMovieSelectize = sceneMovieSelect[0].selectize;
       this.getData({path: this.props.api + "movie", auth: true}, function (data, ref) {

           // set state
           if (data.status === 200) {
               let currentItem;
               console.log(sceneMovieSelectize);
               sceneMovieSelectize.clearOptions();
               for (let i in data.response.movies) {
                   currentItem = data.response.movies[i];
                   sceneMovieSelectize.addOption({value: currentItem.id, text: currentItem.name});
               }

              ref.setState({});

           }
       });

       sceneMovieSelectize.on('change', function (res) {
           self.state.scene.movie = sceneMovieSelectize.getValue();
           self.creat(self.state.scene.location, self.state.scene.movie);
           //console.log(self.state.movie);
       });
//scene  Movie select end////////////////////////////////////////////

// scene  Location select////////////////////////////////////////////
       let sceneLocationSelect = $("#sceneLocation");
       let sceneLocationSelectize = sceneLocationSelect[0].selectize;
       console.log("lOCATE PATH=",this.props.api);
       this.getData({path: this.props.api + "location", auth: true}, function (data, ref) {

           // set state
           if (data.status === 200) {
               let currentItem;
               console.log("lOCATE RESPON=",data.response);
               console.log(sceneLocationSelectize);
               sceneLocationSelectize.clearOptions();
               for (let i in data.response.locations) {
                   currentItem = data.response.locations[i];
                   sceneLocationSelectize.addOption({value: currentItem.id, text: currentItem.name});
               }
           }
       });
      
       sceneLocationSelectize.on('change', function (res) {
           self.state.scene.location = sceneLocationSelectize.getValue();
           console.log("location id = ", sceneLocationSelectize.getValue());
           console.log("locate=", self.props);
           console.log("lOCATE PATH=",self.props.pathlocate + '/' + self.state.scene.location);
           self.getLocated({path: self.props.pathlocate + '/' + self.state.scene.location, auth: true}, function () {
           });

           //self.creat(self.state.scene.location.name, self.state.scene.movie);
       });
       
   }


    getLocated(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback();
            // set state
            //	console.log("GetDATAAAAAAAAAAAAAAA= ", data.status,"DDDDDD=",ref.state.baseItems.currentlinkWord.substring(0, ref.state.baseItems.currentlinkWord.length));
            if (data.status === 200) {
                console.log("data.response=", data.response);
                ref.state.scene.location = data.response;
                console.log("In ref=", data.response);
                let scene = ref.state.scene;
                ref.setState({scene: scene});
                ref.creat(ref.state.scene.location.id, ref.state.scene.movie);
            }
        })
    }

    creat(location, movie) {
        console.log("locate = ", location);
        if (location !== null) {
            this.mapping();
        }
        if (parseInt(movie) > 0 && parseInt(location) > 0) {
            console.log("two valums movie= ", movie, " location= ", location);
            let a = movie;
            let b = location;
            let dataSend = {movie: {id: a}, location: {id: b}};
            this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
            });
           
        }
    }

    getData(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);

        })
    }

    postData(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            console.log("postdata=", data.response);
            if (data.status === 200) {
                if (data.response.success === true) {
                    ref.state.scene.id = data.response.id;
                    // ref.setState({
                    //     disabledState: 0
                    // });
                    // ref.disabledStateChange();
                    console.log("ref.state.scene.id=", ref.state.scene.id);
                    ref.state.disabledState = 0;
                    ref.disabledStateChange();
                }
            }
            else if (data.status === 400) {
                ref.setState({
                    disabledState: 1
                });
                ref.disabledStateChange();
                console.log("the names are same");
            }
        })
    }

    postDataUpdate(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            console.log("ref state.scene=", ref.state.scene, " data request= ", data.response);
            if (data.response.success === true) {
                console.log("update");
            }
            else {
                console.log("no update");
            }
        })
    }

///////////////////mapping/////////////////////
    onMapLoad(map) {
        this.state.googleMap = map;
        this.state.googleMap.map.setCenter({
            lat: 59.938043, lng: 30.337157
        });
    }


    mapping() {
        console.log("Mapping contry name =", this.state.scene.location);
        let loc;
        let marker = this.state.marker;
        marker = new this.state.googleMap.maps.Marker({
            position: {
                lat: this.state.scene.location.address.latitude,
                lng: this.state.scene.location.address.longitude
            }
        });
        this.state.googleMap.map.setCenter({
            lat: this.state.scene.location.address.latitude,
            lng: this.state.scene.location.address.longitude
        });
        // ref.state.googleMap.maps.MaxZoomService();
        this.state.googleMap.map.setZoom(16);
        marker.setMap(this.state.googleMap.map);
        this.setState({marker: marker});


    }

    /**
     * Render the component.
     */
    render() {
        console.log(this.state.scene);
        return (<ScenesAddComponent bootStrapArray={this.state.bootStrapArray}
                                    disabledState={this.state.disabledState}
                                    scene={this.state.scene}
                                    handleChange={this.handleChange}
                                    handleBlur={this.handleBlur}
                                    onMapLoad={this.onMapLoad}
                                    onSelect={this.onSelect}/>);
    }

}

export default ScenesAdd;
