import React from 'react';
import Auth from '../../modules/Auth';
import MoviesUsersAddedComponent from '../../components/Movies/MoviesUsersAddedComponent.jsx';

class MoviesUsersAdded extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Users' added","/movies/users-added","Movies","/movies/index"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		this.setState({

		});
		
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<MoviesUsersAddedComponent home={this.state.home} root={this.state.root} />);
	}

}

export default MoviesUsersAdded;
