import React from 'react';
import Auth from '../../modules/Auth';
import MoviesAddComponent from '../../components/Movies/MoviesAddComponent.jsx';
import ImgService from '../../services/ImgService.jsx';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import Movie from '../../models/Movie';
import {browserHistory} from 'react-router';

class MoviesAdd extends React.Component {
	xhr = null;
	img = null;
	static defaultProps = {
		path: Config.getPath().movie,
		api: Config.getPath().baseroute,
		tagPath: Config.getPath().tag
	};
	
	constructor(props) {
		super(props);
		this.state = {
			bootStrapArray: ["Add Movie", "/movies/add", "Movies", "/movies/index"],
			movie: new Movie(),
			marker: false,
			disabledState: 1,
			googleMap: ""
		};
		this.xhr = new XhrService();
		this.img = new ImgService();
		//this.onMapClick = this.onMapClick.bind(this);
		//this.onMapLoad = this.onMapLoad.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleBlur = this.handleBlur.bind(this);
		this.onImgUpload = this.onImgUpload.bind(this);
		this.onImgDelete = this.onImgDelete.bind(this);
	}
	
	disabledStateChange() {
		if (this.state.disabledState === 0) {
			$('#movieTags')[0].selectize.enable();
			$("#movieCountry")[0].selectize.enable();
			$("#movieGenre")[0].selectize.enable();
			$("#movieDirector")[0].selectize.enable();
			$("#movieActor")[0].selectize.enable();
		}
		else {
			$('#movieTags')[0].selectize.disable();
			$("#movieCountry")[0].selectize.disable();
			$("#movieGenre")[0].selectize.disable();
			$("#movieDirector")[0].selectize.disable();
			$("#movieActor")[0].selectize.disable();
		}
	}
	
	onImgUpload(imgID) {
		console.log("movie image upload");
		let self = this;
		console.log("movie image upload id = ", self.state.movie.coverImg);
		if (self.state.movie.coverImg === null || self.state.movie.coverImg === undefined) {
			self.state.movie.coverImg = {};
			
		}
		self.state.movie.coverImg.id = imgID;
		let dataSend;
		dataSend = {
			id: self.state.movie.id,
			coverImg: {
				id: self.state.movie.coverImg.id
			}
		};
		self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
		});
		
	}
	
	onImgDelete() {
		console.log("movie image delete");
		let self = this;
		self.state.movie.coverImg.id = 0;
		let dataSend;
		dataSend = {
			id: self.state.movie.id,
			coverImg: {
				id: self.state.movie.coverImg.id
			}
		};
		self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
		});
		
	}
	
	handleClick(name, event) {
		console.log("Click button = ", name);
		console.log("event = ", event.type);
	}
	
	handleChange(event) {
		console.log("dd=", event.target.name);
		let movie = this.state.movie;
		if (event.target.name === "movieName") {
			movie.name = "";
			movie.name = event.target.value;
			console.log("movie.name =", movie.name);
			this.setState({movie: movie});
		}
		else if (event.target.name === "movieYear") {
			console.log(" handlechange year event=", event.target.value);
			movie.year = 0;
			movie.year = event.target.value;
			this.setState({movie: movie});
		}
		else if (event.target.name === "#movieDescription") {
			movie.content = "";
			movie.content = tinyMCE.get("movieDescription").getContent();
		}
	}
	
	handleBlur(event) {
		let movie = this.state.movie;
		let dataSend;
		if (event.target.name === "movieName") {
			movie.name = event.target.value;
			this.setState({movie: movie});
			
			if (this.state.movie.id === null) {
				dataSend = {name: this.state.movie.name};
				this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
				});
			}
			else {
				dataSend = {id: this.state.movie.id, name: this.state.movie.name};
				this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
				});
			}
		}
		else if (event.target.name === "movieYear") {
			movie.year = 0;
			movie.year = event.target.value;
			this.setState({movie: movie});
			console.log("handleblur year=", this.state.movie.year);
			
			dataSend = {id: this.state.movie.id, year: this.state.movie.year};
			this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
			});
			
		}
		else if (event.target.name === "#movieDescription") {
			movie.description = "";
			movie.description = tinyMCE.get("movieDescription").getContent();
			this.setState({movie: movie});
			// update existing movie content
			console.log("movie.description=",this.state.movie.description);
			dataSend = {id: this.state.movie.id, description: this.state.movie.description};
			console.log("dataSend=",dataSend);
			this.postDataUpdate({path: this.props.path + "/update/", auth: true}, dataSend, function (data, ref) {});
		}
	}
	
	
	componentWillReceiveProps(nextProps) {
		console.log("componentWillReceiveProps = ", nextProps);
		this.loadPage(nextProps);
	}
	
	componentDidMount() {
		window.scrollTo(0, 0);
		console.log("componenetDidMount = ", this.state.movie);
		this.state.movie.id = (this.props.params.id === "add") ? null : this.props.params.id;
		console.log("componentDidMount=", this.props);
		this.selectizeProperties();
		this.loadPage(this.props);
	}
	
	
	selectizeProperties() {
		$(".selectize").selectize({
			plugins: ["remove_button", "drag_drop"],
			delimiter: ",",
			persist: false,
			sortField: 'text',
			create: function (input) {
				return {
					value: input,
					text: input
				}
			}
		});
		$('#movieTags').selectize({
			persist: false,
			createOnBlur: true,
			create: function (input) {
				return {
					value: null,
					text: input
				}
			},
			load: function (query, callback) {
				
				if (query.length === 0) {
					callback();
					return;
				}
				
				XhrService.getMtData({auth: true, path: self.props.tagPath + '/' + query}, {}, function (data, ref) {
					let resData = data.response;
					let options = [];
					if (typeof resData !== undefined && resData !== null) {
						for (let i in resData) {
							let temp = {
								value: resData[i].id,
								text: resData[i].name
							};
							options.push(temp);
						}
					}
					callback(options);
				});
			}
		});
		//  tags:[{id:1930},{id:786}]
		let select1 = $("#movieTags");
		let selectize11 = select1[0].selectize;
		selectize11.on('change', function (res) {
			console.log("select = ", selectize11.getValue())
		});
		let tagSettings = {
			plugins: ["remove_button", "drag_drop"],
			delimiter: ",",
			persist: false,
			create: function (input) {
				return {
					value: input,
					text: input
				}
			}
		};
		let movieCountrySelect = $("#movieCountry");
		let movieCountrySelectize = movieCountrySelect[0].selectize;
		
		this.getData({path: this.props.api + "country/getcountry", auth: true}, function (data, ref) {
			
			// set state
			if (data.status === 200) {
				let currentItem;
				movieCountrySelectize.clearOptions();
				for (let i in data.response.countries) {
					currentItem = data.response.countries[i];
				//	console.log("currentItem=", currentItem);
					let v, t;
					(currentItem === null || currentItem.id === null) ? (v = "" , t = "") : (v = currentItem.id, t = currentItem.name);
					movieCountrySelectize.addOption({value: v, text: t});
				}
				ref.setState({});
				
			}
		});
		movieCountrySelectize.on('change', function (res) {
			self.state.countryID = movieCountrySelectize.getValue();
			console.log("countries:");
			console.log(self.state.countryID);
		});
		
		////////////// Genre ////////////////////////////////////
		let movieGenreSelect = $("#movieGenre");
		let movieGenreSelectize = movieGenreSelect[0].selectize;
		this.getData({path: this.props.api + "movie/genres", auth: true}, function (data, ref) {
			if (data.status === 200) {
				let currentItem;
				movieGenreSelectize.clearOptions();
				for (let i in data.response) {
					currentItem = data.response[i];
					let v;
					currentItem.id === null ? v = "" : v = currentItem.id;
					movieGenreSelectize.addOption({value: v, text: currentItem.name});
				}
			}
		});
		movieGenreSelectize.on('change', function (res) {
			self.state.filmpersons.genres = movieGenreSelectize.getValue();
			console.log("genres:");
			console.log(self.state.filmpersons.genres);
		});
		let movieDirectors = $("#movieDirector");
		let movieActors = $("#movieActor");
		let movieTags = $("#movieTags");
		
		movieDirectors.selectize(tagSettings);
		movieActors.selectize(tagSettings);
		movieTags.selectize(tagSettings);
		
		let movieDirectorsSelectize = movieDirectors[0].selectize;
		let movieActorsSelectize = movieActors[0].selectize;
		let movieTagsSelectize = movieTags[0].selectize;
		
		movieDirectorsSelectize.on('change', function (res) {
			let movieDirectorsList = movieDirectorsSelectize.getValue().split(",");
			self.state.filmpersons.directors = movieDirectorsList;
			console.log("directors:");
			console.log(self.state.filmpersons.directors);
		});
		
		movieActorsSelectize.on('change', function (res) {
			let movieActorsList = movieActorsSelectize.getValue().split(",");
			self.state.filmpersons.actors = movieActorsList;
			console.log("actors:");
			console.log(self.state.filmpersons.actors);
		});
		
		movieTagsSelectize.on('change', function (res) {
			let movieTagsList = movieTagsSelectize.getValue().split(",");
			self.state.filmpersons.tags = movieTagsList;
			console.log("tags:");
			console.log(self.state.filmpersons.tags);
		});


	}
	
	
	loadPage(props) {
		let self = this;
		tinymce.init({
			selector: "#movieDescription",
			theme: "modern",
			setup: function (editor) {
				editor.on("change", function (e) {
					self.handleChange({target: {name: "#movieDescription"}});
				});
				editor.on("blur", function (e) {
					self.handleBlur({target: {name: "#movieDescription"}});
				});
				editor.on("init", function (e) {
					if (self.state.movie!== undefined && self.state.movie.description !== null && self.state.movie.description !== "") {
						tinyMCE.get("movieDescription").setContent(self.state.movie.description);
					}
					else {
						tinyMCE.get("movieDescription").setContent("");
					}
				});
			},
			height: 250,
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons"
		});
		this.state.movie.id = (props.params.id === "add") ? null : props.params.id;
		if (this.state.movie.id === null || this.state.movie === null) {
			console.log("if =", this.state.movie.id);
			let state = {
				bootStrapArray: ["Add Movie", "/movies/add", "Movies", "/movies/index"],
				movie: new Movie(),
				marker: false,
				disabledState: 1,
				googleMap: ""
			};
			this.state = state;
			this.setState(state);
		}
		else if (this.state.movie.id !== null) {
			console.log("edit");
			XhrService.getMtData({
				path: this.props.path + '/' + this.state.movie.id,
				auth: false
			}, this, function (data, ref) {
				if (data.status === 200) {
					ref.state.movie = data.response;
					console.log("In ref=", data.response);
					ref.setState({movie: data.response});
					console.log("EDIT TAG3432432R5 = ", ref.state.movie.tags);
					if (ref.state.movie.tags !== null) {
						
						let select1 = $("#movieTags");
						let selectize11 = select1[0].selectize;
						selectize11.clearOptions();
						selectize11.setValue(parseInt(ref.state.movie.tags[0].id));
					}
				}
			});
			
			this.state.disabledState = 0;
			this.disabledStateChange();
			
		}
		/////////////call selectize function//////////
		this.droImage("MTDropBox", this);
		// this.getData({path: this.props.api + "country/getcountry", auth: false}, function (data, ref) {
		// 	if (data.status === 200) {
		// 		let currentItem;
		// 		let select = $("#movieCountry");
		// 		let selectize = select[0].selectize;
		// 		selectize.clearOptions();
		// 		for (let i in data.response) {
		// 			currentItem = data.response[i];
		// 			selectize.addOption({value: currentItem.id, text: currentItem.name});
		// 		}
		// 		if (ref.props.params.id !== undefined && ref.state.movie.country !== null && ref.state.movie.country.id !== undefined) {
		// 			console.log("LOAD SELECT COUNTRY");
		// 			selectize.setValue(parseInt(ref.state.movie.country.id));
		//
		// 		}
		// 	}
		// });
		// this.getDatatype({path: this.props.path + "/types", auth: false}, function (data, ref) {
		// 	if (data.status === 200) {
		// 		let currentItem;
		// 		let select = $("#movieType");
		// 		let selectize = select[0].selectize;
		// 		selectize.clearOptions();
		// 		for (let i in data.response) {
		// 			currentItem = data.response[i];
		// 			selectize.addOption({value: currentItem.id, text: currentItem.name});
		// 		}
		// 		if (ref.props.params.id !== undefined && ref.state.movie.type !== null && ref.state.movie.type.id !== undefined) {
		// 			selectize.setValue(parseInt(ref.state.movie.type.id));
		// 		}
		//
		// 	}
		// });
	}
	
	
	getData(props, callback) {
		// call xhr service
		XhrService.getMtData(props, this, function (data, ref) {
			// job is done
			callback(data, ref);
			
		})
	}
	
	postDataUpdate(props, dataSend, callback) {
		XhrService.postMtData(props, dataSend, this, function (data, ref) {
			callback(data, ref);
			
			if (data.status === 200) {
				// if (data.response.success === false) {
				//console.log("update", data.response);
				//console.log("update name", data.response.name);
				//console.log("update year", data.response.year);
				console.log("tvec error");
				console.log(data.response);
				//}
				//else console.log("Save request -= ", data.response);
				//
			}
		})
	}
	
	postData(props, dataSend, callback) {
		console.log(props);
		XhrService.postMtData(props, dataSend, this, function (data, ref) {
			callback(data, ref);
			
			if (data.status === 200) {
				console.log(data.response);
				ref.state.movie.id = data.response.id;
				console.log("PostData = ", dataSend, "  PostData dataresponse===", data.response, "  Props=", props);
				ref.setState({disabledState: 0});
				ref.disabledStateChange();
				
			}
		})
	}
	
	
	droImage(dropzone, self) {
		ImgService.dropImage(dropzone, self);
	}
	
	/**
	 * Render the component.
	 */
	render() {
		return (
		 <MoviesAddComponent bootStrapArray={this.state.bootStrapArray}
		                     disabledState={this.state.disabledState}
		                     movie={this.state.movie}
		                     handleChange={this.handleChange}
		                     handleBlur={this.handleBlur}/>);
	}
	
}

export default MoviesAdd;
