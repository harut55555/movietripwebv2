import React from 'react';
import Auth from '../../modules/Auth';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';

import Movie from '../../models/Movie';

import MoviesListComponent from '../../components/Movies/MoviesListComponent.jsx';
import { browserHistory } from 'react-router';

class MoviesList extends React.Component {

    xhr = null;
    static defaultProps = {
        path: Config.getPath().movie
    };

    /**
     * Class constructor.
     */
    
    constructor(props) {
        super(props);
        console.log(props.route.path);
   
        let w = props.route.path.substring(1,props.route.path.length).substring(0,props.route.path.substring(1,props.route.path.length).indexOf('/'));
        let a = w.charAt(0).toUpperCase() + w.slice(1).toLowerCase();
        let b = "/" + w + "/index";
        console.log(a);
        console.log(b);
        this.state = {
            "home": {},
            bootStrapArray: [a, b],
            movies: [],
            pagescroll: [],
            value: 10,
            arr: 0,
            countitem: 0,
            max:0,
            page: 1
        };
        this.xhr = new XhrService();
        this.handleChange = this.handleChange.bind(this);

        
    }

    componentWillReceiveProps(nextProps) {
        console.log("will");
        console.log(nextProps);
        if (nextProps.params.page === undefined) {
            nextProps.params.page= 1;
        }
        this.loadPage(nextProps);
    }
    componentDidMount() {
        if (this.props.params.page === undefined) {
            this.props.params.page= 1;
        }
        console.log("did");
        this.loadPage(this.props);
    }
    loadPage(props) {
        this.state.page = props.params.page;
        this.setState({
            page: props.params.page
        });
        this.props.params.page = props.params.page;
        window.scrollTo(0, 0);
        console.log("page");
        console.log(props.params.page);
        console.log(this.props.path);
        // get data
        // let mystring = this.state.root[1];
        // this.state.arr=mystring.split("/locations/index/").pop();
        // console.log(this.state.arr);
        console.log("tttt");

        if((parseInt(props.params.page))%3===0) {

            this.state.pagescroll[0] = parseInt(this.state.page)-2;
            this.state.pagescroll[1] = parseInt(this.state.page)-1;
            this.state.pagescroll[2] = parseInt(this.state.page);
            console.log("1 depq");

        }
        else if((parseInt(props.params.page))%3===1 || (parseInt(props.params.page))===1)
        {

            this.state.pagescroll[0] = parseInt(this.state.page);
            this.state.pagescroll[1] = parseInt(this.state.page)+1;
            this.state.pagescroll[2] = parseInt(this.state.page)+2;
            console.log("2 depq");

        }
        else if((parseInt(props.params.page))%3===2) {
            this.state.pagescroll[0] = parseInt(this.state.page) - 1;
            this.state.pagescroll[1] = parseInt(this.state.page);
            this.state.pagescroll[2] = parseInt(this.state.page) + 1;
            console.log("3 depq");
        }

        // this.setState({
        // 	arr:  (parseInt(props.params.page)+1).toString()
        // });

        console.log("load");
        console.log(this.state.page);

        this.getDatacountitem({path: this.props.path + "/count", auth: false}, function () {

        });
        this.getData({path: this.props.path + '/' + this.state.page + '/' + this.state.value, auth: false}, function () {

        });
        this.state.max = (parseInt(this.state.countitem/this.state.value));
        if((this.state.countitem % this.state.value) >  0){this.state.max=this.state.max+1;}

    }

    getData(props, callback) {
        // call xhr service
        this.xhr.getMtData(props, this, function (data, ref) {

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                let  movies = [];
                for (let i in data.response) {
                    let currentMovie = new Movie(data.response[i]);
                    movies.push(currentMovie);
                }
                ref.setState({
                    movies:  movies
                });
            }
        })
    }

    getDatacountitem(props, callback) {
        // call xhr service
        this.xhr.getMtData(props, this, function (data, ref) {

            // job is done
            callback();

            // set state
            if (data.status === 200) {
                let countitem =0;
                let currentLocation = data.response;
                countitem =currentLocation;

                ref.setState({
                    countitem: countitem
                });
            }
        })
    }

    handleChange(event) {
        // console.log("handleChange");
        // console.log(event.target.value);
        this.state.value = event.target.value;
        // this.props.params.page = 1;

        this.props.params.page = 1;
        //	console.log(this.props.params.page);
        this.loadPage(this.props);
        browserHistory.push('/movies/index');
    }
    handleClick(event) {
        console.log("handleClick");
        console.log(event);
        // console.log(event.target.value);

    }
    /**
     * Render the component.
     */
    render() {
        {console.log("ddd")}
        {console.log(this.state.max)}
        return (<MoviesListComponent home={this.state.home} bootStrapArray={this.state.bootStrapArray} movies={this.state.movies} handleChange={this.handleChange}
                                        value={this.state.value} pagescroll={this.state.pagescroll} page={this.state.page} max={this.state.max} />);
             }

    
}

export default MoviesList;
