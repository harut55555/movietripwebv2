/**
 * Created by Arsen on 13.03.2017.
 */
import React from 'react';
import Auth from '../modules/Auth';
import Base from '../components/Base.jsx';
import User from '../models/User';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx';
import UserProfile from '../models/UserProfile';
import {browserHistory} from 'react-router';

class BasePage extends React.Component {

    static defaultProps = {
        userProfile: Config.getPath().userProfile
    };

    constructor(props, context) {
        super(props, context);
        const storedMessage = localStorage.getItem('successMessage');
        let successMessage = '';
        if (storedMessage) {
            successMessage = storedMessage;
            localStorage.removeItem('successMessage');
        }

        // set the initial component state
        this.state = {
            user: null,
            statistics: {
                location: 2886,
                movies: 444,
                scenes: 123
            }
        };

        this.xhr = new XhrService();
        this.changeUser = this.changeUser.bind(this);
        this.handleSignOut = this.handleSignOut.bind(this);
    }

    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);

        let newUser = new User();
        newUser.init({id: 0, name: "Harut"});

        this.setState({
            user: newUser
        });

        // bind Ui elements
        this.bindUiElements();

        this.getUserInfo();
    }

    bindUiElements(){

        let sideMenuToggle = $(".sideMenuToggle");

        sideMenuToggle.click(function (e) {
            e.preventDefault();
            let body = $("body");
            if (body.hasClass("mtSidebarOpen")) {
                let sidebarCollapsedLink = $(".mtSidebar a[data-toggle='collapse']:not(.collapsed)");
                sidebarCollapsedLink.addClass("collapsed");
                let sidebarCollapsedPanel = $(".mtSidebar .panel-collapse.in");
                sidebarCollapsedPanel.removeClass("in");
            }
            body.toggleClass("mtSidebarOpen");
        });

        let sidebarHeadingCollapseLink = $(".mtSidebar .panel-heading a[data-toggle='collapse']");

        sidebarHeadingCollapseLink.click(function () {
            if (!$("body").hasClass("mtSidebarOpen")) {
                return false;
            }
        });

        let contentWrapper = ".contentWrapper";
        let headerHeight = $("header").height();
        let footerHeight = $("footer").height();

        function contentWrapperHeight() {
            let calcHeight = $(window).height() - headerHeight - footerHeight;
            let sidebarHeight = $(".mtSidebar > .sidebar-menu").height() - footerHeight;
            if (calcHeight <= sidebarHeight) {
                $(contentWrapper).css("min-height", sidebarHeight);
            }
            else {
                $(contentWrapper).css("min-height", calcHeight);
            }
        }

        contentWrapperHeight();
        $("footer").removeClass("invisibile");

        $(window).resize(function() {
            contentWrapperHeight();
        });
    }

    handleSignOut() {
        Auth.deauthenticateUser();

        browserHistory.push('/login');
        this.state.isAuthenticated=false;
    }

    getUserInfo() {

        if (this.state.user === null) {

            // call xhr service
            XhrService.getMtData({path: this.props.userProfile, auth: true}, this, function (data, ref) {
                // set state
                if (data.status === 200) {
                    let userData = data.response;
                    let state = ref.state;
                    state.user = new UserProfile(userData);
                    ref.state = state;
                    ref.setState(state);
                }
            })
        }
    }

    /**
     * Change the user object.
     *
     * @param {object} event - the JavaScript event object
     */
    changeUser(event) {
        event.preventDefault();
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;

        this.setState({
            user
        });
    }

    /**
     * Render the component.
     */
    render() {
        return (<Base isAuthenticated={this.state.isAuthenticated}
                      children={this.props.children}
                      handleSignOut={this.handleSignOut}
                      statistics={this.state.statistics}
                      user={this.state.user}/>);
    }

}

export default BasePage;