import React from 'react';
import Auth from '../../modules/Auth';
import GeoAddComponent from '../../components/Geo/GeoAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import ImgService from '../../services/ImgService.jsx';
import Config from '../../Config.jsx';
import {browserHistory} from 'react-router';

class GeoAdd extends React.Component {

	/**
	 * Class constructor.
	 */
	static defaultProps = {
		path: Config.getPath().baseRoute,
		pathlocate: Config.getPath().location,
		api: Config.getPath().baseRoute,
		tagPath: Config.getPath().tag
	};
	constructor(props) {
		super(props);

		this.state = {
			bootStrapArray: ["Add Geo", "/geo/add", "Geo", "/geo/index"],
			geo: {
				id: null,
				name: null,
				country: null,
				postCode: null
			},
			disabledState: 1
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleBlur = this.handleBlur.bind(this);
		this.onSelect = this.onSelect.bind(this);
	}
	
	disabledStateChange() {
		if (this.state.disabledState === 0) {
			$(".selectize")[0].selectize.enable();
			$("#cityCountry")[0].selectize.enable();
		}
		else {
			$(".selectize")[0].selectize.disable();
			$("#cityCountry")[0].selectize.disable();
		}
	}
	
	onSelect(event) {

		console.log("onSelect = ", event);

	}
	handleChange(event) {
		let geo = this.state.geo;
		if (event.target.name === "cityName") {
			geo.name = "";
			geo.name = event.target.value;
		}
		else if (event.target.name === "cityPostcode") {
			geo.postCode = "";
			geo.postCode = event.target.value;
		}
		console.log("handlechange=", event.target.value);
		
		
		this.setState({geo: geo});
	}

	handleBlur(event) {
		let geo = this.state.geo;
		let dataSend;
		console.log("OBJ= ",this.state.geo);
		if (event.currentTarget.name === "cityName" && (this.state.geo.id === null || this.state.geo.id ===undefined)) 
		{
			geo.name = "";
			geo.name = event.currentTarget.value;
			this.setState({geo: geo});
			console.log("name====",this.state.geo);
			dataSend = {parent_id: this.state.geo.country.id, name: this.state.geo.name};
			this.postData({path: this.props.path +"country", auth: true}, dataSend, function (data, ref) {
			});
		}
			else {
			
			let obj;
			console.log("event.currentTarget.name.substring(4).toLowerCase()).substring(6)==",event.currentTarget.name.substring(4).toLowerCase().substring(6));
			if(event.currentTarget.name.substring(4).toLowerCase().substring(6)) {obj=event.currentTarget.name.substring(4) }
			else {
				obj = (event.currentTarget.name.substring(4)).toLowerCase();
			}
			console.log("OBJ= ",obj);
			geo[obj]="";
			geo[obj]= event.currentTarget.value;
			this.setState({geo: geo});
				dataSend = {id: this.state.geo.id, [obj]: this.state.geo[obj]};
				this.postDataUpdate({path: this.props.path +"country/update", auth: true}, dataSend, function (data, ref) {
				});
			}
		}


	


	componentWillReceiveProps(nextProps) {
		console.log("componentWillReceiveProps = ", nextProps);
		this.loadPage(nextProps);
	}
	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);
		this.selectizeProperties();
		this.loadPage(this.props);
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	loadPage(props) {

		this.state.geo.id = (props.params.id === "add") ? null : props.params.id;
		console.log("this.state.geo.id = ",this.state.geo.id  );
		if (this.state.geo.id === null || this.state.geo.id ===  undefined || this.state.scene === null) {
			let state = {
				bootStrapArray: ["Add Geo", "/geo/add", "Geo", "/geo/index"],
				geo: {
					id: null,
					name: null,
					country: null,
					postCode: null
				},
				disabledState: 1
			};
			this.state = state;
			this.setState(state);
		}
		else if (this.state.geo.id !== null) {
			XhrService.getMtData({
				path: this.props.path  +"country/"+ this.state.geo.id,
				auth: true
			}, this, function (data, ref) {
				if (data.status === 200) {
					ref.state.scene = data.response;
					console.log("In ref=", data.response);
					ref.setState({geo: data.response});
				}
			});

			this.state.disabledState = 0;
			this.disabledStateChange();

		}
		this.getData({path: this.props.api + "country/getcountry", auth: true}, function (data, ref) {
			if (data.status === 200) {
				let currentItem;
				let select = $("#cityCountry");
				let selectize = select[0].selectize;
				console.log("select=",select);
				console.log("selectize=",selectize);
				selectize.clearOptions();
				for (let i in data.response.countries) {
					currentItem = data.response.countries[i];
					let v,t;
					(currentItem===null ||  currentItem.id===null) ? (v="" , t=""): (v=currentItem.id, t=currentItem.name);
					selectize.addOption({value: v, text: t});
				}
			//	console.log("ref.props.params.id=",ref.props.params.id," ref.state.location.country =",ref.state.geo.country," ref.state.geo.country.id = " );
				if (ref.props.params.id !== undefined && ref.state.geo.country !== null) {
					console.log("LOAD SELECT COUNTRY=",ref.state.geo.id);
					//ref.state.geo.country={};
					selectize.setValue(parseInt(ref.state.geo.id));
					console.log("Try SELECT COUNTRY=",selectize.setValue(parseInt(ref.state.geo.id)));
				}
			}
		});
	}


	selectizeProperties()
	{
		// let self = this;
		// $(".selectize").selectize({
		// 	plugins: ["remove_button", "drag_drop"],
		// 	delimiter: ",",
		// 	persist: false,
		// 	create: function (input) {
		// 		return {
		// 			value: input,
		// 			text: input
		// 		}
		// 	}
		// });
        //
		// let select = $("#cityCountry");
		// let selectize = select[0].selectize;
		// selectize.on('change', function (res) {
		// 	let call;
		// 	console.log("33333locationCountry=", selectize.getItem(selectize.getValue())[0]);
		// 	(self.state.geo.country !== null && self.state.geo.country.name === selectize.getItem(selectize.getValue())[0].innerText) ?
		// 		(call = false, console.log("HAVASAR EN", self.state.geo.country.name, "===", selectize.getItem(selectize.getValue())[0].innerText)) :
		// 		(console.log("HAVASAR CHEEN"),
		// 			call = true);
		// 	let geo = self.state.geo;
		// 	geo.country = {};
		// 	geo.country.id = selectize.getValue();
		// 	geo.country.name = selectize.getItem(selectize.getValue())[0].innerText;
		// 	// self.state.disabledState = 0;
		// 	//self.state.geo.id=geo;
		// 	self.setState({geo: geo,
		// 		disabledState: 0
		// 	});
		// 	self.disabledStateChange();
		// });
        //


		

		let self = this;
		console.log("********** selectizeProperties");
		/////////////selectize ////////////////
		$(".selectize").selectize({
			plugins: ["remove_button", "drag_drop"],
			delimiter: ",",
			persist: false,
			create: function (input) {
				return {
					value: input,
					text: input
				}
			}
		});
		

		/////////// select country//////////////////
		let select = $("#cityCountry");
		let selectize = select[0].selectize;
		selectize.on('change', function (res) {
			console.log("onchange selectise");
			let geo= self.state.geo;
			geo.country = {};
			geo.country.id = selectize.getValue();
			geo.country.name = selectize.getItem(selectize.getValue())[0].innerText;
			self.setState({geo: geo});
			//console.log("On country id= ", self.state.location.country.id, "  On country name= ", self.state.location.country.name);
		});
	}
	getData(props, callback) {
		XhrService.getMtData(props, this, function (data, ref) {
			// job is done
			callback(data, ref);
		})
	}

	postData(props, dataSend, callback) {
		XhrService.postMtData(props, dataSend, this, function (data, ref) {
			callback(data, ref);
			console.log("postdata=", data.response);
			if (data.status === 200) {
				if (data.response.success === true) {
					ref.state.geo.id = data.response.id;
					// ref.setState({
					//     disabledState: 0
					// });
					// ref.disabledStateChange();
					console.log("ref.state.geo.id=", ref.state.geo.id);
					ref.state.disabledState = 0;
					ref.disabledStateChange();
				}
			}
			else if (data.status === 400) {
				ref.setState({
					disabledState: 1
				});
				ref.disabledStateChange();
				console.log("the names are same");
			}
		})
	}

	postDataUpdate(props, dataSend, callback) {
		XhrService.postMtData(props, dataSend, this, function (data, ref) {
			callback(data, ref);
			if (data.response.success === true) {
				console.log(" postDataUpdate update dataSend=", dataSend, " Krevor=", dataSend.country);
				if (dataSend.country !== undefined && dataSend.city !== undefined) {
					console.log(" IFFFFF postDataUpdate update dataSend=", dataSend.country);
					ref.mapping();

				}
			}
			else {
				console.log("no noupdate = ", data.response);
			}
		})
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<GeoAddComponent bootStrapArray={this.state.bootStrapArray}
								 disabledState={this.state.disabledState}
								 geo={this.state.geo}
								 handleChange={this.handleChange}
								 handleBlur={this.handleBlur}
								 onSelect={this.onSelect}/>);
	}

}

export default GeoAdd;
