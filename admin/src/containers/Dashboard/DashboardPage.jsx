import React from 'react';
import Auth from '../../modules/Auth';
import Dashboard  from '../../components/Dashboard.jsx';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';

class DashboardPage extends React.Component {


    static defaultProps = {
        countPath: Config.getPath().dashboardCount // same as /api/
    };

    /**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			locationCount: 0,
			movieCount: 0,
			scenaCount: 0,
			bootStrapArray:["Dashboard","/"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

        // get data of list;
        this.getData({
            path: this.props.countPath,
            auth: true
        }, function (data, ref) {
        	if (typeof data !== 'undefined' && data !== null && data.length === 3) {
				let state = ref.state;
				state.locationCount = data[0];
				state.movieCount = data[1];
				state.scenaCount = data[2];
				ref.setState(state);
			}
        });

	}


    // get data function
    getData(props, callback) {
        let self = this;

        // add loader
        //self.setLoaderStatus(true);

        // call xhr service
        XhrService.getMtData(props, self, function (data, ref) {

            // remove loader
            //self.setLoaderStatus(false);

            // job is done
            callback(data.response, ref);
        })
    }

	/**
	 * Render the component.
	 */
	render() {
		return (<Dashboard locationCount={this.state.locationCount}
						   movieCount={this.state.movieCount}
						   scenaCount={this.state.scenaCount}
						   bootStrapArray={this.state.bootStrapArray} />);
	}

}

export default DashboardPage;
