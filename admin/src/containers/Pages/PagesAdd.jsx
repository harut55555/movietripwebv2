import React from 'react';
import Auth from '../../modules/Auth';
import PagesAddComponent from '../../components/Pages/PagesAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import Page from '../../models/Page';
import BootStrap from '../../services/BootStrap';
import {browserHistory} from 'react-router';

class PagesAdd extends React.Component {

    static defaultProps = {
        path: Config.getPath().page
    };

    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);

        this.state = {
            bootStrap: {
                controller: '',
                apiPath: '',
                params: {},
                path: this.props.route.path,
                breadCrumbs: []
            },
            data: new Page(),
            disabled: true,
            publishDisabled: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.onSaveAndClose = this.onSaveAndClose.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onSaveDraft = this.onSaveDraft.bind(this);
    }

    resetState(){

    }

    // called when component mounted
    bootStrap(props) {
        let state = this.state;
        state.bootStrap = {
            controller: BootStrap.getControllerName(props),
            params: props.params,
            path: props.route.path,
            breadCrumbs: BootStrap.getBreadCrubs(props)
        };

        state.bootStrap.apiPath = state.bootStrap.breadCrumbs[0].apiPath;

        // update state
        this.state = state;
        this.setState(state);
    }


    handleChange(event) {
        let state = this.state;
        let data = state.data;
        if (event.target.name === "title") {
            data.title = event.target.value;
        }
        else if (event.target.name === "alias") {
            data.alias = event.target.value;
        }
        else if (event.target.name === "content") {
            data.content = tinyMCE.get("pageEditor").getContent();
        }
        this.state = state;
        this.setState(state);

        // check filters
        this.checkFilters();
    }

    handleBlur(event) {
        let dataSend;
        let state = this.state;
        let data = state.data;
        if (event.target.name === "title") {
            data.title = event.target.value;
            state.data = data;
            this.setState(state);
            if (state.data.id === null) {
                // create new page, set title and get id
                dataSend = {title: state.data.title};
                this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
                    if (data.status === 200) {
                        ref.state.data.id = data.response.id;
                        ref.state.disabled = false;
                        ref.setState(ref.state);
                    }
                });
            }
            else {
                this.postDataUpdate({path: this.props.path + "/update/", auth: true}, data, function (data, ref) {

                });
            }
        }
        else if (event.target.name === "alias") {
            data.alias = event.target.value;
            state.data = data;
            this.setState(state);
            // update existing page alias
            this.postDataUpdate({path: this.props.path + "/update/", auth: true}, data, function (data, ref) {

            });
        }
        else if (event.target.name === "content") {
            data.content = tinyMCE.get("pageEditor").getContent();
            state.data = data;
            this.setState(state);
            // update existing page content
            this.postDataUpdate({path: this.props.path + "/update/", auth: true}, data, function (data, ref) {

            });
        }
    }

    componentWillReceiveProps(nextProps) {
        // bind events
        this.bindEvents();

        // reset the state
        if (nextProps.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(nextProps);

        // load page
        this.loadPage(nextProps);
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        // update id
        let state = this.state;
        state.data.id = (this.props.params.id === "add") ? null : this.props.params.id;
        this.state = state;
        this.setState(state);

        // reset the state
        if (this.props.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(this.props);

        // load page
        this.loadPage(this.props);

        // bind events
        this.bindEvents();
    }

    bindEvents() {

        let self = this;
        if (tinymce.get('pageEditor') === null) {

            // tiny mce editor
            tinymce.init({
                selector: "#pageEditor",
                theme: "modern",
                setup: function (editor) {
                    editor.on("change", function (e) {
                        self.handleChange({target: {name: "content"}});
                    });
                    editor.on("blur", function (e) {
                        self.handleBlur({target: {name: "content"}});
                    });
                    editor.on("init", function (e) {
                        if(self.state.data.id !== null)
                            tinyMCE.activeEditor.setContent(self.state.data.content);
                    });
                },
                height: 250,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons"
            });
        }
    }

    componentWillUnmount() {
        tinymce.remove('#pageEditor');
    }

    loadPage(props) {
        if (this.state.data.id !== null) {
            XhrService.getMtData({
                path: props.path + '/' + this.state.data.id,
                auth: true
            }, this, function (data, ref) {
                if (data.status === 200) {

                    // update state
                    let state = ref.state;
                    state.data = data.response;
                    state.disabled = false;
                    ref.state = state;
                    ref.setState(state);

                    // check filters
                    ref.checkFilters();

                    // set content
                    tinyMCE.get("pageEditor").setContent(ref.state.data.content);
                }
            });
        }
    }

    checkFilters() {

        // enable or disable publish buttons
        let state = this.state;
        state.publishDisabled = !Page.isComplete(state.data);
        this.state = state;
        this.setState(state);
    }


    /**
     * This method will be executed after initial rendering.
     */
    postData(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
        })
    }

    postDataUpdate(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            if (data.response.success === true) {
            }
            else {
                console.warn("could not update");
            }
        })
    }

    onSaveAndClose(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postDataUpdate({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                        browserHistory.goBack();
                });
            }
        } catch(e){console.error(e);}
    }

    onSave(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postDataUpdate({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                    });
            }
        } catch(e){console.error(e);}
    }

    onSaveDraft(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postDataUpdate({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                    });
            }
        } catch(e){console.error(e);}
    }

    /**
     * Render the component.
     */
    render() {
        return (
            <PagesAddComponent onSaveAndClose ={this.onSaveAndClose}
                               onSave={this.onSave}
                               onSaveDraft={this.onSaveDraft}
                               publishDisabled={this.state.publishDisabled}
                               disabled={this.state.disabled}
                               data={this.state.data}
                               handleChange={this.handleChange}
                               handleBlur={this.handleBlur}
                               bootStrap={this.state.bootStrap}
            />
        );
    }

}

export default PagesAdd;
