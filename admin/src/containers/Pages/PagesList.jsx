import React from 'react';
import Auth from '../../modules/Auth';
import PagesListComponent from '../../components/Pages/PagesListComponent.jsx';
import PagesAddComponent from '../../components/Pages/PagesAddComponent.jsx';
class PagesList extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			bootStrapArray:["Pages","/pages/index"]
		};
		this.editClick= this.editClick.bind(this);
	}
 
	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		this.setState({

		});
		/*    const xhr = new XMLHttpRequest();
		 let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
		 xhr.open('get', host);
		 xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		 // set the authorization HTTP header
		 xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
		 xhr.responseType = 'json';
		 xhr.addEventListener('load', () => {
		 if (xhr.status === 200) {
		 this.setState({
		 home: xhr.response.list
		 });
		 }
		 });
		 xhr.send(); */
	}
	editClick()
	{
		console.log("edittty");
		return ( <PagesAddComponent home={this.state.home} bootStrapArray={this.state.bootStrapArray}/> );
	}
	/**
	 * Render the component.
	 */
	render() {
		return (<PagesListComponent home={this.state.home} bootStrapArray={this.state.bootStrapArray} editClick={this.editClick}/>);
	}

}

export default PagesList;
