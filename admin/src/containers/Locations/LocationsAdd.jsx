import React from 'react';
import Auth from '../../modules/Auth';
import LocationsAddComponent from '../../components/Locations/LocationsAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import ImgService from '../../services/ImgService.jsx';
import MapService from '../../services/MapService.jsx';
import Location from '../../models/Location';
import {browserHistory} from 'react-router';

class LocationsAdd extends React.Component {
    xhr = null;
    img = null;
    static defaultProps = {
        path: Config.getPath().location,
        api: Config.getPath().baseRoute,
        tagPath: Config.getPath().tag
    };

    constructor(props) {
        super(props);
        this.state = {
            bootStrapArray: ["Add Location", "/locations/add", "Locations", "/locations/index/1"],
            location: new Location(),
            marker: false,
            disabledState: 1,
            googleMap: ""

        };
        console.log("path=",this.props);
        this.xhr = new XhrService();
        this.img = new ImgService();
        this.onMapClick = this.onMapClick.bind(this);
        this.onMapLoad = this.onMapLoad.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.onImgUpload = this.onImgUpload.bind(this);
        this.onImgDelete = this.onImgDelete.bind(this);
        this.onclickSave =  this.onclickSave.bind(this);
    }
/////////////////////////////////////

    onclickSave(event) {
        console.log("handleclick =", parseInt(event.target.id));
    }
    
    
//////////////edit or add page//////////////////////
    disabledStateChange() {
        if (this.state.disabledState === 0) {
            $('#locationCountry')[0].selectize.enable();
            $("#locationCity")[0].selectize.enable();
            $("#locationType")[0].selectize.enable();
            $("#locationTags")[0].selectize.enable();
        }
        else {
            $('#locationCountry')[0].selectize.disable();
            $("#locationCity")[0].selectize.disable();
            $("#locationType")[0].selectize.disable();
            $("#locationTags")[0].selectize.disable();
        }
    }

    onImgUpload(imgID) {
        console.log("location image upload");
        let self = this;
        console.log("location image upload id = ",self.state.coverImg);
        if(self.state.location.coverImg===null || self.state.location.coverImg===undefined)
        {
            self.state.location.coverImg={};

        }
       self.state.location.coverImg.id = imgID;
        let dataSend;
        dataSend = {
            id: self.state.location.id,
            coverImg: {
                id: self.state.location.coverImg.id
            }
        };
        self.postDataUpdate({path: this.props.path + "/update", auth: true }, dataSend, function (data, ref) {});
    }

    onImgDelete() {
        console.log("movie image delete");
        let self = this;
        self.state.location.coverImg.id = 0;
        let dataSend;
        dataSend = {
            id: self.state.location.id,
            coverImg: {
                id: self.state.location.coverImg.id
            }
        };
        self.postDataUpdate({path: this.props.path + "/update", auth: true }, dataSend, function (data, ref) {});
    }

//////////////////name and asddress label events/////////////////
    handleChange(event) {
      //  console.log(" handlechange name event = ", event.target.name);
        let locate = this.state.location;
        if (event.target.name === "locationName") {
            locate.name = "";
            locate.name = event.target.value;
            this.setState({location: locate});
        }
        else if (event.target.name === "locationAddress") {
            console.log(" handlechange address event=", event.target.value);
            locate.address = {};
            locate.address.name = event.target.value;
            this.setState({location: locate});
        }
    }

    handleBlur(event) {
        let located = this.state.location;
        if (event.currentTarget.name === "locationName") {
            located.name = event.target.value;
            this.setState({location: located});
            let dataSend;
            if (this.state.location.id === null) {
                dataSend = {name: this.state.location.name};
                this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
                });
            }
            else {
                dataSend = {id: this.state.location.id, name: this.state.location.name};
                this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
                });
            }
        }
        else if (event.currentTarget.name === "locationAddress") {
            located.address = {};
            located.address.name = event.target.value;
            this.setState({location: located});
            console.log("handleblur address=", this.state.location.address.name);
            this.mapping();
           
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("componentWillReceiveProps = ", nextProps);
        this.loadPage(nextProps);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        console.log("componenetDidMount = ", this.state.location);
        this.state.location.id = (this.props.params.id === "add") ? null : this.props.params.id;
        this.selectizeProperties();
        this.loadPage(this.props);
    }


    loadPage(props) {
        this.state.location.id = (props.params.id === "add") ? null : props.params.id;
        if (this.state.location.id === null || this.state.location.id===undefined || this.state.location === null) {
            console.log("if =", this.state.location.id);
            let state = {
                bootStrapArray: ["Add Location", "/locations/add", "Locations", "/locations/index/1"],
                location: new Location(),
                marker: false,
                disabledState: 1,
                googleMap: ""
            };
            this.state = state;
            this.setState(state);
        }
        else if (this.state.location.id !== null) {
           // console.log("777777777777777777777===", this.props.path + '/' + this.state.location.id);
            XhrService.getMtData({
                path: this.props.path + '/' + this.state.location.id,
                auth: true
            }, this, function (data, ref) {
                if (data.status === 200) {
                    ref.state.location = data.response;
                    ref.setState({location: data.response});
                   // console.log("EDIT  = ",ref.state.location);
                    if (ref.state.location.tags!==null) {
                       
                        let select1 = $("#locationTags");
                        let selectize11 = select1[0].selectize;
                        selectize11.clearOptions();
                       // console.log("777777= ",ref.state.location.tags[0].id," select edit= ",selectize11.setValue(parseInt(ref.state.location.tags[0].id)));
                      //  selectize11.setValue(ref.state.location.tags[0].id);
                        selectize11.setValue(parseInt(ref.state.location.tags[0].id));
                    }
                }
            });
            
            this.state.disabledState = 0;
            this.disabledStateChange();

        }
        /////////////call selectize function//////////

        this.droImage("MTDropBox", this);
        console.log("url =",this.props);
        this.getData({path: this.props.api + "country/getcountry", auth: true}, function (data, ref) {
            if (data.status === 200) {
                let currentItem;
                let select = $("#locationCountry");
                let selectize = select[0].selectize;
                console.log("select=",select);
                console.log("selectize=",selectize);
                selectize.clearOptions();
                console.log("LOAD SELECT =",data.response);
                for (let i in data.response.countries) {
                    currentItem = data.response.countries[i];
                    let v,t;
                    (currentItem===null ||  currentItem.id===null) ? (v="" , t=""): (v=currentItem.id, t=currentItem.name);
                    selectize.addOption({value: v, text: t});
                }
                console.log("ref.props.params.id=",ref.props.params.id," ref.state.location.country =",ref.state.location.country," ref.state.location.country.id = " );
                if (ref.props.params.id !== undefined && ref.state.location.country !== null) {
                 
                    console.log("LOAD SELECT COUNTRY");
                    selectize.setValue(parseInt(ref.state.location.country.id));

                }
            }
        });
        this.getDatatype({path: this.props.path + "/types", auth: true}, function (data, ref) {
            if (data.status === 200) {
                let currentItem;
                let select = $("#locationType");
                let selectize = select[0].selectize;
                selectize.clearOptions();
                for (let i in data.response) {
                    currentItem = data.response[i];
                    selectize.addOption({value: currentItem.id, text: currentItem.name});
                }
                if (ref.props.params.id !== undefined && ref.state.location.type !== null && ref.state.location.type.id !== undefined) {
                    selectize.setValue(parseInt(ref.state.location.type.id));
                }

            }
        });
    }


    selectizeProperties() {
        let tagpath = this.props.tagPath;
        let citypath = this.props.api;
        let self = this;
        console.log("********** selectizeProperties");
        /////////////selectize ////////////////
        $(".selectize").selectize({
            plugins: ["remove_button", "drag_drop"],
            delimiter: ",",
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
        ////////////// select tag//////////////////
        let select1 =$('#locationTags').selectize({
            persist: false,
            createOnBlur: true,
            onDropdownClose: function (dropdown) {
                console.log("this.state.location.tags =", self.state.location.tags);
                let array = [];
                for (let i = 0; i < self.state.location.tags.length; i++) {
                    let idpart = {id: +self.state.location.tags[i]};
                    array.push(idpart);
                }
              //  console.log("array =", array, "my id=", self.state.location.id);

                let dataSend = {id: self.state.location.id, tags: array};
               // console.log("8888888locationTags array =", array, "my id=", self.state.location.id);
                self.postDataUpdate({path: self.props.path + "/update", auth: true}, dataSend, function (data, ref) {
                });
            },
            create: function (input) {
                console.log("Creat");
                let dataSend = {name: input};
                console.log("creat in", input);
                XhrService.postMtData({
                    path: self.props.tagPath, auth: true
                }, dataSend, this, function (data, ref) {
                    if (data.status === 200) {
                      //  console.log("DATA RESPONSE", data.response);
                    }
                    //console.log("chexav DATA RESPONSE", data.response);
                });

                // self.postData({path: self.props.tagPath, auth: false}, dataSend, function (data, ref) {
                // });

                // self.postDataUpdate({path: self.props.tagPath, auth: false}, dataSend, function (data, ref) {
                // });
                return {
                    value: input,
                    text: input
                }
            },
            load: function (query, callback) {
                console.log("load");
                if (query.length === 0) {
                    callback();
                    return;
                }
                console.log("load");
                XhrService.getMtData({auth: true, path: self.props.tagPath + '/' + query}, {}, function (data, ref) {
                    let resData = data.response;
                    let options = [];
                    if (typeof resData !== undefined && resData !== null) {
                        for (let i in resData) {
                            let temp = {
                                value: resData[i].id,
                                text: resData[i].name
                            };
                            options.push(temp);
                        }
                    }
                    callback(options);
                });
            }
        });
        //let select1 = $("#locationTags");
        let selectize11 = select1[0].selectize;
        console.log("selectize = ==",select1[0].selectize);
       // selectize11.setValue(614)
        selectize11.on('change', function (res) {
            console.log("changeeee");
            let splits = selectize11.getValue().split(",");
            self.state.location.tags === null ? (self.state.location.tags = [], self.state.location.tags.push(splits[splits.length - 1])) : self.state.location.tags.push(splits[splits.length - 1]);
            console.log("in else ", self.state.location.tags);
            
        });

       
        /////////// select country//////////////////
        let select = $("#locationCountry");
        let selectize = select[0].selectize;
        selectize.on('change', function (res) {
            let call;
            console.log("33333locationCountry=", selectize.getItem(selectize.getValue())[0]);
            (self.state.location.country !== null && self.state.location.country.name === selectize.getItem(selectize.getValue())[0].innerText) ?
                (call = false, console.log("HAVASAR EN", self.state.location.country.name, "===", selectize.getItem(selectize.getValue())[0].innerText)) :
                (console.log("HAVASAR CHEEN"),
                    self.state.location.city=null,
                    self.state.location.address=null,
                    console.log("HAVASAR CHEEN  self.state.location.city= ", self.state.location.city," self.state.location.address= ",self.state.location.address),
                    call = true);
            let loc = self.state.location;
            loc.country = {};
            loc.country.id = selectize.getValue();
            loc.country.name = selectize.getItem(selectize.getValue())[0].innerText;
            self.setState({location: loc});
            //console.log("On country id= ", self.state.location.country.id, "  On country name= ", self.state.location.country.name);
            self.getDatacity({
                path: citypath + "country/getcity/" + selectize.getValue(),
                auth: true
            }, function (data, ref) {
               
                if (data.status === 200) {
                    let currentItem;
                    let select1 = $("#locationCity");
                    let selectize1 = select1[0].selectize;
                    selectize1.clearOptions();
                    console.log("DAT country=",data.response);
                    for (let i in data.response.countries) {
                        currentItem = data.response.countries[i];
                        selectize1.addOption({value: currentItem.id, text: currentItem.name});
                    }
                    if (ref.props.params.id !== undefined && ref.state.location.city !== null && ref.state.location.city.id !== undefined) {
                        selectize1.setValue(parseInt(ref.state.location.city.id));
                    }
                    if (call) {
                        ref.mapping();
                    }
                    ref.setState({});
                }
            });

        });
        let selectcity = $("#locationCity");
        let selectizecity = selectcity[0].selectize;
        selectizecity.on('change', function (res) {
            let call;
            ///console.log("2222locationCountry=", selectizecity.getItem(selectizecity.getValue()));
            if (selectizecity.getItem(selectizecity.getValue())[0] !== undefined) {
                (self.state.location.city !== null && self.state.location.city.name === selectizecity.getItem(selectizecity.getValue())[0].innerText) ?
                    (call = false, console.log("HAVASAR EN", self.state.location.city.name, "===", selectizecity.getItem(selectizecity.getValue())[0].innerText)) :
                    (console.log("HAVASAR CHEEN"),
                        self.state.location.address=null,
                        console.log("HAVASAR CHEEN self.state.location.address=",self.state.location.address),
                        call = true);
            }
            let loc = self.state.location;
            loc.city = {};
            loc.city.id = selectizecity.getValue();
            loc.city.name = (selectizecity.getValue() !== "" ) ? selectizecity.getItem(selectizecity.getValue())[0].innerText : null;
            self.setState({location: loc});
           // console.log("On city id= ", self.state.location.city.id, "  On city name= ", self.state.location.city.name);
            if (selectizecity.getValue() !== "") {
                let dataSend = {
                    id: self.state.location.id,
                    country: {id: self.state.location.country.id, name: self.state.location.country.name},
                    city: {id: self.state.location.city.id, name: self.state.location.city.name}
                };
                self.postDataUpdate({path: self.props.path + "/update", auth: true}, dataSend, function (data, ref) {
                });
                // if (call) {
                //     self.mapping();
                // }

            }
        });
        let selecttype = $("#locationType");
        let selectizetype = selecttype[0].selectize;
        selectizetype.on('change', function (res) {
            self.state.location.type = {};
            self.state.location.type.id = selectizetype.getValue();
            let id = self.state.location.id;
            let dataSend = {id: id, type: {id: self.state.location.type.id}};
            self.postDataUpdate({path: self.props.path + "/update", auth: true}, dataSend, function (data, ref) {
            });
        });


    }

///////////////////mapping/////////////////////
    onMapLoad(map) {
        this.state.googleMap = map;
       console.log("111111111111111onMapLoad = ", this.state.googleMap);
        if (this.state.location.address !== null) {
            console.log(" onMapLoad ADRess map=", this.state.location.address, " id=", this.state.location.id, " lat=", this.state.location.address.latitude, "long=", this.state.location.address.longitude);
            let marker = this.state.marker;
            let lt, lg;
            if (this.state.location.id !== null && (this.state.location.address.latitude !== null || this.state.location.address.longitude !== null)) {
                lt = this.state.location.address.latitude;
                lg = this.state.location.address.longitude;
                console.log(" onMapLoad new addres===", this.state.location.address.name, "posit=", this.state.location.address.latitude, " && ", this.state.location.address.longitude);
                marker = new this.state.googleMap.maps.Marker({position: {lat: lt, lng: lg}});
                this.state.googleMap.map.setCenter(marker.getPosition());
                marker.setMap(this.state.googleMap.map);
                this.setState({marker: marker});
                console.log("ONLOAD MAP MARKER=",this.state.googleMap.map);
            }

        }
    }

    onMapClick(map, location) {
        this.state.googleMap.maps.event.clearInstanceListeners(this.state.marker);
        console.log("MAp googlemap onclick",this.state.googleMap.map);
        this.state.googleMap=map;
        let marker = this.state.marker;
        console.log("onmap clicl Marker=",this.state.marker);
       // this.state.googleMap.map.setMap(null); ///delete marker
        console.log("onmap delete clicl Marker=",this.state.marker);
      
        if (marker) {
            console.log("if");
            marker.setPosition(location);
        } else {
            console.log("else");
            marker = new this.state.googleMap.maps.Marker({position: location});
            this.state.googleMap.map.setZoom(16); //zoom map
            marker.setMap(this.state.googleMap.map);
            this.setState({marker: marker});

        }
        console.log("onMapClick =", marker.position.lat() + " - " + marker.position.lng());
        this.state.location.address.latitude=marker.position.lat();
        this.state.location.address.longitude=marker.position.lng();
        if (this.state.location.address !== undefined && this.state.location.address.name !== undefined) {
            let dataSend = {
                id: this.state.location.id,
                address: {
                    name: this.state.location.address.name,
                    latitude: this.state.location.address.latitude,
                    longitude: this.state.location.address.longitude
                }
            };
            this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
            });
        }
    }
    

    mapping() {
        console.log("Mapping contry name =", this.state.location.country.name);
        let city;
        let addres;
        if (this.state.location.city !== null && this.state.location.city.name !== null) {
            city = this.state.location.city.name;
        }
            else{
            city = "";
        }
        if (this.state.location.address !== null && this.state.location.address.name!==undefined) {
           // console.log("ADDRESCFTDSFC=", this.state.location.address);
            addres = this.state.location.address.name;console.log(" Mapping this.state.location.address.name=", this.state.location.address.name);
        }
        else{ addres = "";console.log("addchexaV=",addres);}
        let mappath = Config.getPath().mapAddressSearch + this.state.location.country.name + city + addres + "&key=AIzaSyCmFJ5zhFfnWNo8WSmqZd5CLERccPR-Esk";
        XhrService.getMtData({
            path: mappath,
            auth: false
        }, this, function (data, ref) {
            if (data.status === 200) {
                //console.log("im MAP11 request Path= ", mappath);
               // console.log("im MAP11 request = ", data.response);
                let loc;
                (ref.state.location.address === null || ref.state.location.address.name === null) ? (loc = ref.state.location, loc.address = {}) : loc = ref.state.location;
                ref.state.location.address = loc.address;
                console.log("data.response.results[0]=",data.response.results);
                ref.state.location.address.latitude = data.response.results[0].geometry.location.lat;
                ref.state.location.address.longitude = data.response.results[0].geometry.location.lng;
                if (ref.state.location.address !== null && ref.state.location.address.name !== undefined) {
                    console.log("ADDresss", ref.state.location.address, " &&", ref.state.location.address.name);
                    let marker = ref.state.marker;
                    marker = new ref.state.googleMap.maps.Marker({
                        position: {lat: ref.state.location.address.latitude, lng: ref.state.location.address.longitude}
                    });
                    //ref.state.googleMap.map.setCenter(marker.getPosition());
                    ref.state.googleMap.map.setCenter({
                        lat: ref.state.location.address.latitude,
                        lng: ref.state.location.address.longitude
                    });
                   // ref.state.googleMap.maps.MaxZoomService();
                    ref.state.googleMap.map.setZoom(16);
                    marker.setMap(ref.state.googleMap.map);
                    ref.setState({marker: marker});
                    
                    let located = ref.state.location;
                    located.address.latitude = ref.state.marker.position.lat();
                    located.address.longitude = ref.state.marker.position.lng();
                    ref.setState({location: located});
                    let dataSend = {
                        id: ref.state.location.id,
                        address: {
                            name: ref.state.location.address.name,
                            latitude: ref.state.location.address.latitude,
                            longitude: ref.state.location.address.longitude
                        }
                    };
                    ref.postDataUpdate({path: ref.props.path + "/update", auth: true}, dataSend, function (data, ref) {
                    });


                }
                else {
                    ref.state.googleMap.map.setCenter({
                        lat: ref.state.location.address.latitude,
                        lng: ref.state.location.address.longitude
                    });
                    //ref.setState({marker: marker});
                }
            }
        });
    }

//////////////////////////
    postData(props, dataSend, callback) {
       
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            console.log(data.response);
            if (data.status === 200) {
                ref.state.location.id = data.response.id;
                console.log("PostData = ", dataSend, "  PostData dataresponse===", data.response, "  Props=", props);
                ref.setState({disabledState: 0});
                ref.disabledStateChange();
            }
        })
    }

    postDataUpdate(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            if (data.response.success === true) {
                console.log(" postDataUpdate update dataSend=", dataSend," Krevor=",dataSend.country);
                if (dataSend.country !== undefined && dataSend.city !== undefined) {
                    console.log(" IFFFFF postDataUpdate update dataSend=", dataSend.country);
                    ref.mapping();

                }
               // console.log(" postDataUpdate TAGs=", dataSend,);
               // console.log("postDataUpdate response.data ===", data.response);
                // console.log("call country=", ref.state.location.country.name);
               // console.log(" postDataUpdate call clocate update =", ref.state.location);
                // if (ref.state.location.country.name !== null || ref.state.location.city.name !== null) {
                //     console.log("Map contry name = ", ref.state.location.country.name, ", city name =", ref.state.location.city.name);
                //     ref.mapping();
                // }
                // if (data.response.id !== null) {
                //     console.log("update and tag");
                //     ref.state.location.tags.pop();
                //     ref.state.location.tags.push(parseInt(data.response.id));
                //     console.log("arrtag = ", ref.state.location.tags);
                // }

            }
            else {
                console.log("no noupdate = ", data.response);
            }
        })
    }

    getData(props, callback) {
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);
        })
    }

    getDatacity(props, callback) {
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);

        })
    }

    getDatatype(props, callback) {
        XhrService.getMtData(props, this, function (data, ref) {
            callback(data, ref);
        })
    }

    getDataEdit(props, callback) {
        XhrService.getMtData(props, this, function (data, ref) {
            callback(data, ref);
        })
    }

    droImage(dropzone, self) {
        ImgService.dropImage(dropzone, self);
    }
    
    /**
     * Render the component.
     */
    render() {
        return (<LocationsAddComponent bootStrapArray={this.state.bootStrapArray}
                                       disabledState={this.state.disabledState}
                                       location={this.state.location}
                                       handleChange={this.handleChange}
                                       handleBlur={this.handleBlur}
                                       onMapClick={this.onMapClick}
                                       onMapLoad={this.onMapLoad}
                                       onclickSave={this.onclickSave}/>);
    }
}
export default LocationsAdd;
