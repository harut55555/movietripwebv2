import React from 'react';
import Auth from '../../modules/Auth';
import LocationsUsersAddedComponent from '../../components/Locations/LocationsUsersAddedComponent.jsx';

class LocationsUsersAdded extends React.Component {

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Users' added","/locations/users-added","Locations","/locations/index/1"]
		};
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		this.setState({

		});
		
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<LocationsUsersAddedComponent home={this.state.home} root={this.state.root} />);
	}

}

export default LocationsUsersAdded;
