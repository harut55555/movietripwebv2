import React from 'react';
import Auth from '../../modules/Auth';
import LocationsListComponent from '../../components/Locations/LocationsListComponent.jsx';
import Location from '../../models/Location';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';


import { browserHistory } from 'react-router';


class LocationsList extends React.Component {
	xhr = null;
	static defaultProps = {
		path: Config.getPath().location
	};

	constructor(props) {
		super(props);

		this.state = {
			"home": {},
			root:["Locations","/locations/index"],
			locations: [],
			pagescroll: [],
			value: 10,
			arr: 0,
			countitem: 0,
			max:0,
			page: 1
		};
		this.xhr = new XhrService();
		this.handleChange = this.handleChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.params.page === undefined) {
			nextProps.params.page= 1;
		}
		this.loadPage(nextProps);
	}
		componentDidMount() {
			if (this.props.params.page === undefined) {
				this.props.params.page= 1;
			}
		this.loadPage(this.props);
	}
	loadPage(props) {
		this.state.page = props.params.page;
		this.setState({
			page: props.params.page
		});
		this.props.params.page = props.params.page;
		window.scrollTo(0, 0);
		// get data
		// let mystring = this.state.root[1];
		// this.state.arr=mystring.split("/locations/index/").pop();

		  if((parseInt(props.params.page))%3===0) {

				this.state.pagescroll[0] = parseInt(this.state.page)-2;
			    this.state.pagescroll[1] = parseInt(this.state.page)-1;
			    this.state.pagescroll[2] = parseInt(this.state.page);

			}
		  else if((parseInt(props.params.page))%3===1 || (parseInt(props.params.page))===1)
		  {

			  this.state.pagescroll[0] = parseInt(this.state.page);
			  this.state.pagescroll[1] = parseInt(this.state.page)+1;
			  this.state.pagescroll[2] = parseInt(this.state.page)+2;

		  }
		  else if((parseInt(props.params.page))%3===2) {
			  this.state.pagescroll[0] = parseInt(this.state.page) - 1;
			  this.state.pagescroll[1] = parseInt(this.state.page);
			  this.state.pagescroll[2] = parseInt(this.state.page) + 1;
		  }

		// this.setState({
		// 	arr:  (parseInt(props.params.page)+1).toString()
		// });

		this.getDatacountitem({path: this.props.path + "/count", auth: false}, function () {

		});
		this.getData({path: this.props.path + '/' + this.state.page + '/' + this.state.value, auth: false}, function () {

		});
		this.state.max = (parseInt(this.state.countitem/this.state.value));
		if((this.state.countitem % this.state.value) >  0){this.state.max=this.state.max+1;}
	}

	getData(props, callback) {
		// call xhr service
		this.xhr.getMtData(props, this, function (data, ref) {

			// job is done
			callback();

			// set state
			if (data.status === 200) {
				let locations = [];
				for (let i in data.response) {
					let currentMovie = new Location(data.response[i]);
					locations.push(currentMovie);
				}
				ref.setState({
					locations: locations
				});
			}
		})
	}

	getDatacountitem(props, callback) {
		// call xhr service
		this.xhr.getMtData(props, this, function (data, ref) {

			// job is done
			callback();

			// set state
			if (data.status === 200) {
				     let countitem =0;
				     let currentLocation = data.response;
					countitem =currentLocation;

				ref.setState({
					countitem: countitem
				});
			}
		})
	}

	handleChange(event) {
		this.state.value = event.target.value;
		// this.props.params.page = 1;

		this.props.params.page = 1;
		this.loadPage(this.props);
		browserHistory.push('/locations/index');
	}
	handleClick(event) {

	}

	render() {
		return (<LocationsListComponent home={this.state.home} root={this.state.root} locations={this.state.locations} handleChange={this.handleChange}
										value={this.state.value} pagescroll={this.state.pagescroll}  page={this.state.page} max={this.state.max} />);
	}

}
export default LocationsList;
