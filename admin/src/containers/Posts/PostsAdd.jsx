import React from 'react';
import Auth from '../../modules/Auth';
import PostsAddComponent from '../../components/Posts/PostsAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import ImgService from '../../services/ImgService.jsx';
import MapService from '../../services/MapService.jsx';
import Post from '../../models/Post';
import {browserHistory} from 'react-router';

class PostsAdd extends React.Component {
    xhr = null;
    img = null;
    static defaultProps = {
        path: Config.getPath().post,
        api: Config.getPath().baseroute,
        tagPath: Config.getPath().tag
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);
     
        this.state = {
            bootStrapArray: ["Add Post", "/post/add", "Posts", "/posts/index/1"],
            post: new Post(),
            marker: false,
            disabledState: 1,
            googleMap: "",
            categories: []
        };

      
        this.xhr = new XhrService();
        this.img = new ImgService();
        // this.onMapClick = this.onMapClick.bind(this);
        // this.onMapLoad = this.onMapLoad.bind(this);
         this.handleChange = this.handleChange.bind(this);
         this.handleBlur = this.handleBlur.bind(this);

        // this.onImgUpload = this.onImgUpload.bind(this);
        // this.onImgDelete = this.onImgDelete.bind(this);
        // this.onclickSave =  this.onclickSave.bind(this);
    }

    disabledStateChange() {
        if (this.state.disabledState === 0) {
            $("#blogPostTags")[0].selectize.enable();
        }
        else {
            $("#blogPostTags")[0].selectize.disable();
        }
    }
    // onImgUpload(imgID) {
    //     console.log("post image upload");
    //     let self = this;
    //     console.log("post image upload id = ",self.state.coverImg);
    //     if(self.state.post.coverImg===null || self.state.post.coverImg===undefined)
    //     {
    //         self.state.post.coverImg={};
    //
    //     }
    //     self.state.post.coverImg.id = imgID;
    //     let dataSend;
    //     dataSend = {
    //         id: self.state.post.id,
    //         coverImg: {
    //             id: self.state.post.coverImg.id
    //         }
    //     };
    //     self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {});
    // }
    //
    // onImgDelete() {
    //     console.log("post image delete");
    //     let self = this;
    //     self.state.post.coverImg.id = 0;
    //     let dataSend;
    //     dataSend = {
    //         id: self.state.post.id,
    //         coverImg: {
    //             id: self.state.post.coverImg.id
    //         }
    //     };
    //     self.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {});
    // }
    handleChange(event) {
        //  console.log(" handlechange name event = ", event.target.name);
        console.log(" handlechange name event = ", event.target.value);  
        let post = this.state.post;
        let obj=(event.currentTarget.name.substring(8)).toLowerCase();

        this.setState({post: post});
        if(event.target.name==="blogPostAllowComments")
        {
         //   event.target.value === 1 ? post.commentStatus=1 :post.commentStatus=0;
            console.log(" event.target.value =", event.target.value ,"   post.commentStatus==",post.commentStatus);
            let dataSend = {id: this.state.post.id, commentStatus: event.target.value };
            this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
            });

        }
            else if(event.target.name==="blogPostTop")
        {

            let dataSend = {id: this.state.post.id, top: event.target.value };
            this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
            });
            
        }
        else{

            post[obj]="",
            post[obj]=event.target.value;

        }
    

    }

    handleBlur(event) {
        let post = this.state.post;
        console.log("Auth====",Auth.getAccessToken());
        let dataSend;
        console.log("event.currentTarget.name.substring(8)=",(event.currentTarget.name.substring(8)).toLowerCase());
         let obj=(event.currentTarget.name.substring(8)).toLowerCase();
         post[obj]=event.target.value;
        this.setState({post: post});
        console.log("this.state.post===",this.state.post);
        if (obj === "title" && this.state.post.id === null) {
                console.log("this.state.post.id=",this.state.post.id);
                    console.log("blur title = " ,this.props.path);
                    dataSend = {title: this.state.post.title};
                    this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
                    });
            
            
            }
        else {
            let pass = /^[A-Za-z()'-]{1,100}$/;
            if(obj === "alias" && !(this.state.post.alias.match(pass)))
            {
                console.log("error=",this.state.post.alias);
            }
            else {
                console.log("uELSE=", obj);
                dataSend = {id: this.state.post.id, [obj]: this.state.post[obj]};
                console.log("dataSend  =", dataSend);
                this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
                });
            }
        }
    }
    
    componentWillReceiveProps(nextProps) {
       
        this.loadPage(nextProps);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        
        this.state.post.id = (this.props.params.id === "add") ? null : this.props.params.id;
        this.getData({path: this.props.api + "post/posttypes", auth: true}, function (data, ref) {
            if (data.status === 200) {
                let categories=[];
                for (let i in data.response) {
                    let currentItem = data.response[i];
                    
                    
                   
                    categories.push(currentItem);
                    ref.setState({categories:categories})

                }

            }
        });
        
        this.selectizeProperties();
        this.loadPage(this.props);
    }

    loadPage(props) {
        this.state.post.id = (props.params.id === "add") ? null : props.params.id;
       
        if (this.state.post.id === null || this.state.post === null) {
           
            let state = {
                bootStrapArray: ["Add Post", "/post/add", "Posts", "/posts/index/1"],
                post: new Post(),
                marker: false,
                disabledState: 1,
                googleMap: "",
                categories:[]
            };
            this.state = state;
            this.setState(state);
            (document.getElementById('blogPostTop')).value = this.state.post.top;
            (document.getElementById('blogPostAllowComments')).value = this.state.post.commentStatus;
        }
        else if (this.state.post.id !== null) {
            
            XhrService.getMtData({
                path: this.props.path + '/' + this.state.post.id,
                auth: true
            }, this, function (data, ref) {
                if (data.status === 200) {
                    ref.state.post = data.response;
                   
                    ref.setState({post: data.response});
                 
                    (document.getElementById('blogPostTop')).value = ref.state.post.top;
                    (document.getElementById('blogPostAllowComments')).value = ref.state.post.commentStatus;
                    if (ref.state.post.tags!==null) {

                        let select1 = $("#locationTags");
                        let selectize11 = select1[0].selectize;
                        selectize11.clearOptions();
                      //  console.log("777777= ",ref.state.post.tags[0].id," select edit= ",selectize11.setValue(parseInt(ref.state.post.tags[0].id)));
                        //  selectize11.setValue(ref.state.location.tags[0].id);
                        selectize11.setValue(parseInt(ref.state.post.tags[0].id));
                    }
                }
            });

            this.state.disabledState = 0;
            this.disabledStateChange();

        }
        /////////////call selectize function//////////
        // this.droImage("MTDropBox", this);
       // this.droImage("MTDropBox", this);
    
        $('.deleteMe').unbind().change(function () {

            console.log("Delkegfdgvdted id= ");
               // if ($(this).is(':checked')) {
                    console.log("Delketed id= ",$(this).data('id'));
                   
               // }
            });
    
        
        
    }
    postData(props, dataSend, callback) {
      console.log("Props ===",props);
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
           // console.log(data.response);
            if (data.status === 200) {
                //ref.state.post.id = data.response.id;
            //    console.log("PostData dataresponse===", data.response);
                ref.setState({disabledState: 0});
                let post= ref.state.post;
                post.id=data.response.id;
                post.alias=ref.state.post.title;
                ref.setState({post:post});
                ref.disabledStateChange();
            }
        })
    }

    postDataUpdate(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
           // console.log("postDataUpdate update dataSend=", data.response," datasend = ",dataSend);
            if (data.response.success === true) {
              //  console.log("postDataUpdate update dataSend=", dataSend, " Krevor=");
            }
        })
    }
    getData(props, callback) {
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);
        })
    }
    selectizeProperties() {
        $("#blogPostDate").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 30,
            locale: {
                format: "YYYY/MM/DD HH:mm"
            },
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });

        tinymce.init({
            selector: "#blogPostEditor",
            theme: "modern",
            height: 250,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons"
        });

        $("#blogPostTags").selectize({
            plugins: ["remove_button", "drag_drop"],
            delimiter: ",",
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
    }
    // droImage(dropzone, self) {
    //     ImgService.dropImage(dropzone, self);
    // }

    /**
     * Render the component.
     */
    render() {
        console.log("LOAD Categories=", this.state.categories);
        return (<PostsAddComponent bootStrapArray={this.state.bootStrapArray}
                                   disabledState={this.state.disabledState}
                                   post={this.state.post}
                                   categories={this.state.categories}
                                   handleChange={this.handleChange}
                                   handleBlur={this.handleBlur}/>);
    }

}

export default PostsAdd;
