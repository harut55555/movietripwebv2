import React from 'react';
import Auth from '../../modules/Auth';
import UsersAddComponent from '../../components/Users/UsersAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import User from '../../models/User';
import Config from '../../Config.jsx';
import {browserHistory} from 'react-router';

class UsersAdd extends React.Component {
    xhr = null;
    static defaultProps = {
        path: Config.getPath().user,
        api: Config.getPath().baseRoute
    };
    /**
     * Class constructor.
     */
    constructor(props) {
        super(props);
        this.state = {
            bootStrapArray: ["Add User", "/users/add", "Users", "/users/index"],
            user : new User(),
            password:"",
            confirmpassword:"",
            classcolorerror: {password: [], confirmpassword: [], email: [], userName: []}
        };

        console.log("fgfgfg=",this.props.params.id);
        if (props.params.id === undefined) {
            console.log(props.path);
        }
        else this.getDataEdit({path: this.props.path + "/" + props.params.id, auth: true}, function (data, ref) {
                // set state
                if (data.status === 200) {

                    ref.state.user.email = data.response.email;
                    ref.state.user.userName = data.response.userName;
                    ref.state.user.userRole.name = data.response.userRole.name;
                    ref.state.user.userRole.id = data.response.userRole.id;
                 
                    console.log(ref.state.user.email,
                        ref.state.user.userName,
                        ref.state.user.userRole.name,
                        ref.state.user.userRole.id
                    );

                    //selectize.addOption({value: currentItem.id, text: currentItem.name});
                }
                ref.setState({});
            }
        );
        console.log(props.params.id);
        this.xhr = new XhrService();

        this.handleChange = this.handleChange.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    handleClick(event) {
        console.log("Click button = ", event.target.name);
        //console.log("ahavor = ",event.type);
        let email = this.state.user.email;
        let userName = this.state.user.userName;
        let pass = this.state.password;
        let confirmpass = this.state.confirmpassword;
        let useroleid = this.state.user.userRole.id;
        if (this.state.classcolorerror.email[0] === "has-success" && this.state.classcolorerror.userName[0] === "has-success"
            && this.state.classcolorerror.password[0] === "has-success" && this.state.classcolorerror.confirmpassword[0] === "has-success" && parseInt(this.state.user.userRole.id) > 0) {
            let dataSend = {
                email: email,
                username: userName,
                password: pass,
                confirmpassword: confirmpass,
                userrole: [{id: useroleid}]
            };
            this.postDataUser({path: this.props.path, auth: true}, dataSend, function (data, ref) {
            });
        }
    }

    handleChange( event) {

        let user = this.state.user;
      
        user[event.target.name] = $.trim(event.target.value);
        this.setState({user: user});

    }

    handleBlur( event) {
        let user = this.state.user;
        console.log("pordzana=",event.currentTarget.name );
       user[event.currentTarget.name] = event.currentTarget.value;
        this.setState({user: user});
        if (event.currentTarget.name === "password" || event.currentTarget.name === "confirmpassword") {
           
            let pass = /^[A-Za-z][A-Za-z0-9_]{7,19}$/;
            if ((user[event.currentTarget.name].match(pass)) && ((event.currentTarget.name === "confirmpassword" && this.state.confirmpassword === this.state.user.password) || (event.currentTarget.name !== "confirmpassword"))) {
                this.state.classcolorerror[event.currentTarget.name] = ["has-success", "fa fa-check"];
            }
            else {
                this.state.classcolorerror[event.currentTarget.name] = ["has-error", "fa fa-warning"];
            }
        }
        else if (event.currentTarget.name === "email") {
            let emailreg = /[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,4}$/;
            if (user[event.currentTarget.name].match(emailreg)) {
                let email1 = this.state.user.email;
                let dataSend = {email: email1};
                this.postData({
                    path: this.props.api + "auth" + "/verifyemail",
                    auth: true
                }, dataSend, function (data, ref) {
                });
            }
            else {
                this.state.classcolorerror[event.currentTarget.name] = ["has-error", "fa fa-warning"];
            }

        }
        else if (event.currentTarget.name === "userName") {

            let username = /^(?=.{3,29}$)[a-zA-Z0-9]+([a-zA-Z0-9](_|-)[a-zA-Z0-9])*[a-zA-Z0-9]*$/;
            if (user[event.currentTarget.name].match(username)) {

                this.state.classcolorerror[event.currentTarget.name] = ["has-success", "fa fa-check"];
            }
            else {
                this.state.classcolorerror[event.currentTarget.name] = ["has-error", "fa fa-warning"];
            }
        }
    }
    /**
     * This method will be executed after initial rendering.
     */
    componentDidMount() {
        window.scrollTo(0, 0);
        let self = this;
        $(".selectize").selectize({
            plugins: ["remove_button", "drag_drop"],
            delimiter: ",",
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
        let selecttype = $("#userRole");
        let selectizetype = selecttype[0].selectize;
        selectizetype.on('change', function (res) {
            console.log(selectizetype.getValue());
            self.state.user.userRole.id= selectizetype.getValue();
        });

        this.getDataRole({path: this.props.api + "userrole", auth: true}, function (data, ref) {
            // set state
            if (data.status === 200) {
                let currentItem;
                let select = $("#userRole");
                let selectize = select[0].selectize;
                selectize.clearOptions();
                for (let i in data.response) {
                    currentItem = data.response[i];
                    selectize.addOption({value: currentItem.id, text: currentItem.name});
                }
                if (ref.props.params.id !== undefined && ref.state.user.userRole.id !== undefined) {
                    selectize.setValue(parseInt(ref.state.user.userRole.id));
                }
            }
        });

        
        this.setState({});
    }

    postData(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);

            if (data.response.success === true) {
                console.log(data.response);
                ref.state.classcolorerror.email = ["has-error", "fa fa-warning"];
            }
            else if (data.response.success === false) {
                ref.state.classcolorerror.email = ["has-success", "fa fa-check"];
                ref.state.user.userRole.id = data.response.id;
                console.log("exaaaav", data.response);
            }
        })
    }

    postDataUser(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
            if (data.response.success === false) {
                console.log(data.response);
                console.log("tvec error");

            }
            else console.log("Save request -= ", data.response);
        })
    }

    /**
     * Render the component.
     */
    getDataRole(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);
        })
    }

    getDataEdit(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);
        })
    }

    render() {
        console.log(this.state.classcolorerror.email);
        return (<UsersAddComponent bootStrapArray={this.state.bootStrapArray}
                                   user={this.state.user}
                                   password={this.state.password}
                                   confirmpassword = {this.state.confirmpassword}
                                   classcolorerror={this.state.classcolorerror}
                                   handleChange={this.handleChange}
                                   handleBlur={this.handleBlur}
                                   handleClick={this.handleClick} 
                                  
                                   id={this.props.params.id}/>);
    }

}
export default UsersAdd;
