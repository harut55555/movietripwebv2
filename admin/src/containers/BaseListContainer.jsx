import React from 'react';
import Auth from '../modules/Auth';
import BaseListComponent from '../components/BaseListComponent.jsx';
import Location from '../models/Location';
import Movie from '../models/Movie';
import Scene from '../models/Scene';
import User from '../models/User';
import Post from '../models/Post';
import Page from '../models/Page';
import Trip from '../models/Trip';
import Category from '../models/Category';
import GeoCountries from '../models/GeoCountries';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx';
import BootStrap from '../services/BootStrap';
import SelectizeHelper from '../helpers/SelectizeHelper'
import { browserHistory } from 'react-router';

class BaseListContainer extends React.Component {

    static defaultProps = {
        path: Config.getPath().baseRoute // same as /api/
    };

    constructor(props) {
        super(props);

        this.state = {
            bootStrap: {
                controller: '',
                apiPath: '',
                params: {},
                path: this.props.route.path,
                breadCrumbs: []
            },
            list: [],
            count: 0,
            viewCount: 10,
            modalFunction: null,
            modalData: null,
            ready: true
        };

        // bind functions
        this.onClickSearch = this.onClickSearch.bind(this);
        this.onClickItemDelete = this.onClickItemDelete.bind(this);
        this.onClickDelete = this.onClickDelete.bind(this);
        this.onChangeViewCount = this.onChangeViewCount.bind(this);
        this.onClickModalYes = this.onClickModalYes.bind(this);
        this.onClickModalNo = this.onClickModalNo.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // set page if it is not defined
        if (nextProps.params.page === undefined || nextProps.params.page === null) {
            nextProps.params.page = 1;
        }

        // reset the state
        if (nextProps.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(nextProps);

        // load page again
        this.loadPage();
    }

    componentDidMount() {
        let self = this;

        if (this.props.params.page === undefined || this.props.params.page === null) {
            this.props.params.page = 1;
        }

        // call selectize service for bindings
        SelectizeHelper.bind('.selectize');
        SelectizeHelper.bindStatus(self, function(ref, value) {

            if (self.state.ready)
                browserHistory.push('/'+self.state.bootStrap.controller + '/index');
            // self.getData({
            //     path: self.genPath(),
            //     auth: true
            // }, function () {
            // });
        });

        // reset the state
        if (this.props.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(this.props);

        //bind buttons (delete selected, etc)
        this.bindButtons();

        // load page
        this.loadPage();
    }

    /**
     *
     * @param action
     * @returns {void}
     */
    resetState(){
        let state = this.state;
        state.list = [];
        state.count = 0;
        state.viewCount = 10;
        state.modalFunction = null;
        state.modalData = null;

        // change state for filter change not to redirect recurrent
        this.state.ready = false;

        // reset filters
        $('#SearchFilter').val("");
        $('#showEntriesCount').val(10);
        SelectizeHelper.setValue("#SelectStatus", -1);

        // reset state to ready for redirect
        state.ready = true;

        // save the state
        this.state = state;
        this.setState(state);
    }

    // generate path for search or getting list
    genPath(action) {
        let path = '';

        // get value of search and change link depending on that
        let search = $('#SearchFilter').val();
        // other query values
        let statusValue = SelectizeHelper.getValue('#SelectStatus');

        switch(action) {
            case 'delete':
                path = this.props.path + this.state.bootStrap.apiPath + "/delete";
                break;
            default:
                if (search === '') {    // api/admin/location/startindex/viewCount/status
                    path = this.props.path
                        + this.state.bootStrap.apiPath + '/'
                        + this.state.bootStrap.params.page + '/'
                        + this.state.viewCount
                        + ((statusValue !== '-1') ? ('/' + statusValue) : '');
                }
                else {  // api/admin/location/search/pageIndex/viewCount?q=searchString&status=status
                    path = this.props.path
                        + this.state.bootStrap.apiPath + '/'
                        + 'search' + '/'
                        + this.state.bootStrap.params.page + '/'
                        + this.state.viewCount
                        + '?q=' + search
                        + '&status=' + (statusValue === '-1' ? '' : statusValue);
                }
                break;
        }

        return path;
    }

    // called when component mounted
    bootStrap(props) {
        let state = this.state;
        state.bootStrap = {
            controller: BootStrap.getControllerName(props),
            params: props.params,
            path: props.route.path,
            breadCrumbs: BootStrap.getBreadCrubs(props)
        };

        state.bootStrap.apiPath = state.bootStrap.breadCrumbs[0].apiPath;

        // update state
        this.state = state;
        this.setState(state);
    }

    loadPage() {
        // scrool to top
        window.scrollTo(0, 0);

        // get data of list;
        this.getData({
            path: this.genPath(),
            auth: true
        }, function () {
        });
    }

    // bind buttons
    bindButtons() {

    }

    // on search button
    onClickSearch(event) {
        if (this.state.ready)
            browserHistory.push('/'+this.state.bootStrap.controller + '/index');
    }

    onChangeViewCount(event) {
        this.state.viewCount = event.target.value;
        this.setState({viewCount: event.target.value});

        if (this.state.ready)
            browserHistory.push('/'+this.state.bootStrap.controller + '/index');
    }

    // delete item
    onClickItemDelete(event) {

        // item id to delete
        let dataSend = [];
        dataSend.push(parseInt(event.target.id));

        // check length
        if (dataSend.length !== 0) {

            // save modal function and data state
            let state = this.state;
            state.modalData = dataSend;
            state.modalFunction = function(ref, data) {

                // enable laoder
                ref.setLoaderStatus(true);

                // delete action
                XhrService.postMtData({
                    path: ref.genPath('delete'),
                    auth: true
                }, data, ref, function (data, ref_) {

                    if (data.response) {
                        // delete modal previous function and data
                        let state = ref_.state;
                        state.modalFunction = null;
                        state.modalData = null;
                        ref_.setState(state);

                        // reload the data
                        ref_.loadPage();
                    }
                });
            };
            this.setState(state);

            // show modal
            $("#ModalYesNo").modal("show");
        }
    }

    setLoaderStatus(action) {
        if (action)
            $('.listTable').addClass('loading');
        else
            $('.listTable').removeClass('loading');
    }

    // delete items
    onClickDelete(event) {

        // array to send
        let dataSend = [];
        // collect array
        $('.deleteMe').each(function () {
            // if is checked
            if ($(this).is(':checked')) {
                dataSend.push($(this).data('id'));
            }
        });

        // check length
        if (dataSend.length !== 0) {
            // save modal function and data state
            let state = this.state;
            state.modalData = dataSend;
            state.modalFunction = function (ref, data) {

                // enable laoder
                ref.setLoaderStatus(true);

                // delete action
                XhrService.postMtData({
                    path: ref.genPath('delete'),
                    auth: true
                }, data, ref, function (data, ref_) {

                    if (data.response) {
                        // delete modal previous function and data
                        let state = ref_.state;
                        state.modalFunction = null;
                        state.modalData = null;
                        ref_.setState(state);

                        // reload the data
                        ref_.loadPage();
                    }
                });
            };
            this.setState(state);

            // show modal
            $("#ModalYesNo").modal("show");
        }
    }

    // get data function
    getData(props, callback) {
        let self = this;

        // add loader
        self.setLoaderStatus(true);

        // call xhr service
        XhrService.getMtData(props, self, function (data, ref) {

            // remove loader
            self.setLoaderStatus(false);

            // job is done
            callback();
            // set state
            if (data.status === 200) {

                let arr = [];

                let currentCount = data.response.count;

                let controller = self.state.bootStrap.controller;

                let currentItem;

                for (let i in data.response[controller]) {
                    if (controller === "locations") {
                        currentItem = new Location(data.response[controller][i]);
                    }
                    else if (controller === "movies") {
                        currentItem = new Movie(data.response[controller][i]);
                    }
                    else if (controller === "scenes") {
                        currentItem = new Scene(data.response[controller][i]);
                    }
                    else if (controller === "trips") {
                        currentItem = new Trip(data.response[controller][i]);
                    }
                    else if (controller === "posts") {
                        currentItem = new Post(data.response[controller][i]);
                    }
                    else if (controller === "users") {
                        currentItem = new User(data.response[controller][i]);
                    }
                    else if (controller === "pages") {
                        currentItem = new Page(data.response[controller][i]);
                    }
                    else if (controller === "countries") {
                        currentItem = new GeoCountries(data.response[controller][i]);
                    }
                    else if (controller === "category") {
                        currentItem = new Category(data.response[controller][i]);
                    }
                    arr.push(currentItem);
                }

                ref.setState({list: arr, count: currentCount});
            }
        })
    }

    onClickModalYes() {
        try {
            if (typeof this.state.modalFunction === "function") {
                this.state.modalFunction(this, this.state.modalData)
            }
        } catch(e){
            console.error(e);
        }
    }

    onClickModalNo() {

        // delete previous modal function and data
        let state = this.state;
        this.state.modalData = null;
        this.state.modalFunction = null;
        this.setState(state);
    }

    paginationDefine(props) {

    }

    render() {
            return (<BaseListComponent state={this.state}
                                       list={this.state.list}
                                       count={this.state.count}
                                       viewCount={this.state.viewCount}
                                       onChangeViewCount={this.onChangeViewCount}
                                       bootStrap={this.state.bootStrap}
                                       onClickSearch={this.onClickSearch}
                                       onClickItemDelete={this.onClickItemDelete}
                                       onClickDelete={this.onClickDelete}
                                       onClickModalYes={this.onClickModalYes}
                                       onClickModalNo={this.onClickModalNo}
                                       />)
    }

}
export default BaseListContainer;