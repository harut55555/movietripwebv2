import React from 'react';
import Root from '../components/Root'

class RootPage extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    /**
     * Render the component.
     */
    render() {
        return (<Root
                      children={this.props.children}
                />);
    }
}

export default RootPage;