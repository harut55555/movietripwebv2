import React, {Component} from 'react';
import {render} from 'react-dom';
import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';

const DragHandle = SortableHandle(() => <span className="dragHandle"></span>);

const SortableItem = SortableElement(({index, indexs, point, pointEditClick}) => {
	let movie = "";
	let scene = "";
	let img = "";
	let location = "";
	let description = "";
	console.log(point);
	if (point.type === "scene") {
		movie = point.scenePoint.movie.name;
		scene = point.scenePoint.scene.location;
		img = point.scenePoint.scene.img;
		description = point.scenePoint.description;s
		return (
		 <li className="tripPointsIn">
			 <DragHandle />
			 <div className="pointsWrapInImg">
				 <div className="pointsWrapInImgInside" style={{backgroundImage: "url('" + img + "')"}}></div>
			 </div>
			 <div className="pointsWrapInText">
				 <h2>Movie: {movie}</h2>
				 <h3>Scene: {scene}</h3>
				 <p>{description}</p>
			 </div>
			 <div className="pointsWrapInTextToolbox">
				 <div className="btn-group btn-group-vertical btn-group-sm nowrap">
					 <button type="button" data-curpoint={indexs} onClick={pointEditClick} title="Edit" className="btn btn-flat btn-info"><i data-curpoint={indexs} className="fa fa-pencil"></i></button>
					 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
				 </div>
			 </div>
		 </li>
		)
	}
	else {
		location = point.locationPoint.location.name;
		img = point.locationPoint.location.img;
		description = point.locationPoint.description;
		return (
		 <li className="tripPointsIn">
			 <DragHandle />
			 <div className="pointsWrapInImg">
				 <div className="pointsWrapInImgInside" style={{backgroundImage: "url('" + img + "')"}}></div>
			 </div>
			 <div className="pointsWrapInText">
				 <h2>Location: {location}</h2>
				 <p>{description}</p>
			 </div>
			 <div className="pointsWrapInTextToolbox">
				 <div className="btn-group btn-group-vertical btn-group-sm nowrap">
					 <button type="button" data-curpoint={indexs} onClick={pointEditClick} title="Edit" className="btn btn-flat btn-info"><i data-curpoint={indexs} className="fa fa-pencil"></i></button>
					 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
				 </div>
			 </div>
		 </li>
		)
	}
});

const SortableList = SortableContainer(({items, pointEditClick}) => {
	return (
	 <ul className="tripPoints">
		 {items.map((value, index) => (
			<SortableItem key={`item-${index}`} index={index} indexs={index} point={items[index]} pointEditClick={pointEditClick}/>
		 ))}
	 </ul>
	);
});

class SortableComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: props.items
		};
		this.arrayChanged = props.arrayChanged.bind(this);
		this.pointEditClick = props.pointEditClick.bind(this);
	}

	onSortEnd = ({oldIndex, newIndex}) => {
		let tripPointsArray = arrayMove(this.state.items, oldIndex, newIndex);
		this.setState({
			items: tripPointsArray
		});
		this.arrayChanged(tripPointsArray);
	};

	render() {
		return (<SortableList useDragHandle={true} items={this.state.items} onSortEnd={this.onSortEnd} pointEditClick={this.pointEditClick} />);
	}
}

export default SortableComponent;