import React from 'react';
import Auth from '../../modules/Auth';

import Trip from '../../models/Trip';
import XhrService from '../../services/XhrService.jsx';
import Config from '../../Config.jsx';
import TripListComponent from '../../components/Trip/TripListComponent.jsx';

class TripList extends React.Component {

	xhr = null;
	static defaultProps = {
		path: Config.getPath().trip
	};

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);

		this.state = {
			"home": {

			},
			root:["Trip","/trip/index"],
			trips: []
		};
		this.xhr = new XhrService();
	}

	/**
	 * This method will be executed after initial rendering.
	 */
	componentDidMount() {
		window.scrollTo(0, 0);

		console.log(this.props.path);
		// get data
		this.getData({path: this.props.path, auth: false}, function () {

		});

		this.setState({

		});
		/*    const xhr = new XMLHttpRequest();
		 let host = location.protocol + "//" + location.hostname + ":" + "3000/api/get_products";
		 xhr.open('get', host);
		 xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		 // set the authorization HTTP header
		 xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
		 xhr.responseType = 'json';
		 xhr.addEventListener('load', () => {
		 if (xhr.status === 200) {
		 this.setState({
		 home: xhr.response.list
		 });
		 }
		 });
		 xhr.send(); */
	}

	getData(props, callback) {


		// call xhr service
		this.xhr.getMtData(props, this, function (data, ref) {

			// job is done
			callback();

			// set state
			if (data.status === 200) {
				let trips = [];
				for (let i in data.response) {
					let currentTrip = new Trip(data.response[i]);
					trips.push(currentTrip);
					console.log(currentTrip);

				}

				ref.setState({
					trips: ref.state.trips.concat(trips)
				});
			}
		})
	}

	/**
	 * Render the component.
	 */
	render() {
		return (<TripListComponent home={this.state.home} root={this.state.root} trips={this.state.trips} />);
	}

}

export default TripList;
