import React from 'react';
import Auth from '../../modules/Auth';
import TripAddComponent from '../../components/Trip/TripAddComponent.jsx';
import XhrService from '../../services/XhrService.jsx';
import ImgService from '../../services/ImgService.jsx';
import Config from '../../Config.jsx';
import Trip from '../../models/Trip';
import BootStrap from '../../services/BootStrap';
import { browserHistory } from 'react-router';

class TripAdd extends React.Component {

    static defaultProps = {
        path: Config.getPath().trip
    };

	/**
	 * Class constructor.
	 */
	constructor(props) {
		super(props);
		
		this.state = {
            bootStrap: {
                controller: '',
                apiPath: '',
                params: {},
                path: this.props.route.path,
                breadCrumbs: []
            },
			data: new Trip(),
			disabled: true,
            publishDisabled: true
		};

		this.handleBlur = this.handleBlur.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.onMapClick = this.onMapClick.bind(this);
		this.onMapLoad = this.onMapLoad.bind(this);
		this.onSaveAndClose = this.onSaveAndClose.bind(this);
		this.onSave = this.onSave.bind(this);
		this.onSaveDraft = this.onSaveDraft.bind(this);
	}

    componentWillReceiveProps(nextProps) {
        // bind events
        this.bindEvents();

        // reset the state
        if (nextProps.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(nextProps);

        // load page
        this.loadPage(nextProps);
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        // update id
        let state = this.state;
        state.data.id = (this.props.params.id === "add") ? null : this.props.params.id;
        this.state = state;
        this.setState(state);

        // bind events
        this.bindEvents();

        // reset the state
        if (this.props.route.path !== this.state.bootStrap.path)
            this.resetState();

        // call bootstrap
        this.bootStrap(this.props);

        // load page
        this.loadPage(this.props);
    }

    bindEvents() {
        ImgService.dropImage("MTDropBox", this);
	}

    onImgUpload(res){
		if (res.success) {
		    let state = this.state;
		    state.data.cover = res.id;
		    this.state = state;
		    this.setState(state);

		    // save changes
            this.postData({path: this.props.path + "/update/", auth: true},
                {id: this.state.data.id, coverImg: {id:this.state.data.cover}},
                function (data, ref) {
            });
        }
	}

	loadPage(props) {
        if (this.state.data.id !== null) {
            XhrService.getMtData({
                path: props.path + '/' + this.state.data.id,
                auth: true
            }, this, function (data, ref) {
                if (data.status === 200) {

                    // update state
                    let state = ref.state;
                    state.data = data.response;
                    state.disabled = false;
                    ref.state = state;
                    ref.setState(state);

                    // check filters
                    ref.checkFilters();

                    // get picture
                    if (data !== null && data.coverImg !== null)
                        ImgService.getMediaByID(data.response.coverImg.id, ref, function (data_, ref_) {

                            console.log(data);
                            // add cover
                            //ImgService.setImage("MTDropBox", ref_);
                        });
                }
            });
        }
	}

	resetState(){

	}

    // called when component mounted
    bootStrap(props) {
        let state = this.state;
        state.bootStrap = {
            controller: BootStrap.getControllerName(props),
            params: props.params,
            path: props.route.path,
            breadCrumbs: BootStrap.getBreadCrubs(props)
        };

        state.bootStrap.apiPath = state.bootStrap.breadCrumbs[0].apiPath;

        // update state
        this.state = state;
        this.setState(state);
    }

	handleChange(event) {
        let state = this.state;
        let data = state.data;
		data[event.target.name] = event.target.value;
        this.state = state;
        this.setState(state);

        // check filters
        this.checkFilters();
	}

	handleBlur(event) {
        let dataSend;
        let state = this.state;

        if (state.data.id === null) {
            // create new page, set title and get id
            dataSend = {title: state.data.title};
            this.postData({path: this.props.path, auth: true}, dataSend, function (data, ref) {
                if (data.status === 200) {
                    ref.state.data.id = data.response.id;
                    ref.state.disabled = false;
                    ref.setState(ref.state);
                }
            });
        }
        else {
        	dataSend = {id: state.data.id};
        	dataSend[event.target.name] = event.target.value;
            this.postData({path: this.props.path + "/update/", auth: true}, dataSend, function (data, ref) {

            });
        }
	}

	checkFilters() {
		// console.log(this.state.data);
	}

	onMapLoad(map) {
		//console.log("map=",map.mapProps);
		//let marker=this.state.marker;
		//console.log("nn =" ,this.state.nn);
		//let v=this.state.nn.lat;
		//var latlng = new google.maps.LatLng(latitude, longitude);
		// map.setCenter({lat: 60.03192969911561, lng: 30.113525390625});
		// let marker = new map.maps.Marker({
		// 	position: {lat: 60.03192969911561, lng: 30.113525390625}
		// });
		// marker.setMap(map.map);
		// this.setState({marker: marker});
	}

	onMapClick(map, location) {
		let marker = this.state.marker;
		if (marker) {
			marker.setPosition(location);
		} else {
			marker = new map.maps.Marker({
				position: location
			});
			marker.setMap(map.map);
			this.setState({marker: marker});
		}
		// console.log(this.state.marker);
		// console.log(marker.position.lat() + " - " + marker.position.lng());


		// if (this.state.label.address.length > 0) {
		// 	this.locate();
		// }
	}

	// locate() {
	// 	let address1 = this.state.label.address;
	// 	console.log("arjeq@= ", this.state.label.address, this.state.marker.position.lat() + " - " + this.state.marker.position.lng());
	// 	let id = this.state.idpropertys.id;
	// 	let lat = this.state.marker.position.lat();
	// 	let lng = this.state.marker.position.lng();
	// 	let dataSend = {id: id, address: {name: address1, latitute: lat, longtitude: lng}};
	// 	this.postDataUpdate({path: this.props.path + "/update", auth: true}, dataSend, function (data, ref) {
	// 	});
	// }

    onSaveAndClose(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postData({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                        browserHistory.goBack();
                    });
            }
        } catch(e){console.error(e);}
    }

    onSave(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postData({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                    });
            }
        } catch(e){console.error(e);}
    }

    onSaveDraft(event) {
        try {
            let status = parseInt(event.target.name);
            if (this.state.data.id !== null && status !== null) {
                // update status
                this.postData({path: this.props.path + "/update/", auth: true},
                    {"id": this.state.data.id, "status": status}, function (data, ref) {
                    });
            }
        } catch(e){console.error(e);}
    }

    getData(props, callback) {
        // call xhr service
        XhrService.getMtData(props, this, function (data, ref) {
            // job is done
            callback(data, ref);

        })
    }

    postData(props, dataSend, callback) {
        XhrService.postMtData(props, dataSend, this, function (data, ref) {
            callback(data, ref);
        })
    }

	/**
	 * Render the component.
	 */
	render() {
		return (<TripAddComponent
								data={this.state.data}
								disabled={this.state.disabled}
								handleBlur={this.handleBlur}
								handleChange={this.handleChange}
								onMapClick={this.onMapClick}
								onMapLoad={this.onMapLoad}
								onSaveAndClose={this.onSaveAndClose}
								onSave={this.onSave}
								onSaveDraft={this.onSaveDraft}
								publishDisabled={this.state.publishDisabled}
		/>);
	}

}

export default TripAdd;
