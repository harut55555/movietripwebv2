import React from 'react';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm/LoginForm.jsx';
import XhrService from '../services/XhrService.jsx';
import Config from '../Config.jsx';
import {browserHistory} from 'react-router';

class LoginPage extends React.Component {

    static defaultProps = {
        login: Config.getPath().login,
        loginPath: Config.getPath().login
    };

    /**
    * Class constructor.
    */
    constructor(props, context) {
        super(props, context);

        // set the initial component state
        this.state = {
            errors: {
                summary: ''
            },
            successMessage: null,
            email: '',
            password: ''
        };

        this.handleLogin = this.handleLogin.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    loaderStatus(status) {
        if (status)
            $('.loginButton').addClass('loading');
        else
            $('.loginButton').removeClass('loading');
    }

    handleLogin(event) {

        this.loaderStatus(true);

        event.preventDefault();
        // create a string for an HTTP body message
        let email = this.state.email;
        let password = this.state.password;
        let dataSend = {email: email, password: password};

        XhrService.postMtData({path: this.props.loginPath, auth: false}, dataSend, this, function (data, ref) {

            ref.loaderStatus(false);

            if (data.status === 400) {
                let oldState = ref.state;
                oldState.errors.summary = data.response;
                ref.setState(oldState);
            }
            else if (data.status === 200) {
                if (Auth.authenticateUser(data.response)) {

                    ref.setState({
                        isAuthenticated: Auth.isUserAuthenticated()
                    });
                    // redirect to my
                    browserHistory.push('/');
                }
                else {
                    let oldState = ref.state;
                    oldState.errors.summary = Config.getError(500);
                    ref.setState(oldState);
                }
            }
        })
    }


  /**
   * Render the component.
   */
    render() {
        return (
          <LoginForm
              handleEmailChange={this.handleEmailChange}
              handlePasswordChange={this.handlePasswordChange}
              onSubmit={this.handleLogin}
              errors={this.state.errors}
              successMessage={this.state.successMessage}
          />
        );
    }
}

export default LoginPage;
