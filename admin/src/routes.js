import BasePage from './containers/BasePage.jsx';
import RootPage from './containers/RootPage.jsx';
import LoginPage from './containers/LoginPage.jsx';
import DashboardPage from './containers/Dashboard/DashboardPage.jsx';
import PagesAdd from './containers/Pages/PagesAdd.jsx';
import PostsAdd from './containers/Posts/PostsAdd.jsx';
import TripAdd from './containers/Trip/TripAdd.jsx';
import TripUsersAdded from './containers/Trip/TripUsersAdded.jsx';
import ScenesAdd from './containers/Scenes/ScenesAdd.jsx';
import ScenesUsersAdded from './containers/Scenes/ScenesUsersAdded.jsx';
import LocationsAdd from './containers/Locations/LocationsAdd.jsx';
import LocationsUsersAdded from './containers/Locations/LocationsUsersAdded.jsx';
import MoviesAdd from './containers/Movies/MoviesAdd.jsx';
import MoviesUsersAdded from './containers/Movies/MoviesUsersAdded.jsx';
import CitiesList from './containers/Geo/CitiesList.jsx';
import GeoAdd from './containers/Geo/GeoAdd.jsx';
import UsersAdd from './containers/Users/UsersAdd.jsx';
import BaseListContainer from './containers/BaseListContainer.jsx';

import Auth from './modules/Auth';

const routes = {
    component: RootPage,
    childRoutes: [
        {
            onEnter: requireAuth,
            component: BasePage,
            childRoutes: [
                {
                    id: "main",
                    parents: [],
                    path: '/',
                    key: 'main',
                    type: "main",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, DashboardPage)
                    }
                },
                {
                    id: "pagesIndex",
                    parents: [],
                    path: '/pages/index(/:page)',
                    apiPath: 'page',
                    key: 'pages',
                    name: 'Pages',
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    id: "pagesAdd",
                    parents: ["pagesIndex"],
                    path: '/page/:id',
                    apiPath: 'page',
                    key: 'pages',
                    name: 'Pages Add',
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, PagesAdd)
                    }
                },
                {
                    path: '/posts/index(/:page)',
                    apiPath: 'post',
                    key: 'posts',
                    name: 'Posts',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/post/:id',
                    apiPath: 'post',
                    key: 'posts',
                    name: 'Posts Add',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, PostsAdd)
                    }
                },
                {
                    path: '/category/index',
                    apiPath: 'post',
                    key: 'categories',
                    name: 'Categories',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                //{
                //	path: '/trip/index',
                //	getComponent: (location, callback) => {
                //		callback(null, TripList)
                //	}
                //},
                {
                    path: '/trips/index(/:page)',
                    apiPath: 'trip',
                    key: 'trips',
                    name: 'Trips',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/trip/:id',
                    apiPath: 'trip',
                    key: 'trips',
                    name: 'Trip Add',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, TripAdd)
                    }
                },

                {
                    path: '/trips/users-added',
                    apiPath: 'trip',
                    key: 'trips',
                    name: 'Trips User Add',
                    type: "addUser",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, TripUsersAdded)
                    }
                },
                //{
                //	path: '/scenes/index',
                //	getComponent: (location, callback) => {
                //		callback(null, ScenesList)
                //	}
                //},
                {
                    path: '/scenes/index(/:page)',
                    apiPath: 'scene',
                    key: 'scenes',
                    name: 'Scenes',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/scenes/:id',
                    apiPath: 'scene',
                    key: 'scenes',
                    name: 'Scene Edit',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, ScenesAdd)
                    }
                },
                {
                    path: '/scenes/users-added',
                    apiPath: 'scene',
                    key: 'scenes',
                    name: 'Scene User Add',
                    type: "addUser",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, ScenesUsersAdded)
                    }
                },
                // {
                // 	path: '/locations/index(/:page)',
                // 	getComponent: (location, callback) => {
                // 		console.log(callback);
                // 		callback(null, LocationsList)
                // 	}
                // },

                {
                    path: '/locations/index(/:page)',
                    apiPath: 'location',
                    key: 'locations',
                    name: 'Locations',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/location/:id',
                    apiPath: 'location',
                    key: 'locations',
                    name: 'Location Edit',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, LocationsAdd);
                    }
                },
                {
                    path: '/locations/users-added',
                    apiPath: 'location',
                    key: 'locations',
                    name: 'Location User Add',
                    type: "addUser",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, LocationsUsersAdded)
                    }
                },
                {
                    path: '/movies/index(/:page)',
                    apiPath: 'movie',
                    key: 'movies',
                    name: 'Movies',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/movie/:id',
                    apiPath: 'movie',
                    key: 'movies',
                    name: 'Movie Edit',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, MoviesAdd)
                    }
                },
                {
                    path: '/movies/users-added',
                    apiPath: 'movie',
                    key: 'movies',
                    name: 'Movie User Add',
                    type: "addUser",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, MoviesUsersAdded)
                    }
                },
                {
                    path: '/countries/index(/:page)',
                    key: 'countries',
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/geo/cities/index',
                    key: 'geo',
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, CitiesList)
                    }
                },
                {
                    path: '/geo/:id',
                    key: 'geo',
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, GeoAdd)
                    }
                },
                //{
                //	path: '/users/index',
                //	getComponent: (location, callback) => {
                //		callback(null, UsersList)
                //	}
                //},
                {
                    path: '/users/index(/:page)',
                    apiPath: 'user',
                    key: 'users',
                    name: 'Users',
                    type: "index",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, BaseListContainer)
                    }
                },
                {
                    path: '/users/:id',
                    apiPath: 'user',
                    key: 'users',
                    name: 'Users Add',
                    type: "add",
                    onEnter: requireAuth,
                    getComponent: (location, callback) => {
                        callback(null, UsersAdd)
                    }
                }
            ]
        },
        {
            path: '/login',
            key: 'login',
            onEnter: requireUnAuth,
            getComponent: (location, callback) => {
                callback(null, LoginPage);
            }
        }
    ]

};

function requireAuth(nextState, replaceState) {
    if (!Auth.isUserAuthenticated()) {
        replaceState('/login')
    }
}

function requireUnAuth(nextState, replaceState) {
    if (Auth.isUserAuthenticated()) {
        // replaceState({ nextPathname: nextState.location.pathname }, '/my')
        replaceState('/')
    }
}

export default routes;
