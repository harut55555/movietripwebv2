class Auth {

  /**
   * Authenticate a user. Save a token string in Local Storage
   *
   * @param {string} token
   */
  static authenticateUser(data) {

    if (data !== undefined && data !== null) {
      localStorage.setItem('access_token', data.access_token);
      localStorage.setItem('Access_TokExpTime', data.Access_TokExpTime);
      localStorage.setItem('refresh_token', data.refresh_token);
      localStorage.setItem('Ref_TokExpTime', data.Ref_TokEx0pTime);

      return true;
    }

    return false;
  }

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
  static isUserAuthenticated() {
    return (this.getRefreshToken() !== null);
  }

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  static deauthenticateUser() {

    localStorage.removeItem('access_token');
    localStorage.removeItem('Access_TokExpTime');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('Ref_TokExpTime');
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  static getRefreshToken() {
    return localStorage.getItem('refresh_token');
  }

  static getAccessToken() {
    return localStorage.getItem('access_token');
  }

  static getAccessTokenTimeToken() {
    return localStorage.getItem('Access_TokExpTime');
  }

  static getRefreshTokenTimeToken() {
    return localStorage.getItem('Ref_TokExpTime');
  }

}

export default Auth;
