import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import { browserHistory } from 'react-router';
import routes from '../routes';

class BootStrap
{

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    /**
     * get controller name
     */
    static getControllerName(props) {
        let pathName = props.location.pathname;
        let arr = pathName.split("/");
        return arr[1];
    }

    /**
     * Generate BreadCrumbs
     */
    static getBreadCrubs(props) {

        let pathName = props.location.pathname;
        let _arr = pathName.split('/');
        let controller = _arr[1];
        let arr = BootStrap.getRoutesArray('/' + controller + '/');
        return arr;
        // check if it has child

    }

    /**
     * Get array of current routes
     */
    static getRoutesArray(currentRoute) {
        let arr = [];
        let obj = {};
        let _routes = routes.childRoutes[0].childRoutes;
        for (let i in _routes) {
            if (_routes[i].path.indexOf(currentRoute) !== -1) {
                obj.key = _routes[i].key;
                break;
            }
        }

        for (let i in _routes) {
            if (_routes[i].key === obj.key) {
                let curObj = {};
                let _arr = _routes[i].path.split('/');
                curObj.controller = _arr[1];
                curObj.path = _routes[i].path;
                curObj.name = _routes[i].name;
                curObj.key = _routes[i].key;
                curObj.apiPath = _routes[i].apiPath;
                curObj.type = _routes[i].type;
                arr.push(curObj);
            }
        }

        return arr;
    }

    /**
     * Get link of path
     */
    static getLink(currentPath, breadCrumbs) {
        let link = '/';
        for (let i in breadCrumbs) {
            link += breadCrumbs[i].apiPath + currentPath;
            break;
        }
        return link;
    }
}

export default BootStrap;
