import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import XhrService from './XhrService.jsx';

class ImgService {
	
	/**
	 * Class constructor.
	 */
	
	constructor(props) {
	
	}
	
	static postImg(props, dataSend, callback) {
		XhrService.postMtData(props, dataSend, this, function (data, ref) {
			callback(data, ref);
		})
	}
	
	static getLink(img) {
		let path = (img !== undefined && img !== null) ? img.bigPath : null;
		let protocol = Config.getProtocol();
		let host = Config.getHostname();
		let port = Config.getPort();
		return protocol + '//' + host + ':' + port + path;
	}
	
	static getImage(img) {
		let path = (img !== undefined && img !== null) ? img.bigPath : null;
		let protocol = Config.getProtocol();
		let host = Config.getHostname();
		let port = Config.getPort();
		return protocol + '//' + host + ':' + port + path;
	}

	static getMediaByID(id, ref, callback) {
        XhrService.getMtData({path: Config.getPath().mediaGetById + "/" + id, auth: true},
			ref,
			function (data, ref_) {
            callback(data, ref_);
        })
	}

	static setImage(dropzone, self) {

	}
	
	static dropImage(dropzone, self) {
		let mainself = self;
		let count = 1;
		let imageType = /^image\//;
		let filesArr = [];
		let filesArrGeneral = [];
		let MTDropBoxInput = document.getElementById("MTDropBoxInput");
		let uploadpath = Config.getPath().mediaupload;
		let deletepath = Config.getPath().mediadelete;
		let dzoneobj = document.getElementById(dropzone);
		// dzoneobj.addEventListener("dragenter", dragenter, false);
		// dzoneobj.addEventListener("dragleave", dragleave, false);
		// dzoneobj.addEventListener("dragover", dragover, false);
		dzoneobj.addEventListener("drop", drop, false);
		MTDropBoxInput.onchange = function () {
			handleFiles(MTDropBoxInput.files);
		};
		//
		// function dragenter(e) {
		// 	console.log("dragenter");
		// 	e.stopPropagation();
		// 	e.preventDefault();
		// 	dzoneobj.classList.remove("dragleave");
		// 	dzoneobj.classList.add("dragenter");
		// }
		//
		// function dragleave(e) {
		// 	console.log("dragleave");
		// 	e.stopPropagation();
		// 	e.preventDefault();
		// 	dzoneobj.classList.remove("dragenter");
		// 	dzoneobj.classList.add("dragleave");
		// }
		//
		// function dragover(e) {
		// 	console.log("dragover");
		// 	e.stopPropagation();
		// 	e.preventDefault();
		// }
		//
		function drop(e) {
			e.stopPropagation();
			e.preventDefault();
			// dzoneobj.classList.remove("dragenter");
			// dzoneobj.classList.add("dragleave");
			
			let dt = e.dataTransfer;
			let files = dt.files;
			
			if (filesArrGeneral.length < count) {
				for (let i = 0; i < count; i++) {
					if (typeof files[i] !== "undefined") {
						if (!imageType.test(files[i].type)) {
							continue;
						}
						filesArrGeneral.push(files[i]);
					}
					else {
					}
				}
				handleFiles(filesArrGeneral);
			}
			else {
			}
		}
		
		let preview = dzoneobj.querySelector(".coverImageIn");
		function handleFiles(files) {
			if (!files.length) {
				preview.innerHTML = "<span>No files selected!<br /><label for='MTDropBoxInput'>Choose file(s)</label></span>";
			}
			else {
				preview.innerHTML = "";
				let list = document.createElement("ul");
				list.classList.add("MTDropBoxImgUl");
				preview.appendChild(list);
                for (let i = 0; i < files.length; i++) {
					let file = files[i];
					
					if (!imageType.test(file.type)) {
						continue;
					}
					
					let li = document.createElement("li");
					li.classList.add("MTDropBoxImgLi", ("index-" + i));
					let div = document.createElement("div");
					list.appendChild(li);
					let img = document.createElement("img");
					img.src = window.URL.createObjectURL(file);
					img.file = file;
					img.height = 60;
					img.classList.add("MTDropBoxImg");
					div.appendChild(img);
					// divControls
					let divControls = document.createElement("div");
					divControls.classList.add("coverImageInControls");
					// divControlsBtnGroup
					let divControlsBtnGroup = document.createElement("div");
					divControlsBtnGroup.classList.add("btn-group", "btn-group-sm", "nowrap");
					divControls.appendChild(divControlsBtnGroup);
					// divControlsBtn
					let divControlsDeleteBtn = document.createElement("button");
					divControlsBtnGroup.appendChild(divControlsDeleteBtn);
					divControlsDeleteBtn.classList.add("btn", "btn-flat", "btn-danger", "deleteImgBtn");
					let divControlsDeleteBtnIcon = document.createElement("i");
					divControlsDeleteBtnIcon.classList.add("fa", "fa-trash");
					divControlsDeleteBtn.appendChild(divControlsDeleteBtnIcon);
					div.appendChild(divControls);
					let info = document.createElement("span");
					info.innerHTML = files[i].name;
					div.appendChild(info);
					li.appendChild(div);
					let imgURL = "";
					let reader = new FileReader();
					reader.onload = (function (aImg) {
						return function (e) {
							imgURL = e.target.result;
						};
					})(img);
					reader.readAsDataURL(file);
					let form_data = new FormData();
					form_data.append('file', file);
					ImgService.postImg({path: uploadpath, auth: true, isJson: false}, form_data, function (data, ref) {
						if (data.response.status = 200) {
							mainself.onImgUpload(data.response.result);
						}
						else {
						}
					});
				}
			}
		}
		
		function findAncestor(el, cls) {
			while ((el = el.parentElement) && !el.classList.contains(cls));
			return el;
		}
		
		document.addEventListener('click', function (e) {
			let deleteBtn;
			if (e.target && e.target.classList.contains("deleteImgBtn")) {
				deleteBtn = e.target;
				deleteImg(deleteBtn);
			}
			else if (e.target && e.target.parentElement.classList.contains("deleteImgBtn")) {
				deleteBtn = e.target.parentElement;
				deleteImg(deleteBtn);
			}
		});
		
		function deleteImg(btn) {
			let btnParent = findAncestor(btn, "MTDropBoxImgLi");
			let btnParentClass = btnParent.classList[1].split("-");
			let btnParentIndex = btnParentClass[1];
			filesArrGeneral.splice(btnParentIndex, 1);
			handleFiles(filesArrGeneral);
			mainself.onImgDelete();
		}
		
		function init() {
		
		}
		
		init();
		
		self = null;
	}
}

export default ImgService;