import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import { browserHistory } from 'react-router';

class XhrService
{
    responseType = 'json';
    checkLoginPath = Config.getPath().refreshTokenCheck;

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    // get MovieTrip Data
    static getMtData(props, callObj, callback) {
        let self = new XhrService();
        let data = null;
        // xhr request
        const xhr = new XMLHttpRequest();
        let host = props.path;
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        // set the authorization HTTP header
        if (props.auth) {
            xhr.setRequestHeader('Authorization', `Bearer ${Auth.getAccessToken()}`);
        }
        xhr.responseType = self.responseType;
        xhr.addEventListener('load', () => {

            if (xhr.status === 401) {
                if (Auth.isUserAuthenticated()) {
                    XhrService.checkRefreshToken(props, data, callObj, callback, function(status, props, data, callObj, _callback) {
                        if(status) {
                            XhrService.getMtData(props, callObj, _callback);
                        }
                        else {
                            XhrService._handleLogin();
                        }
                    })
                }
                else {
                    XhrService._handleLogin();
                }
            }
            else {
                // callback the response
                callback(xhr, callObj);
            }
        });
        xhr.send();
    }

    // post MovieTrip Data
    static postMtData(props, data, callObj, callback) {
        let self = new XhrService();

        // xhr request
        const xhr = new XMLHttpRequest();
        let host = props.path;

        xhr.open('post', host);
        if (typeof props.isJson === "undefined" || props.isJson) {
        xhr.setRequestHeader('Content-type', 'application/json');
    }
        //set the authorization HTTP header
        if (props.auth) {
            //xhr.setRequestHeader('refresh_token', `${Auth.getRefreshToken()}`);
            xhr.setRequestHeader('Authorization', `Bearer ${Auth.getAccessToken()}`);
        }

        xhr.responseType = self.responseType;
        xhr.addEventListener('load', () => {

            if (xhr.status === 401) {
               
                if (Auth.isUserAuthenticated()) {
                    XhrService.checkRefreshToken(props, data, callObj, callback, function (status, props, data, callObj, _callback) {

                        if (status) {
                            XhrService.postMtData(props, data, callObj, _callback);
                        }
                        else {
                            XhrService._handleLogin();
                        }
                    })
                }
                else {
                    XhrService._handleLogin();
                }
            }
            else if(xhr.status === 403){
                console.warn("403 forbidden");
            }
            else {
                // callback the response
                callback(xhr, callObj);
            }
        });
        
	    if (typeof props.isJson === "undefined" || props.isJson)
		   xhr.send(JSON.stringify(data));
	    else
		    xhr.send(data);

    }

    static _handleLogin() {

        // redirect to login
        browserHistory.push('/login');
    }


    

    static checkRefreshToken(props, data, callObj, _callback, callback) {
        let self = new XhrService();
        // xhr request
        const xhr = new XMLHttpRequest();
        //let host = self.protocol + '//' + self.hostname + ':' +  self.port + self.checkLoginPath;
            //
           let host = props.path;
        xhr.open('get', host);
        xhr.setRequestHeader('Content-type', 'application/json');
    //    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.responseType = self.responseType;
        // set the authorization HTTP header
        xhr.setRequestHeader('refresh_token', `${Auth.getRefreshToken()}`);

        xhr.addEventListener('load', () => {
        
            try {
                if ( xhr.status === 200)
                    Auth.authenticateUser(xhr.response);

                else if (xhr.status === 401) {
                    Auth.deauthenticateUser();
                }

            } catch(e) {
                console.error(e);
            }
            callback((xhr.status === 200), props, data, callObj, _callback);
        });
        xhr.send();
    }
}


export default XhrService;
