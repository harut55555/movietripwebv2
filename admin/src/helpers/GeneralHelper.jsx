import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import { browserHistory } from 'react-router';

class GeneralHelper
{

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    /**
     * Pagination Max Pages
     */
    static getPaginationMaxPages(viewCount, count) {
        if (count !== null && viewCount !== null) {

            let max = Math.ceil((count / parseInt(viewCount)));

            return max;
        }
        return 1;
    }

    /**
     * Pagination Nearby Pages
     */
    static getPaginationNearByPages(currentPage, viewCount, count) {
        let list = [];
        let max = GeneralHelper.getPaginationMaxPages(viewCount, count);
        let pageNearBy = 4; // must be Ax2 pages, 2, 4 .. n%2 === 0

        if (currentPage === max) {
            let i = pageNearBy;
            while (i >= 1) {
                if (max - i >= 1)
                    list.push(max - i);
                i--;
            }
            list.push(currentPage);

        } else if (currentPage === 1) {
            list.push(currentPage);
            let i = 1;
            while (i <= pageNearBy) {
                if (currentPage + i <= max)
                    list.push(currentPage + i);
                i++;
            }
        } else  {
            let i = currentPage - (pageNearBy / 2);
            let maxNearBy = currentPage + (pageNearBy/2);

            while(i <= maxNearBy) {
                if (i < currentPage && (i) >= 1)
                    list.push(i);
                else if (i === currentPage)
                    list.push(currentPage);
                else if(i > currentPage && (i) <= max)
                    list.push(i);
                i++;
            }
        }

        return list;
    }

}

export default GeneralHelper;
