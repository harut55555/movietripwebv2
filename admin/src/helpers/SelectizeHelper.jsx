import React from 'react';
import Auth from '../modules/Auth';
import Config from '../Config';
import { browserHistory } from 'react-router';

class SelectizeHelper
{

    /**
     * Class constructor.
     */
    constructor(props) {

    }

    /**
     * Bind selectize
     */
    static bind(elem) {
        $(elem).unbind().selectize({
            plugins: ["remove_button", "drag_drop"],
            delimiter: ",",
            persist: false
        });
    }

    static bindStatus(ref, callback){
        let arr = [{text: "All", id: -1}, {text: "Draft", id: 0}, {text: "Publish", id: 1}];
        let selectizeStatus = $("#SelectStatus")[0].selectize;
        selectizeStatus.clearOptions();
        for (let i in arr) {
            let currentItem = arr[i];
            selectizeStatus.addOption({value: currentItem.id, text: currentItem.text});
        }
        selectizeStatus.setValue(parseInt(arr[0].id));

        selectizeStatus.on('change', function () {
            callback(ref,selectizeStatus.getValue());
        });
    }

    // static bindViewCount(ref, callback){
    //     let arr = [{text: "10", id: 10}, {text: "50", id: 50}, {text: "100", id: 100}];
    //     let selectizeView = $("#showEntriesCount")[0].selectize;
    //     selectizeView.clearOptions();
    //     for (let i in arr) {
    //         let currentItem = arr[i];
    //         selectizeView.addOption({value: currentItem.id, text: currentItem.text});
    //     }
    //     selectizeView.setValue(parseInt(arr[0].id));
    //
    //     selectizeView.on('change', function () {
    //         callback(ref,selectizeView.getValue());
    //     });
    // }

    static getValue(elem) {
        return $(elem)[0].selectize.getValue();
    }

    static setValue(elem, value) {
        return $(elem)[0].selectize.setValue(value);
    }

}

export default SelectizeHelper;
