import React from 'react';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import {Link} from 'react-router';

const UsersListComponent = ({list, onClickItemDelete, bootStrap}) => {

    return(
        <MTTable classnames={["bordered", "hover", "valignM"]}>
            <thead>
            <tr>
                <th className="minimal-width"></th>
                <th>Nickname</th>
                <th>Email</th>
                <th>Status</th>
                <th colSpan="2">Date</th>
            </tr>
            </thead>
            <tbody>
            {list.map(function (object, i) {
                let statusClassName = "";
                let statusText = "";
                if (object.status === 0) {
                    statusClassName = "warning";
                    statusText = "Pending";
                }
                if (object.status === 1) {
                    statusClassName = "success";
                    statusText = "Publish";
                }
                if (object.status === 2) {
                    statusClassName = "danger";
                    statusText = "Deleted";
                }
                if (object.status === 3) {
                    statusClassName = "danger";
                    statusText = "Denied";
                }
                return <tr key={object.id}>
                    <td className="minimal-width">
                        <MTCheckbox type="checkbox" namecheck="blogCheckbox" id="blogCheckbox1" name={object.id} />
                    </td>
                    <td><Link to={'/post/' + object.id}>{object.userName}</Link></td>
                    <td>{object.email}</td>
                    <td><span className={"label label-" + statusClassName}>{statusText}</span></td>
                    <td>{object.date}</td>
                    <td className="minimal-width">
                        <div className="btn-group btn-group-sm nowrap">
                            <Link to={"/" + bootStrap.apiPath + "/" + object.id} title="Edit"
                                  className="btn btn-flat btn-info"><i
                                className="fa fa-pencil"></i></Link>
                            <button type="button" title="Delete" className="btn btn-flat btn-info"
                                    id={object.id} onClick={onClickItemDelete}><i
                                id={object.id} className="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            })}
            </tbody>
        </MTTable>);
}


export default UsersListComponent;
