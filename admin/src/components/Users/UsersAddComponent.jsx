import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import Publish from '../Publish.jsx'
import ContentWrapTop from '../ContentWrapTop.jsx';
import {Link, IndexLink} from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const UsersAddComponent = ({bootStrapArray, user, password, confirmpassword,classcolorerror, handleChange, handleBlur, handleClick,  id}) => {
    let k;
    return (
        <div>
            <ContentWrapTop name={["Add User"]} bootStrapArray={bootStrapArray}/>
            <div className="contentWrap">
                <Row>
                    <Col sm={9}>
                        <MTBox>
                            <MTBoxHeader>
                                <MTBoxHeaderLeft>
                                    <h3>
                                        <span>Info</span>
                                    </h3>
                                </MTBoxHeaderLeft>
                            </MTBoxHeader>
                            <MTBoxBody>
                                <Row>
                                    <Col sm={4}>

                                        <FormGroup classname={classcolorerror.email[0]}>
                                            <label htmlFor="email">
                                                <i className={classcolorerror.email[1]}></i>&nbsp;
                                                Email</label>
                                            <input type="email" id="email" name="email" className="form-control"
                                                   value={user.email} onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={4}>
                                        <FormGroup classname={classcolorerror.userName[0]}>
                                            <label htmlFor="userName"> <i
                                                className={classcolorerror.userName[1]}></i>&nbsp;
                                                Nickname</label>
                                            <input type="text" id="userName" name="userName"
                                                   className="form-control" value={user.userName}
                                                   onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>

                                        
                                        <FormGroup className={classcolorerror.password[0]}>
                                            <label htmlFor="password" className="control-label">
                                                <i className={classcolorerror.password[1]}></i>&nbsp;
                                                Password
                                            </label>
                                            <input type="password" id="password" name="password"
                                                   className="form-control" value={password} onChange={handleChange}
                                                   onBlur={handleBlur}/>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={4}>
                                        <FormGroup className={classcolorerror.confirmpassword[0]}>
                                            <label htmlFor="confirmpassword" className="control-label">
                                                <i className={classcolorerror.confirmpassword[1]}></i>&nbsp;
                                                Confirm Password
                                            </label>
                                            <input type="password" id="confirmpassword" name="confirmpassword"
                                                   className="form-control" value={confirmpassword}
                                                   onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={4}>
                                        <FormGroup>
                                            <label htmlFor="userRole">Role</label>
                                            <select name="userRole" id="userRole" defaultValue="" className="selectize">
                                                <option value="" disabled hidden>Select group</option>

                                            </select>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </MTBoxBody>
                        </MTBox>
                    </Col>
                    <Col sm={3}>
                        <Publish handleClick={handleClick} label={user}/>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default UsersAddComponent;
