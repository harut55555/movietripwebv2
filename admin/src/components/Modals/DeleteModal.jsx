import React from 'react';

const DeleteModal = ({
	onClickYes,
	onClickNo
}) => (
	<div className="modal fade" id="deleteModal" tabIndex="-1" role="dialog">
		<div className="modal-dialog modal-lg">
			<div className="modal-content">
				<div className="modal-header">
					<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 className="modal-title">Delete Action</h4>
				</div>
				<div className="modal-body">
					<Row>
						<Col>
							<Typography>
								<h3>Are you sure you want to delete this item?</h3>
							</Typography>
						</Col>
					</Row>
				</div>
				<div className="modal-footer">
					<button type="button" className="btn btn-default btn-flat" id="deleteModalFalse" data-dismiss="modal">No</button>
					<button type="button" className="btn btn-Danger btn-flat" id="deleteModalTrue" data-dismiss="modal">Yes</button>
				</div>
			</div>
		</div>
	</div>
)

export default DeleteModal;
