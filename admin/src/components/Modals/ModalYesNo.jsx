import React from 'react';
import Row from '../Row/Row';
import Col from '../Col/Col';
import Typography from '../Typography/Typography';

const ModalYesNo = ({
	onClickYes,
	onClickNo
}) => (
	<div className="modal fade" id="ModalYesNo" tabIndex="-1" role="dialog">
		<div className="modal-dialog modal-lg">
			<div className="modal-content">
				<div className="modal-header">
					<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 className="modal-title">Delete Action</h4>
				</div>
				<div className="modal-body">
					<Row>
						<Col>
							<Typography>
								<h3>Are you sure you want to delete this item?</h3>
							</Typography>
						</Col>
					</Row>
				</div>
				<div className="modal-footer">
					<button onClick={onClickNo} type="button" className="btn btn-default btn-flat" id="deleteModalFalse" data-dismiss="modal">No</button>
					<button onClick={onClickYes} type="button" className="btn btn-Danger btn-flat" id="deleteModalTrue" data-dismiss="modal">Yes</button>
				</div>
			</div>
		</div>
	</div>
)

export default ModalYesNo;
