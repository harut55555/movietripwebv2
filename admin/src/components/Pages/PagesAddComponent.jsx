import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import Publish from '../Publish.jsx'
import ContentWrapTop from '../ContentWrapTop.jsx';

const PagesAddComponent = ({onSaveAndClose,
    onSave,
    onSaveDraft,
    publishDisabled,
	disabled,
	data,
    handleChange,
    handleBlur,
    bootStrap}) => {
	return (
	 <div>
		 {/*<ContentWrapTop bootStrap={bootStrap}/>*/}
		 <div className="contentWrap">
			 <Row>
				 <Col sm={9}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Info</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="pageTitle">Title</label>
										 <input type="text" id="pageTitle" name="title" className="form-control" value={data.title} onChange={handleChange} onBlur={handleBlur} />
										 {/*<span className="help-block">Processing Request</span>*/}
									 </FormGroup>
								 </Col>
							 </Row>
							 <Row>
								 <Col sm={4}>
									 <FormGroup>
										 <label htmlFor="pageAlias">Alias</label>
										 <input disabled={((disabled) ? "disabled" : '')} type="text" id="pageAlias" name="alias" className="form-control" value={data.alias} onChange={handleChange} onBlur={handleBlur} />
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Content</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : '')}>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="pageEditor">Editor</label>
										 <textarea name="content" id="pageEditor" cols="" rows="" ></textarea>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
				 <Col sm={3}>
					 <Publish onSaveAndClose={onSaveAndClose}
							  onSave={onSave}
							  onSaveDraft={onSaveDraft}
							  publishDisabled={publishDisabled}
					 />
				 </Col>
			 </Row>
		 </div>
	 </div>
	)
};

export default PagesAddComponent;
