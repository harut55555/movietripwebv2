import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTCheckbox = ({type, size, text, id, namecheck, color, disabledState, name}) => {
	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	let classType = "mt" + capitalizeFirstLetter(type);

	if (size === "sm") {
		classType = classType + " " + classType + "-sm";
	}
	else if (size === "lg") {
		classType = classType + " " + classType + "-lg";
	}

	if (typeof color !== "undefined") {
		let colorClass = "color-" + color + "-bg";
		classType = classType + " " + colorClass;
	}

	return (
	 <div className={classType}>
		 <label>
			 <input className="deleteMe" data-id={name} id={id} name={namecheck} type={type} {...disabledState === "true" ? {disabled: "disabled"} : {}} />
			 <span>{text}</span>
		 </label>
	 </div>
	)
};

export default MTCheckbox;