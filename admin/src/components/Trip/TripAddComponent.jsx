import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import SortableComponent from '../../containers/Trip/TripPointList.jsx';
import MapService from '../../services/MapService';
import { Link, IndexLink } from 'react-router';
import Publish from '../Publish.jsx'
import ImagesUploader from 'react-images-uploader';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const TripAddComponent = ({data,
						  disabled,
						  handleBlur,
						  handleChange,
						  onSaveAndClose,
						  onSave,
						  onSaveDraft,
						  publishDisabled,
						  onMapClick,
						  onMapLoad}) => {
	return (
	 <div>
		 {/*<ContentWrapTop name={["Add Trip"]} bootStrapArray={bootStrapArray}/>*/}
		 <div className="contentWrap">
			 <Row>
				 <Col sm={9}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Trip Design</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="tripTitle">Title</label>
										 <input type="text" id="tripTitle" name="title" className="form-control" value={data.title} onChange={handleChange}
												onBlur={handleBlur}/>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="tripDescription">Description</label>
										 <textarea className="form-control" name="description" id="tripDescription" cols="" value={data.description} onChange={handleChange}
												   onBlur={handleBlur} rows="4" disabled={((disabled) ? "disabled" : "")}></textarea>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft >
								 <h3>
									 <span>Cover Image</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : "")}>
							 <Row>
								 <Col>
									 <div className="coverImage dragleave" id="MTDropBox">
										 <input type="file" hidden className="hidden" accept="image/*" id="MTDropBoxInput" />
										 <div className="coverImageIn">
											 <span>No files selected!<br/><label htmlFor="MTDropBoxInput">Choose file(s)</label></span>
										 </div>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Points</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : "")}>
							 <ul className="nav nav-tabs">
								 <li className="active"><a href="#addTripSceneBlock" data-toggle="tab" aria-expanded="false">Scene</a></li>
								 <li><a href="#addTripLocationBlock" data-toggle="tab" aria-expanded="false">Location</a></li>
							 </ul>
							 <Row>
								 <Col>
									 <div className="tab-content">
										 <div className="tab-pane fade active in" id="addTripSceneBlock">
											 <Row>
												 <Col>
													 <FormGroup>
														 <label htmlFor="tripSceneMovie">Movie</label>
														 <select name="tripSceneMovie" id="tripSceneMovie" defaultValue="" className="selectize">
															 <option value="" disabled hidden>Select Movie</option>
														 </select>
													 </FormGroup>
													 <FormGroup>
														 <label htmlFor="tripSceneSelect">Scene</label>
														 <select name="tripSceneSelect" id="tripSceneSelect" defaultValue="" className="selectize">
															 <option value="" disabled hidden>Select Scene</option>
														 </select>
													 </FormGroup>
												 </Col>
											 </Row>
										 </div>
										 <div className="tab-pane fade" id="addTripLocationBlock">
											 <Row>
												 <Col>
													 <FormGroup>
														 <label htmlFor="tripMainLocation">Location</label>
														 <select name="tripMainLocation" id="tripMainLocation" defaultValue="" className="selectize">
															 <option value="" disabled hidden>Select Location</option>
														 </select>
													 </FormGroup>
												 </Col>
											 </Row>
										 </div>
									 </div>
									 <FormGroup>
										 <label htmlFor="tripPointDescription">Description</label>
										 <textarea className="form-control" name="tripPointDescription" id="tripPointDescription" cols="" rows="4"></textarea>
									 </FormGroup>
									 <FormGroup>
										 <button disabled={((disabled) ? "disabled" : "")} type="button" title="Add Point" className="btn btn-flat btn-info" id="tripPointAddBtn">{"Add Point"}</button>
									 </FormGroup>
									 {/*<SortableComponent items={items} arrayChanged={arrayChanged} pointEditClick={pointEditClick}/>*/}
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Map</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : "")}>
							 <Row>
								 <Col>
									 <div id="map_canvas" style={{height: "500px"}}>
										 <MapService onMapClick={onMapClick} onMapLoad={onMapLoad}/>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
				 <Col sm={3}>
					 <Publish onSaveAndClose={onSaveAndClose}
							  onSave={onSave}
							  onSaveDraft={onSaveDraft}
							  publishDisabled={publishDisabled}
					 />
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Editor's choice</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : '')}>
							 <Row>
								 <Col>
									 <label htmlFor="sceneEditorsChoice" className="hidden" hidden></label>
									 <select name="sceneEditorsChoice" id="sceneEditorsChoice" className="selectize selectize-sm">
										 <option value="1">Yes</option>
										 <option value="2">No</option>
									 </select>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Check-In Text</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={((disabled) ? "disabled" : '')}>
							 <Row>
								 <Col>
									 <label htmlFor="sceneCheckinText" className="hidden" hidden></label>
									 <input type="text" name="sceneCheckinText" id="sceneCheckinText" className="form-control input-sm"/>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
			 </Row>
		 </div>
		 <div className="modal fade" id="editPointModal" tabIndex="-1" role="dialog">
			 <div className="modal-dialog modal-lg">
				 <div className="modal-content">
					 <div className="modal-header">
						 <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 <h4 className="modal-title">Edit Point</h4>
					 </div>
					 <div className="modal-body">
						 <Row>
							 <Col>
								 <ul className="nav nav-tabs nav-tabs-custom">
									 <li className="active"><a href="#editTripSceneBlock" data-toggle="tab" aria-expanded="false">Scene</a></li>
									 <li><a href="#editTripLocationBlock" data-toggle="tab" aria-expanded="false">Location</a></li>
								 </ul>
								 <div className="tab-content">
									 <div className="tab-pane fade active in" id="editTripSceneBlock">
										 <FormGroup>
											 <label htmlFor="editPointMovie">Movie</label>
											 <select name="editPointMovie" id="editPointMovie" defaultValue="" className="selectize">
												 <option value="" disabled hidden>Select Movie</option>
											 </select>
										 </FormGroup>
										 <FormGroup>
											 <label htmlFor="editPointSceneSelect">Scene</label>
											 <select name="editPointSceneSelect" id="editPointSceneSelect" defaultValue="" className="selectize">
												 <option value="" disabled hidden>Select Scene</option>
											 </select>
										 </FormGroup>
									 </div>
									 <div className="tab-pane fade" id="editTripLocationBlock">
										 <FormGroup>
											 <label htmlFor="editPointLocation">Location</label>
											 <select name="editPointLocation" id="editPointLocation" defaultValue="" className="selectize">
												 <option value="" disabled hidden>Select Location</option>
											 </select>
										 </FormGroup>
									 </div>
								 </div>
								 <FormGroup>
									 <label htmlFor="editPointDescription">Description</label>
									 <textarea className="form-control" name="editPointDescription" id="editPointDescription" cols="" rows="4"></textarea>
								 </FormGroup>
							 </Col>
						 </Row>
					 </div>
					 <div className="modal-footer">
						 <button type="button" className="btn btn-default btn-flat" data-dismiss="modal">Close</button>
						 <button type="button" className="btn btn-success btn-flat" id="tripPointEditBtn" disabled="disabled">Save changes</button>
					 </div>
				 </div>
			 </div>
		 </div>
	 </div>
	);
};

export default TripAddComponent;