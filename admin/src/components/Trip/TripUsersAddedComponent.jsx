import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const TripUsersAdded = ({home, bootStrapArray}) => (
 <div>
	 <ContentWrapTop name={["Users' added"]} bootStrapArray={bootStrapArray} />
	 <div className="contentWrap">
		 <Row>
			 <Col md={3}>
				 <MTWidget colorClass="invert-success" icon="usd" title="Income report" count="1000 $" progress="true" progressPercent={70} progressText="70% increase in last 3 weeks" />
			 </Col>
			 <Col>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <i className="fa fa-cube"/>
								 <span>Box Example</span>
							 </h3>
						 </MTBoxHeaderLeft>
						 <MTBoxHeaderRight>

						 </MTBoxHeaderRight>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <MTTable classnames={["hover"]}>
							 <thead>
							 <tr>
								 <th className="minimal-width">#</th>
								 <th>#</th>
								 <th>#</th>
								 <th>#</th>
							 </tr>
							 </thead>
							 <tbody>
							 <tr>
								 <td className="minimal-width">1</td>
								 <td>
									 <MTCheckbox type="checkbox" text="Check Me" id="check1" name="check1" disabledState="true" />
								 </td>
								 <td>#</td>
								 <td>#</td>
							 </tr>
							 </tbody>
						 </MTTable>
					 </MTBoxBody>
					 <MTBoxFooter>
						 <MTBoxFooterLeft>
							 The footer of the box
						 </MTBoxFooterLeft>
					 </MTBoxFooter>
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default TripUsersAdded;
