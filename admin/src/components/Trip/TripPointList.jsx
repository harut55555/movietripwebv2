import React, {Component} from 'react';
import {render} from 'react-dom';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

const SortableItem = SortableElement(({value}) =>
	<li>
		<div className="pointsWrapInImg">
			<div className="pointsWrapInImgInside" style={{backgroundImage: 'url(/img/users/user-1.jpg)'}}></div>
		</div>
		<div className="pointsWrapInText">
			<h2>Movie: Paris, je t'aime</h2>
			<h3>Location: Le Square Trousseau</h3>
		</div>
		<div className="pointsWrapInTextToolbox">
			<div className="btn-group btn-group-vertical btn-group-sm nowrap">
				<button type="button" title="Edit" className="btn btn-flat btn-info"><i className="fa fa-pencil"></i></button>
				<button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
			</div>
		</div>
	</li>
);

const SortableList = SortableContainer(({items}) => {
	return (
	 <ul>
		 {items.map((value, index) => (
			<SortableItem key={`item-${index}`} index={index} value={value} />
		 ))}
	 </ul>
	);
});

class SortableComponent extends Component {
	state = {
		items: ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6']
	};
	onSortEnd = ({oldIndex, newIndex}) => {
		this.setState({
			items: arrayMove(this.state.items, oldIndex, newIndex)
		});
	};
	render() {
		return <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />;
	}
}

render(<SortableComponent/>, document.getElementById('bootStrapArray'));