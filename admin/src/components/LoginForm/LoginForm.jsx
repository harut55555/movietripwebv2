import React from 'react';
import { Link } from 'react-router';

const LoginForm = ({
	onSubmit,handlePasswordChange, handleEmailChange, errors, successMessage
}) => (
	<div>
		<section className="loginSection">
			<form action="/" className="clearfix" onSubmit={onSubmit}>
				<section className="boxWrap">
					<div className="box">
						<div className="boxHeader">
							<div className="boxHeaderLeft">
								<h3>
									<i className="fa fa-user"></i>
									<span>Login</span>
								</h3>
							</div>
						</div>
						<div className="boxBody">
							<div className="row">
								<div className="col-xs-12">
									<div className="form-group">
										<label htmlFor="email">Username:</label>
										<input name="email" onKeyUp={handleEmailChange} onChange={handleEmailChange} className="form-control" type="text" id="email" />
									</div>
									<div className="form-group">
										<label htmlFor="password">Password:</label>
										<input name="password" onKeyUp={handlePasswordChange} onChange={handlePasswordChange} className="form-control" type="password" id="password" />
									</div>
									{
                                        (typeof errors.summary !== 'undefined' && typeof errors.summary.Email !== 'undefined') ?
											<div className="form-group">
												<div className="alert alert-danger">
													<i className="fa fa-warning"></i> Please enter a valid email address
												</div>
											</div>
											: null
									}
								</div>
							</div>
							<div className="col-xs-12">
							</div>
						</div>
						<div className="boxFooter">
							<div className="boxFooterLeft">
								{/*<div className="mtCheckbox">*/}
								{/*<label>*/}
								{/*<input type="checkbox" />*/}
								{/*<span>Remember Me</span>*/}
								{/*</label>*/}
								{/*</div>*/}
							</div>
							<div className="boxFooterRight">
								<button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white loginButton">Login</button>
							</div>
						</div>
					</div>
				</section>
			</form>
		</section>
	</div>
);

export default LoginForm;
