import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import ListFilter from '../ListFilter.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const CountriesListComponent = ({baseItems, handleChange, onclickItemDelete, onChangeSearch, onClickSearch }) => (
 <div>
	
	 <ContentWrapTop name={["Countries"]} bootStrapArray={baseItems.bootStrapArray} />
	 <div className="contentWrap">
		 <Row>
			 <Col>
				 <MTBox>
					 <ListFilter selectbottom={baseItems.selectbottom} onClickSearch={onClickSearch} onChangeSearch={onChangeSearch} currentlinkWord={baseItems.currentlinkWord}/>
					 <MTBoxBody>
						 <MTTable classnames={["bordered", "hover", "valignM"]}>
							 <thead>
							 <tr>
								 <th className="minimal-width"></th>
								 <th>Name</th>
								 <th>Post code</th>
							 </tr>
							 </thead>
							 <tbody>
							 <tr>
								 <td className="minimal-width">
									 <MTCheckbox type="checkbox" name="countryCheckbox" id="countryCheckbox1"/>
								 </td>
								 <td><a href="#">Afghanistan</a></td>
								 <td>-</td>
							 </tr>
							 </tbody>
						 </MTTable>
					 </MTBoxBody>
					
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default CountriesListComponent;
