import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const GeoAddComponent = ({bootStrapArray, geo, disabledState, handleChange, handleBlur,onSelect}) => {

	let disabled = "";
	let di = "";
	let c;
	if (disabledState === 1) {
		disabled = "disabled";
		di = disabled;
	}

	return (<div>
			<ContentWrapTop name={["Add Country/City"]} bootStrapArray={bootStrapArray}/>
			<div className="contentWrap">
				<Row>
					<Col sm={9}>
						<MTBox>
							<MTBoxHeader>
								<MTBoxHeaderLeft>
									<h3>
										<span>Info</span>
									</h3>
								</MTBoxHeaderLeft>
							</MTBoxHeader>
							<MTBoxBody>
								<Row>
									<Col>
										<FormGroup>
											<label htmlFor="cityCountry">Country</label>
											<select name="cityCountry" id="cityCountry" className="selectize"
													defaultValue="">
												<option value="" disabled hidden>Select Country</option>
											</select>
										</FormGroup>

										<FormGroup>
											<label htmlFor="cityName">Name</label>
											<input disabled={disabled} type="text" id="cityName" name="cityName"
												   className="form-control" onChange={handleChange}
												   onBlur={handleBlur}/>
										</FormGroup>

										<FormGroup>
											<label htmlFor="citypostCode">Postcode</label>
											<input disabled={disabled} type="text" id="citypostCode" name="citypostCode"
												   className="form-control" onChange={handleChange}
												   onBlur={handleBlur}/>
										</FormGroup>
									</Col>
								</Row>
							</MTBoxBody>
						</MTBox>
					</Col>
					<Col sm={3}>
						<MTBox>
							<MTBoxHeader>
								<MTBoxHeaderLeft>
									<h3>
										<span>Publish</span>
									</h3>
								</MTBoxHeaderLeft>
							</MTBoxHeader>
							<MTBoxBody>
								<Row>
									<Col md={6}>
										<FormGroup>
											<button type="button" title="Save & Close"
													className="btn btn-sm btn-block btn-flat btn-success">{"Save & Close"}</button>
										</FormGroup>
									</Col>
									<Col md={6}>
										<FormGroup>
											<button type="button" title="Save"
													className="btn btn-sm btn-block btn-flat btn-success">{"Save"}</button>
										</FormGroup>
									</Col>
								</Row>
								<Row>
									<Col md={6}>
										<FormGroup>
											<button type="button" title="Save Draft"
													className="btn btn-sm btn-block btn-flat btn-warning">{"Save Draft"}</button>
										</FormGroup>
									</Col>
									<Col md={6}>
										<button type="button" title="Discard"
												className="btn btn-sm btn-block btn-flat btn-danger">{"Discard"}</button>
									</Col>
								</Row>
							</MTBoxBody>
						</MTBox>
					</Col>
				</Row>
			</div>
		</div>
	)
};

export default GeoAddComponent;
