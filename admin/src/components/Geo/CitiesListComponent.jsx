import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const CitiesListComponent = ({home, root}) => (
 <div>
	 <ContentWrapTop name={["Cities"]} root={root} />
	 <div className="contentWrap">
		 <Row>
			 <Col>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <div>
								 <div className="btn-group btn-group-sm nowrap">
									 <Link to="/geo/add" title="Add" className="btn btn-flat btn-info"><i className="fa fa-plus"></i></Link>
									 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
								 </div>
							 </div>
						 </MTBoxHeaderLeft>
						 <MTBoxHeaderRight>
							 <div className="boxSearch">
								 <div className="input-group input-group-sm">
									 <label for="citySearch" className="hidden" hidden></label>
									 <input className="form-control pull-right" id="citySearch" name="citySearch" type="text" placeholder="Search"/>
									 <div className="input-group-btn">
										 <button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white">
											 <i className="fa fa-search"></i>
										 </button>
									 </div>
								 </div>
							 </div>
						 </MTBoxHeaderRight>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <MTTable classnames={["bordered", "hover", "valignM"]}>
							 <thead>
							 <tr>
								 <th className="minimal-width">
									 <MTCheckbox type="checkbox" name="cityCheckbox" id="cityCheckboxAll"/>
								 </th>
								 <th>Name</th>
								 <th>Country</th>
								 <th colSpan="2">Post code</th>
							 </tr>
							 </thead>
							 <tbody>
							 <tr>
								 <td className="minimal-width">
									 <MTCheckbox type="checkbox" name="cityCheckbox" id="cityCheckbox1"/>
								 </td>
								 <td><a href="#">Kabul</a></td>
								 <td><a href="#">Afghanistan</a></td>
								 <td>-</td>
								 <td className="minimal-width">
									 <div className="btn-group btn-group-sm nowrap">
										 <button type="button" title="Edit" className="btn btn-flat btn-info"><i className="fa fa-pencil"></i></button>
										 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
									 </div>
								 </td>
							 </tr>
							 </tbody>
						 </MTTable>
					 </MTBoxBody>
					 <MTBoxFooter type="paginationFooter" />
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default CitiesListComponent;
