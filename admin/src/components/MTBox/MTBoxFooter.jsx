import React from 'react';
import {Link, IndexLink} from 'react-router';
import MTBoxFooterLeft from './MTBoxFooterLeft.jsx';
import MTBoxFooterRight from './MTBoxFooterRight.jsx';
import GeneralHelper from '../../helpers/GeneralHelper';


const MTBoxFooter = ({children, type, controller, count, params, viewCount, onChangeViewCount}) => {

    if (type === "paginationFooter") {
        return (
            <MTBoxFooter>
                <MTBoxFooterLeft>
                    <div className="boxFieldCount">
                        <div className="input-group input-group-sm">
                             <span className="input-group-addon">
                                 <span>Show</span>
                             </span>
                            <label htmlFor="showEntriesCount" className="hidden" hidden></label>
                            <select id="showEntriesCount" onChange={onChangeViewCount} className="form-control" defaultValue="10">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="150">150</option>
                            </select>
                             <span className="input-group-addon">
                                 <span>Entries</span>
                             </span>
                        </div>
                    </div>
                </MTBoxFooterLeft>
                <MTBoxFooterRight>
                    <div className="boxPaganation clearfix">
                        <ul className="pagination pagination-sm pull-right">
                            <li><Link to={'/'+ controller +'/index'} className="disabled"><i
                                className="fa fa-angle-double-left"></i></Link></li>
                            {parseInt((params.page)) === 1 ?
                                <li><Link to={ '/'+controller+"/index"} className="disabled"><i
                                    className="fa fa-angle-left"></i></Link></li> :
                                <li><Link to={ '/'+controller+"/index/" +(parseInt(params.page) - 1)} className=""><i
                                    className="fa fa-angle-left"></i></Link></li>
                            }
                            {GeneralHelper.getPaginationNearByPages(parseInt(params.page), parseInt(viewCount), parseInt(count)).map(function (object, i) {
                                return <li key={i}><Link to={ '/'+controller+'/index/'+ object}
                                                         className={ (parseInt(params.page)===object) ? "active" : ''}>{object}</Link>
                                </li>
                            })}
                            {parseInt(params.page) < GeneralHelper.getPaginationMaxPages(viewCount, count) ?
                                <li><Link to={ '/'+controller+"/index/" + (parseInt(params.page) + 1)} className=""><i
                                    className="fa fa-angle-right"></i></Link></li> :
                                <li><Link to={ '/'+controller+"/index/" +parseInt(params.page)} className="disabled"><i
                                    className="fa fa-angle-right"></i></Link></li>
                            }
                            <li><Link to={'/'+controller+'/index/' + GeneralHelper.getPaginationMaxPages(viewCount, count)} className="disabled"><i
                                className="fa fa-angle-double-right"></i></Link></li>

                        </ul>
                    </div>
                </MTBoxFooterRight>
            </MTBoxFooter>
        )
    }
    return (
        <div className="boxFooter">
            {children}
        </div>
    )
};

//MTBoxHeader.propTypes = {
//	MTWidget: PropTypes.array.isRequired,
//	icon: PropTypes.string.isRequired,
//	title: PropTypes.string.isRequired,
//	count: PropTypes.string.isRequired
//};

export default MTBoxFooter;