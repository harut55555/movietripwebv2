import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBoxFooterLeft = ({children}) => {
	return (
	 <div className="boxFooterLeft">
		 {children}
	 </div>
	)
};
//MTBoxHeader.propTypes = {
//	MTWidget: PropTypes.array.isRequired,
//	icon: PropTypes.string.isRequired,
//	title: PropTypes.string.isRequired,
//	count: PropTypes.string.isRequired
//};

export default MTBoxFooterLeft;