import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBoxHeader = ({children}) => {
	return (
	 <div className="boxHeader">
		 {children}
	 </div>
	)
};


export default MTBoxHeader;