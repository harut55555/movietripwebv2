import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBox = ({children, classname}) => {
	let mtBoxClassName = "boxWrap";
	if (typeof classname !== "undefined") {
		mtBoxClassName = mtBoxClassName + " " + classname;
	}
	return (
		<section className={mtBoxClassName}>
			<div className="box">
				{children}
			</div>
		</section>
	)
};

//MTBox.propTypes = {
//	MTWidget: PropTypes.array.isRequired,
//	icon: PropTypes.string.isRequired,
//	title: PropTypes.string.isRequired,
//	count: PropTypes.string.isRequired
//};

export default MTBox;