import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBoxHeaderLeft = ({children}) => {
	return (
	 <div className="boxHeaderLeft">
		 {children}
	 </div>
	)
};

export default MTBoxHeaderLeft;