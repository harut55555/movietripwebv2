import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBoxBody = ({children, classname}) => {
	let mtBoxBodyClassName = "boxBody";
	if (typeof classname !== "undefined") {
		mtBoxBodyClassName = mtBoxBodyClassName + " " + classname;
	}
	return (
	 <div className={mtBoxBodyClassName}>
		 {children}
	 </div>
	)
};

export default MTBoxBody;