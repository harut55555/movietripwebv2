import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTBoxHeaderRight = ({children}) => {
	return (
	 <div className="boxHeaderRight">
		 {children}
	 </div>
	)
};

export default MTBoxHeaderRight;