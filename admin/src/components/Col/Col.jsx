import React from 'react';
import { Link, IndexLink } from 'react-router';

const Col = ({children, xs, sm, md, lg}) => {
	let sizeClassPrefix = "col-";
	let sizeClass = "";
	if (typeof xs === "undefined") {
		sizeClass = sizeClassPrefix + "xs-12" + " ";
	}
	if (typeof xs !== "undefined") {
		sizeClass = sizeClassPrefix + "xs-" + xs + " ";
	}
	if (typeof sm !== "undefined") {
		sizeClass = sizeClass + sizeClassPrefix + "sm-" + sm + " ";
	}
	if (typeof md !== "undefined") {
		sizeClass = sizeClass + sizeClassPrefix + "md-" + md + " ";
	}
	if (typeof lg !== "undefined") {
		sizeClass = sizeClass + sizeClassPrefix + "lg-" + lg + " ";
	}
	if (typeof sm === "undefined" && typeof md === "undefined" && typeof lg === "undefined") {
		sizeClass = sizeClassPrefix + "xs-12";
	}
	return (
	 <div className={sizeClass}>
		 {children}
	 </div>
	)
};

export default Col;