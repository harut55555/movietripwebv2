import React from 'react';
import {Link, IndexLink} from 'react-router';

const FormGroup = ({children, classname}) => {
    let formGroupClassName = "form-group";
    if (typeof classname !== "undefined") {
        formGroupClassName = formGroupClassName + " " + classname;
    }
    return (
        <div className={formGroupClassName}>
            {children}
        </div>
    )
};

export default FormGroup;