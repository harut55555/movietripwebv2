import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTTable = ({classnames, children}) => {
	let classnamePrefix = "table-";
	let classes = ["bordered", "condensed", "striped", "hover"];
	let colorClass = "";
	let classExport = "";
	classnames.map(function(classnamesobject, i) {
		let found = false;
		classes.map(function(classesobject, y) {
			if (classnames[i] === classes[y]) {
				found = true;
			}
		});
		if (found) {
			colorClass = classnamePrefix + classnames[i];
			classExport = classExport + colorClass + " ";
		}
		else {
			colorClass = classnames[i];
			classExport = classExport + colorClass + " ";
		}
	});
	return (
	 <div className="tableWrap">
		 <table className={"table " + classExport}>
			 {children}
		 </table>
	 </div>
	)
};

export default MTTable;