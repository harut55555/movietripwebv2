import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const LocationsUsersAdded = ({home, root}) => (
 <div>
	 <ContentWrapTop name={["Users' added"]} root={root} />
	 <div className="contentWrap">
		 <Row>
			 <Col>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <div>
								 <div className="btn-group btn-group-sm nowrap">
									 <Link to="/locations/add" title="Add" className="btn btn-flat btn-info"><i className="fa fa-plus"></i></Link>
									 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
								 </div>
							 </div>
						 </MTBoxHeaderLeft>
						 <MTBoxHeaderRight>
							 <div className="boxStatus boxHeaderRightSelectWrap-md">
								 <label for="locationStatus" className="hidden" hidden></label>
								 <select name="locationStatus" id="locationStatus" className="selectize selectize-sm" defaultValue="">
									 <option value="" disabled hidden>Select Status</option>
									 <option value="-1">All</option>
									 <option value="0">Pending</option>
									 <option value="1">Approved</option>
									 <option value="2">Denied</option>
								 </select>
							 </div>
							 <div className="boxSearch">
								 <div className="input-group input-group-sm">
									 <label for="locationSearch" className="hidden" hidden></label>
									 <input className="form-control pull-right" id="locationSearch" name="locationSearch" type="text" placeholder="Search"/>
									 <div className="input-group-btn">
										 <button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white">
											 <i className="fa fa-search"></i>
										 </button>
									 </div>
								 </div>
							 </div>
						 </MTBoxHeaderRight>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <MTTable classnames={["bordered", "hover", "valignM"]}>
							 <thead>
							 <tr>
								 <th className="minimal-width">
									 <MTCheckbox type="checkbox" name="blogCheckbox" id="blogCheckboxAll"/>
								 </th>
								 <th>Name</th>
								 <th>City</th>
								 <th>Country</th>
								 <th>Address</th>
								 <th>Status</th>
								 <th colSpan="2">Date</th>
							 </tr>
							 </thead>
							 <tbody>
							 <tr>
								 <td className="minimal-width">
									 <MTCheckbox type="checkbox" name="blogCheckbox" id="blogCheckbox1"/>
								 </td>
								 <td><a href="#">Hard Rock Cafe</a></td>
								 <td>Yerevan</td>
								 <td>Armenia</td>
								 <td>Hard Rock Cafe, Broadway, New York, NY, United States</td>
								 <td><span className="label label-success">Approved</span></td>
								 <td>2017-05-08 16:02:00</td>
								 <td className="minimal-width">
									 <div className="btn-group btn-group-sm nowrap">
										 <button type="button" title="Edit" className="btn btn-flat btn-info"><i className="fa fa-pencil"></i></button>
										 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
									 </div>
								 </td>
							 </tr>
							 </tbody>
						 </MTTable>
					 </MTBoxBody>
					 <MTBoxFooter>
						 <MTBoxFooterLeft>
							 <div className="boxFieldCount">
								 <div className="input-group input-group-sm">
									 <span className="input-group-addon">
										 <span>Show</span>
									 </span>
									 <label htmlFor="showEntriesCount" className="hidden" hidden></label>
									 <select id="showEntriesCount" className="form-control">
										 <option value="1">1</option>
										 <option value="5">5</option>
										 <option value="10">10</option>
										 <option value="20">20</option>
										 <option value="50">50</option>
										 <option value="100">100</option>
									 </select>
									 <span className="input-group-addon">
										 <span>Entries</span>
									 </span>
								 </div>
							 </div>
						 </MTBoxFooterLeft>
						 <MTBoxFooterRight>
							 <div className="boxPaganation headerPag clearfix">
								 <ul className="pagination pagination-sm pull-right">
									 <li><a href="#"><i className="fa fa-angle-double-left"></i></a></li>
									 <li><a href="#">1</a></li>
									 <li><a href="#">2</a></li>
									 <li><a href="#">3</a></li>
									 <li><a href="#">5</a></li>
									 <li><a href="#"><i className="fa fa-angle-double-right"></i></a></li>
								 </ul>
							 </div>
						 </MTBoxFooterRight>
					 </MTBoxFooter>
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default LocationsUsersAdded;
