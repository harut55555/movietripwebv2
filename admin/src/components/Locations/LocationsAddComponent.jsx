import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import Publish from '../Publish.jsx'
import ContentWrapTop from '../ContentWrapTop.jsx';
import MapService from '../../services/MapService';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const LocationsAddComponent = ({bootStrapArray,location, disabledState, onMapClick, onMapLoad, handleChange, handleBlur,onclickSave }) => {
	let disabled = "";
	let di="";
	let c;
	if (disabledState === 1) {
		disabled = "disabled";
		di=disabled;
	}
	(location.country!==null  ) &&  (location.city!==null ) && disabled !== "disabled"? di="" : di="disabled";
	return (
	 <div>
		 <ContentWrapTop name={["Add Location"]} bootStrapArray={bootStrapArray}/>

		 <div className="contentWrap">
			 <Row>
				 <Col sm={9}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Info</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="locationName">Name</label>
										 <input type="text" id="locationName" className="form-control" name="locationName" value={location.name} onChange={handleChange} onBlur={handleBlur} />
										 <span className="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
									 </FormGroup>
								 </Col>
							 </Row>
							 {disabled === "disabled" ? <Row>
								 <Col>
									 <FormGroup>
										 <h2>Necessarily adding name to fill another contents</h2>
									 </FormGroup>
								 </Col>
							 </Row> : null}
							 <Row>
								 <Col>
									 <FormGroup >
										 <label htmlFor="locationTags">Tags</label>
										 <input disabled={disabled} type="text" id="locationTags" name="locationTags" className=""/>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox >
						 <MTBoxHeader>
							 <MTBoxHeaderLeft >
								 <h3>
									 <span>Cover Image</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col>
									 <div className="coverImage dragleave" id="MTDropBox">
										 <input type="file" hidden className="hidden" accept="image/*" id="MTDropBoxInput" />
										 <div className="coverImageIn">
											 <span>No files selected!<br/><label htmlFor="MTDropBoxInput">Choose file(s)</label></span>
										 </div>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Location</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col sm={4}>
									 <FormGroup >
										 <label htmlFor="locationCountry">Country</label>
										 <select disabled={disabled} name="locationCountry" id="locationCountry" defaultValue="" className="selectize">
											 <option value="" disabled hidden>Select Country</option>
										 </select>
									 </FormGroup>
								 </Col>
								 <Col sm={4}>
									 <FormGroup >
										 <label htmlFor="locationCity">City</label>
										 <select disabled={disabled} name="locationCity" id="locationCity" defaultValue="" className="selectize">
											 <option value="" disabled hidden>Select City</option>
										 </select>
									 </FormGroup>
								 </Col>
								 <Col sm={4}>
									 <FormGroup >
										 <label htmlFor="locationType">Type</label>
										 <select disabled={disabled} name="locationType" id="locationType" defaultValue="" className="selectize">
											 <option value="" disabled hidden>Select Type</option>
										 </select>
									 </FormGroup>
								 </Col>
								 <Col>
									 <FormGroup >
										 
										
										
										 <label htmlFor="locationAddress">Address</label>
										 <input type="text" disabled={di} id="locationAddress" className="form-control"
												name="locationAddress" value={ location.address!==null? location.address.name: "" }
												onChange={handleChange} onBlur={handleBlur}/>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Map</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>

						 {(location.country!==null  ) &&  (location.city!==null ) && (location.address!==null) && disabled !== "disabled" ? di="" : di="disabled noclick" }
						 <MTBoxBody classname={di}>
							 <Row>
								 <Col>
									
									 <div id="map_canvas" style={{height: "500px"}}>
										 <MapService onMapClick={onMapClick} onMapLoad={onMapLoad}/>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
				 <Col sm={3}>
					 <Publish handleClick ={onclickSave} label={location} bootStrapArray = "/locations/index/1"/>
				 </Col>
			 </Row>
		 </div>
	 </div>
	)
};

export default LocationsAddComponent;
