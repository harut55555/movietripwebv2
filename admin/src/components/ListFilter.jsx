import React from 'react';
import { Link, IndexLink } from 'react-router';
import MTBoxHeader from './MTBox/MTBoxHeader.jsx';
import MTBoxHeaderRight from './MTBox/MTBoxHeaderRight.jsx';
import MTBoxHeaderLeft from './MTBox/MTBoxHeaderLeft.jsx';
import BootStrap from '../services/BootStrap';

const ListFilter = ({state, onClickSearch, onClickDelete}) => (
    <div>
        
        <MTBoxHeader>
            <MTBoxHeaderLeft>
                <div>
                    <div className="btn-group btn-group-sm nowrap">
                        <Link to={BootStrap.getLink('/add', state.bootStrap.breadCrumbs)} title="Add" className="btn btn-flat btn-info"><i className="fa fa-plus"></i></Link>
                        <button type="button" title="Add" onClick={onClickDelete} className="deleteButton btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
                    </div>
                </div>
            </MTBoxHeaderLeft>
                <MTBoxHeaderRight>
                    <div className="boxStatus boxHeaderRightSelectWrap-md">
                        <label htmlFor="SelectStatus" className="hidden" hidden></label>
                        <select name="SelectStatus" id="SelectStatus" className="selectize selectize-sm" defaultValue="">
                            <option value="" disabled hidden>All</option>
                        </select>
                    </div>
                    <div className="boxSearch">
                        <div className="input-group input-group-sm">
                            <label htmlFor="Search" className="hidden" hidden></label>
                            <input className="form-control pull-right" id="SearchFilter" name="Search" type="text" placeholder="Search"/>
                            <div className="input-group-btn">
                                <button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white" onClick={onClickSearch}>
                                    <i className="fa fa-search" onClick={onClickSearch}></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </MTBoxHeaderRight>
        </MTBoxHeader>
    </div>
);

export default ListFilter;