import React from 'react';
import {Link, IndexLink} from 'react-router';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm/LoginForm.jsx';

const Base = ({
    children, statistics, handleSignOut, isAuthenticated,
     user
}) => (
    <div>
        <div>
            <header className="color-1-bg">
                <section className="headerLeft">
                    <div className="mtadminLogo-lg">
                        <a href="#"></a>
                    </div>
                    <div className="mtadminLogo-sm">
                        <a href="#"></a>
                    </div>
                </section>
                <section className="headerRight">
                    <div className="headerRight-Left">
                        <div>
                            <a href="#" className="sideMenuToggle color-1-hover-bg" title="Toggle Sidebar">
                                <i className="fa fa-bars"></i>
                            </a>
                        </div>
                    </div>
                    <div className="headerRight-Right">
                        {/*<div className="hdrLng dropdown">*/}
                            {/*<a id="hdrLngToggle" className="hdrLngToggle color-1-hover-bg" title="Language" href="#"*/}
                               {/*data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/}
                                {/*<span>En</span>*/}
                            {/*</a>*/}
                            {/*<ul id="hdrLngWrap" className="dropdown-menu dropdown-menu-custom hdrDropdown lngDropdown"*/}
                                {/*aria-labelledby="hdrLngToggle">*/}
                                {/*<li className="dropdown-menu-body">*/}
                                    {/*<div>*/}
                                        {/*<ul className="hdrTasksMenu">*/}
                                            {/*<li><a href="#">En</a></li>*/}
                                            {/*<li><a href="#">Ru</a></li>*/}
                                            {/*<li><a href="#">Hy</a></li>*/}
                                        {/*</ul>*/}
                                    {/*</div>*/}
                                {/*</li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                        {/*<div className="hdrMsg dropdown">*/}
                            {/*<a id="hdrMsgToggle" className="hdrMsgToggle color-1-hover-bg" title="Messages" href="#"*/}
                               {/*data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/}
                                {/*<i className="fa fa-envelope"></i>*/}
                                {/*<span>12</span>*/}
                            {/*</a>*/}
                            {/*<ul id="hdrMsgWrap" className="dropdown-menu dropdown-menu-custom hdrDropdown"*/}
                                {/*aria-labelledby="hdrMsgToggle">*/}
                                {/*<li className="dropdown-menu-header">*/}
                                    {/*<h2>4 new messages</h2>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-body">*/}
                                    {/*<div>*/}
                                        {/*<ul>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-1.jpg" alt=""/>*/}
                                                    {/*<h2>Mher Sargsyan</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Now</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Gordz ara!</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-6.jpg" alt=""/>*/}
                                                    {/*<h2>Grzegorz Brzeczyszczykiewicz</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Today 15:30</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-8.jpg" alt=""/>*/}
                                                    {/*<h2>Artavazd Yeghiazaryan</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Today 15:30</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-2.jpg" alt=""/>*/}
                                                    {/*<h2>Artavazd Yeghiazaryan</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Today 15:30</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-6.jpg" alt=""/>*/}
                                                    {/*<h2>Grzegorz Brzeczyszczykiewicz</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Today 15:30</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<img src="/img/users/user-8.jpg" alt=""/>*/}
                                                    {/*<h2>Artavazd Yeghiazaryan</h2>*/}
                                                    {/*<h3>*/}
                                                        {/*<i className="fa fa-clock-o"></i>*/}
                                                        {/*<span>Today 15:30</span>*/}
                                                    {/*</h3>*/}
                                                    {/*<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</p>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                        {/*</ul>*/}
                                    {/*</div>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-footer">*/}
                                    {/*<a href="#">See All</a>*/}
                                {/*</li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                        {/*<div className="hdrNtf dropdown">*/}
                            {/*<a id="hdrNtfToggle" className="hdrNtfToggle color-1-hover-bg" title="Notifications"*/}
                               {/*href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/}
                                {/*<i className="fa fa-bell"></i>*/}
                                {/*<span>123</span>*/}
                            {/*</a>*/}
                            {/*<ul id="hdrNtfWrap" className="dropdown-menu dropdown-menu-custom hdrDropdown"*/}
                                {/*aria-labelledby="hdrNtfToggle">*/}
                                {/*<li className="dropdown-menu-header">*/}
                                    {/*<h2>You have 10 notifications</h2>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-body">*/}
                                    {/*<div>*/}
                                        {/*<ul className="hdrNtfMenu">*/}
                                            {/*<li><a href="#"><i className="fa fa-users color-2"></i> 5 new members joined*/}
                                                {/*today</a></li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<i className="fa fa-warning color-orange"></i> Very long description*/}
                                                    {/*here that may not fit into the page and may cause design problems*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li><a href="#"><i className="fa fa-users color-red"></i> 5 new members*/}
                                                {/*joined</a></li>*/}
                                            {/*<li><a href="#"><i className="fa fa-shopping-cart color-green"></i> 25 sales*/}
                                                {/*made</a></li>*/}
                                            {/*<li><a href="#"><i className="fa fa-user color-red"></i> You changed your*/}
                                                {/*username</a></li>*/}
                                        {/*</ul>*/}
                                    {/*</div>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-footer"><a href="#">View all</a></li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                        {/*<div className="hdrTasks dropdown">*/}
                            {/*<a id="hdrTasksToggle" className="hdrTasksToggle color-1-hover-bg" title="Tasks" href="#"*/}
                               {/*data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/}
                                {/*<i className="fa fa-flag"></i>*/}
                                {/*<span>8</span>*/}
                            {/*</a>*/}
                            {/*<ul id="hdrTasksWrap"*/}
                                {/*className="dropdown-menu dropdown-menu-custom dropdown-menu-right hdrDropdown"*/}
                                {/*aria-labelledby="hdrTasksToggle">*/}
                                {/*<li className="dropdown-menu-header">*/}
                                    {/*<h2>You have 8 tasks</h2>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-body">*/}
                                    {/*<div>*/}
                                        {/*<ul className="hdrTasksMenu">*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<h2>Design some buttons <span className="pull-right">20%</span></h2>*/}
                                                    {/*<div className="progress xs">*/}
                                                        {/*<div className="progress-bar color-1-bg" style={{width: '20%'}}*/}
                                                             {/*role="progressbar" aria-valuenow={20} aria-valuemin={0}*/}
                                                             {/*aria-valuemax={100}>*/}
                                                            {/*<span className="sr-only">20% Complete</span>*/}
                                                        {/*</div>*/}
                                                    {/*</div>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<h2>Create a nice theme<span className="pull-right">40%</span></h2>*/}
                                                    {/*<div className="progress xs">*/}
                                                        {/*<div className="progress-bar color-green-bg"*/}
                                                             {/*style={{width: '40%'}} role="progressbar"*/}
                                                             {/*aria-valuenow={20} aria-valuemin={0} aria-valuemax={100}>*/}
                                                            {/*<span className="sr-only">40% Complete</span>*/}
                                                        {/*</div>*/}
                                                    {/*</div>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<h2>Some task I need to do <span className="pull-right">60%</span>*/}
                                                    {/*</h2>*/}
                                                    {/*<div className="progress xs">*/}
                                                        {/*<div className="progress-bar color-red-bg"*/}
                                                             {/*style={{width: '60%'}} role="progressbar"*/}
                                                             {/*aria-valuenow={20} aria-valuemin={0} aria-valuemax={100}>*/}
                                                            {/*<span className="sr-only">60% Complete</span>*/}
                                                        {/*</div>*/}
                                                    {/*</div>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                            {/*<li>*/}
                                                {/*<a href="#">*/}
                                                    {/*<h2>Make beautiful transitions<span*/}
                                                        {/*className="pull-right">80%</span></h2>*/}
                                                    {/*<div className="progress xs">*/}
                                                        {/*<div className="progress-bar color-orange-bg"*/}
                                                             {/*style={{width: '80%'}} role="progressbar"*/}
                                                             {/*aria-valuenow={20} aria-valuemin={0} aria-valuemax={100}>*/}
                                                            {/*<span className="sr-only">80% Complete</span>*/}
                                                        {/*</div>*/}
                                                    {/*</div>*/}
                                                {/*</a>*/}
                                            {/*</li>*/}
                                        {/*</ul>*/}
                                    {/*</div>*/}
                                {/*</li>*/}
                                {/*<li className="dropdown-menu-footer"><a href="#">View all tasks</a></li>*/}
                            {/*</ul>*/}
                        {/*</div>*/}
                        <div className="hdrUser dropdown">
                            <a id="hdrUSerToggle" className="hdrUserToggle color-1-hover-bg" title="User" href="#"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src={((user !== null && typeof user.avatar !== 'undefined' ) ? user.avatar : null)} alt=""/>
                                <h2><span>{((user !== null && typeof user.firstName !== 'undefined') ? (user.firstName + ' ' + user.lastName) : '')}</span></h2>
                            </a>
                            <ul id="hdrUserWrap"
                                className="dropdown-menu dropdown-menu-custom dropdown-menu-right userDropdown">
                                <li className="dropdown-menu-header color-1-bg">
                                    <img src={((user !== null && typeof user.avatar !== 'undefined' ) ? user.avatar : null)} className="/img-circle" alt=""/>
                                    <p>{((user !== null && typeof user.firstName !== 'undefined') ? (user.firstName + ' ' + user.lastName) : '')}</p>
                                </li>
                                {/*<li className="dropdown-menu-body userMenuBody">*/}
                                    {/*<div>*/}
                                        {/*<ul className="hdrUserMenu">*/}
                                            {/*<li>*/}
                                                {/*<div className="row">*/}
                                                    {/*<div className="col-xs-12 col-sm-4 text-center">*/}
                                                        {/*<a href="#" className="color-black-50 color-info-hover">Link*/}
                                                            {/*1</a>*/}
                                                    {/*</div>*/}
                                                    {/*<div className="col-xs-12 col-sm-4 text-center">*/}
                                                        {/*<a href="#" className="color-black-50 color-info-hover">Link*/}
                                                            {/*2</a>*/}
                                                    {/*</div>*/}
                                                    {/*<div className="col-xs-12 col-sm-4 text-center">*/}
                                                        {/*<a href="#" className="color-black-50 color-info-hover">Link*/}
                                                            {/*3</a>*/}
                                                    {/*</div>*/}
                                                {/*</div>*/}
                                            {/*</li>*/}
                                        {/*</ul>*/}
                                    {/*</div>*/}
                                {/*</li>*/}
                                <li className="dropdown-menu-footer userMenuFooter">
                                    {/*<div className="pull-left">*/}
                                        {/*<button type="button"*/}
                                                {/*className="btn btn-sm btn-flat color-2-bg color-2-hover-bg color-text-white">*/}
                                            {/*Profile*/}
                                        {/*</button>*/}
                                    {/*</div>*/}
                                    <div className="pull-right">
                                        <button onClick={handleSignOut} type="button"
                                                className="btn btn-sm btn-flat color-info-bg color-info-hover-bg color-text-white">
                                            Sign out
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <a href="#" className="sideMenuToggle color-1-hover-bg" title="Settings">
                                <i className="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                </section>
            </header>
            <section className="mtSidebar">
                <div className="sidebar-menu clearfix">
                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab">
                                <h4 className="panel-title">
                                    <Link to="/">
                                        <div className="panel-title-left">
                                            <i className="fa fa-dashboard"></i>
                                            <span>Dashboard</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </Link>
                                </h4>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-1">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-1" aria-controls="collapse-1"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-files-o"></i>
                                            <span>Pages</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-1" aria-labelledby="heading-1" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/pages/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <a href="/page/add">
                                                <span>Add Page</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-2">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-2" aria-controls="collapse-2"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-file-text-o"></i>
                                            <span>Post</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-2" aria-labelledby="heading-2" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/posts/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/post/add">
                                                <span>Add Post</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/category/index">
                                                <span>Categories</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/category/add">
                                                <span>Add Category</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-3">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-3" aria-controls="collapse-3"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-road"></i>
                                            <span>Trip</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-3" aria-labelledby="heading-3" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/trips/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/trip/add">
                                                <span>Add Trip</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/trip/users-added">
                                                <span>Users' Added</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-4">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-4" aria-controls="collapse-4"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-video-camera"></i>
                                            <span>Scenes</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-4" aria-labelledby="heading-4" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/scenes/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/scenes/add">
                                                <span>Add Scene</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/scenes/users-added">
                                                <span>Users' Added</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-5">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-5" aria-controls="collapse-5"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-map-marker"></i>
                                            <span>Locations</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-5" aria-labelledby="heading-5" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/locations/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/location/add">
                                                <span>Add Location</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/locations/users-added">
                                                <span>Users' Added</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-6">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-6" aria-controls="collapse-6"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-film"></i>
                                            <span>Movies</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-6" aria-labelledby="heading-6" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/movies/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <a href="/movie/add">
                                                <span>Add Movie</span>
                                            </a>
                                        </li>
                                        <li>
                                            <Link to="/movies/users-added">
                                                <span>Users' Added</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-7">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-7" aria-controls="collapse-7"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-globe"></i>
                                            <span>Geo</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-7" aria-labelledby="heading-7" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/countries/index">
                                                <span>Countries</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/cities/index">
                                                <span>Cities</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/geo/add">
                                                <span>Add Country/City</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default panel-bg">
                            <div className="panel-heading" role="tab" id="heading-8">
                                <h4 className="panel-title sidebar-title">
                                    <a className="collapsed" href="#collapse-8" aria-controls="collapse-8"
                                       data-parent="#accordion" role="button" data-toggle="collapse"
                                       aria-expanded="false">
                                        <div className="panel-title-left">
                                            <i className="fa fa-users"></i>
                                            <span>Users</span>
                                        </div>
                                        <div className="panel-title-right"></div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-8" aria-labelledby="heading-8" className="panel-collapse collapse"
                                 role="tabpanel">
                                <div className="panel-body">
                                    <ul>
                                        <li>
                                            <Link to="/users/index">
                                                <span>View All</span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/users/add">
                                                <span>Add User</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="contentWrapper">
                {children}
            </section>
            <footer className="color-1-bg">
                <div className="footerIn">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="clearfix">
                                    <div className="footerLeft">
                                        &copy; 2017 MovieTrip team
                                    </div>
                                    <div className="footerRight"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div> 
    </div>
);

export default Base;
