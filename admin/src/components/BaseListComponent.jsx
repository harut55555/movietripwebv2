import React from 'react';
import Row from './Row/Row.jsx';
import Col from './Col/Col.jsx';
import MTBox from './MTBox/MTBox.jsx';
import MTBoxBody from './MTBox/MTBoxBody.jsx';
import MTBoxFooter from './MTBox/MTBoxFooter.jsx';
import MTTable from './MTTable/MTTable.jsx';
import MTCheckbox from './MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from './ContentWrapTop.jsx';
import ListFilter from './ListFilter.jsx';
import { Link, IndexLink } from 'react-router';
import Typography from './Typography/Typography'
import ModalYesNo from './Modals/ModalYesNo'
import LocationsListComponent from './Locations/LocationsListComponent'
import MoviesListComponent from './Movies/MoviesListComponent'
import ScenesListComponent from './Scenes/ScenesListComponent'
import TripListComponent from './Trip/TripListComponent'
import PagesListComponent from './Pages/PagesListComponent'
import PostsListComponent from './Posts/PostsListComponent'
import UsersListComponent from './Users/UsersListComponent'
import CategoryListComponent from './Posts/CategoryListComponent'

const BaseListComponent = ({state,
                               list,
                               count,
                               bootStrap,
                               viewCount,
                               onChangeViewCount,
                               onClickSearch,
                               onClickItemDelete,
                               onClickDelete,
                               onClickModalYes,
                               onClickModalNo}) => {
    return (
        <div>
            {/*<ContentWrapTop bootStrap={bootStrap}/>*/}
            <div className="contentWrap">
                <Row>
                    <Col>
                        <MTBox>
                            <ListFilter state={state} onClickSearch={onClickSearch} onClickDelete={onClickDelete}/>
                            <MTBoxBody classname="listTable">
                                {(bootStrap.controller === "locations" ? <LocationsListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "movies" ? <MoviesListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "scenes" ? <ScenesListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "trips" ? <TripListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "pages" ? <PagesListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "posts" ? <PostsListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "category" ? <CategoryListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}

                                {(bootStrap.controller === "users" ? <UsersListComponent
                                    bootStrap={bootStrap}
                                    list={list}
                                    onClickItemDelete={onClickItemDelete}
                                /> : null)}
                            </MTBoxBody>
                            <MTBoxFooter type="paginationFooter" controller={bootStrap.controller} count={count} params={bootStrap.params}
                                         viewCount={viewCount} onChangeViewCount={onChangeViewCount}/>
                        </MTBox>
                    </Col>
                </Row>
            </div>
            <ModalYesNo onClickYes={onClickModalYes} onClickNo={onClickModalNo} />
        </div>
    );
};

export default BaseListComponent;
