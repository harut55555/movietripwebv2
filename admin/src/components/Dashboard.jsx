import React from 'react';
import Row from './Row/Row.jsx';
import Col from './Col/Col.jsx';
import MTWidget from './MTWidget/MTWidget.jsx';
import MTBox from './MTBox/MTBox.jsx';
import MTBoxHeader from './MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from './MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from './MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from './MTBox/MTBoxBody.jsx';
import MTBoxFooter from './MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from './MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from './MTBox/MTBoxFooterRight.jsx';
import MTTable from './MTTable/MTTable.jsx';
import MTCheckbox from './MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from './ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const Dashboard = ({ bootStrapArray, locationCount, movieCount, scenaCount}) => (
 <div>
	 {/*<ContentWrapTop name={["Dashboard"]} bootStrapArray={bootStrapArray} />*/}
	 <div className="contentWrap">
		 <Row>
			 <Col>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <span>Control Panel</span>
							 </h3>
						 </MTBoxHeaderLeft>
						 <MTBoxHeaderRight>

						 </MTBoxHeaderRight>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <Row>
							 <Col sm={4}>
								 <MTWidget colorClass="success" size="lg" icon="video-camera" title="Scenes" count={locationCount} progress="false" link="/scenes/index" linkText="View All" />
							 </Col>
							 <Col sm={4}>
								 <MTWidget colorClass="success" size="lg" icon="map-marker" title="Locations" count={movieCount} progress="false" link="/locations/index" linkText="View All" />
							 </Col>
							 <Col sm={4}>
								 <MTWidget colorClass="success" size="lg" icon="film" title="Movies" count={scenaCount} progress="false" link="/movies/index" linkText="View All" />
							 </Col>
						 </Row>
					 </MTBoxBody>
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default Dashboard;
