import React from 'react';
import { Link, IndexLink } from 'react-router';

const Typography = ({children}) => {
	return (
	 <div className="typography">
		 {children}
	 </div>
	)
};

export default Typography;