import React from 'react';
import {Link, IndexLink} from 'react-router';

const ContentWrapTop = ({bootStrap}) => (
    <div>
        <section className="contentWrapTop">
            <div className="row">
                <div className="col-xs-12">
                    <div className="contentWrapTopLeft">
                        {/*<h1>*/}
                            {/*{bootStrap.breadCrumbs.map(function (object, i) {*/}
                                {/*return <li key={i}>*/}
                                    {/*{(bootStrap.controller === bootStrap.breadCrumbs[i].controller) ?*/}
                                        {/*bootStrap.breadCrumbs[i].name :*/}
                                        {/*<Link to={bootStrap.breadCrumbs[i].path}>bootStrap.breadCrumbs[i].name</Link>*/}
                                    {/*}*/}
                                {/*</li>*/}
                            {/*})}*/}
                        {/*</h1>*/}
                        {/*<h1>{name[0]}*/}
                            {/*{name[1] !== "" &&*/}
                            {/*<small>*/}
                                {/*&nbsp;{name[1]}*/}
                            {/*</small>*/}
                            {/*}*/}
                        {/*</h1>*/}
                    </div>
                    <div className="contentWrapTopRight">
                        <ul>
                            {bootStrap.breadCrumbs.map(function (object, i) {
                                return <li key={i}>
                                        {(bootStrap.controller === bootStrap.breadCrumbs[i].controller) ?
                                            bootStrap.breadCrumbs[i].name :
                                            <Link to={bootStrap.breadCrumbs[i].path}>{bootStrap.breadCrumbs[i].name}</Link>
                                        }
                                </li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
);


export default ContentWrapTop;