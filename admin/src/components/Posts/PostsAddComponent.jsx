import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import Typography from '../Typography/Typography.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import Publish from '../Publish.jsx'
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const PostsAddComponent = ({bootStrapArray,post,  categories,disabledState,handleChange, handleBlur}) => {

    let disabled = "";
    let di="";
    let c;
    if (disabledState === 1) {
        disabled = "disabled";
        di=disabled;
    }
  //  (location.country!==null  ) &&  (location.city!==null ) && disabled !== "disabled"? di="" : di="disabled";
  return  (
        <div>
            <ContentWrapTop name={["Add Post"]} bootStrapArray={bootStrapArray}/>
            <div className="contentWrap">
                <Row>
                    <Col sm={9}>
                        <MTBox>
                            <MTBoxHeader>
                                <MTBoxHeaderLeft>
                                    <h3>
                                        <span>Info</span>
                                    </h3>
                                </MTBoxHeaderLeft>
                            </MTBoxHeader>
                            <MTBoxBody>
                                <Row>
                                    <Col>
                                        <FormGroup>
                                            <label htmlFor="blogPostTitle">Title</label>
                                            <input type="text" id="blogPostTitle" name="blogPostTitle"
                                                   className="form-control"
                                                   value={post.title} onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>
                                        {disabled === "disabled" ? <Row>
                                            <Col>
                                                <FormGroup>
                                                    <h2>Necessarily adding name to fill another contents</h2>
                                                </FormGroup>
                                            </Col>
                                        </Row> : null}
                                        <FormGroup>
                                            <label htmlFor="blogPostAlias">Alias</label>
                                            <input disabled={disabled}  type="text" id="blogPostAlias" name="blogPostAlias"
                                                   className="form-control"
                                                   value={post.alias} onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={3} md={3}>
                                        <FormGroup>
                                            <label htmlFor="blogPostAllowComments">Allow comments</label>
                                            <select disabled={disabled} name="blogPostAllowComments" id="blogPostAllowComments"
                                                    className="form-control" onChange={handleChange}>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={3} md={3}>
                                        <FormGroup>
                                            <label htmlFor="blogPostTop">Top</label>
                                            <select  disabled={disabled} name="blogPostTop" id="blogPostTop" className="form-control" onChange={handleChange}>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={3} md={3}>
                                        <FormGroup>
                                            <label htmlFor="blogPostDate">Date</label>
                                            <input  disabled={disabled}  type="text" id="blogPostDate" name="blogPostDate"
                                                   className="form-control"/>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <FormGroup>
                                            <label htmlFor="blogPostTakeAway">TakeAway</label>
                                            <textarea className="form-control" name="blogPostTakeAway"
                                                      id="blogPostTakeAway" cols="" rows="4" value={post.takeAway} onChange={handleChange} onBlur={handleBlur}></textarea>
                                        </FormGroup>
                                        <FormGroup>
                                            <label htmlFor="blogPostTags">Tags</label>
                                            <input  type="text" name="blogPostTags" id="blogPostTags" value=""/>
                                        </FormGroup>
                                        <FormGroup>
                                            <label htmlFor="blogPostContent">Content</label>
                                            <textarea name="blogPostContent" id="blogPostContent" cols="" rows="" value={post.content} onChange={handleChange} onBlur={handleBlur}></textarea>
                                        </FormGroup>
                                        <FormGroup>
                                            <label htmlFor="blogPostBrief">Brief</label>
                                            <textarea className="form-control" name="blogPostBrief" id="blogPostBrief"
                                                      cols="" rows="4" value={post.brief} onChange={handleChange} onBlur={handleBlur} ></textarea>
                                        </FormGroup>
                                        <FormGroup>
                                            <label htmlFor="blogPostAuthor">Author</label>
                                            <input type="text" id="blogPostAuthor" name="blogPostAuthor"
                                                   className="form-control" value={post.author} onChange={handleChange} onBlur={handleBlur}/>
                                        </FormGroup>

                                    </Col>
                                </Row>
                            </MTBoxBody>
                        </MTBox>
                        <MTBox>
                            <MTBoxHeader>
                                <MTBoxHeaderLeft>
                                    <h3>
                                        <span>Cover Image</span>
                                    </h3>
                                </MTBoxHeaderLeft>
                            </MTBoxHeader>
                            <MTBoxBody>
                                <Row>
                                    <Col>

                                    </Col>
                                </Row>
                            </MTBoxBody>
                        </MTBox>
                    </Col>
                    <Col sm={3}>
                        <Publish label={post}/>
                        <MTBox>
                            <MTBoxHeader>
                                <MTBoxHeaderLeft>
                                    <h3>
                                        <span>Author settings</span>
                                    </h3>
                                </MTBoxHeaderLeft>
                            </MTBoxHeader>
                            <MTBoxBody>
                                <Row>
                                    <Col>
                                        <div className="input-group input-group-sm">
									 <span className="input-group-addon noradius">
										 <span>User:</span>
									 </span>
                                            <label htmlFor="blogPostAuthorUser" className="hidden" hidden></label>
                                            <input type="text" id="blogPostAuthorUser" name="blogPostAuthorUser"
                                                   className="form-control"/>
                                        </div>
                                    </Col>
                                </Row>
                            </MTBoxBody>
                        </MTBox>
                        <MTBox>
                            <MTBoxHeader>
                                <MTBoxHeaderLeft>
                                    <h3>
                                        <span>Categories</span>
                                    </h3>
                                </MTBoxHeaderLeft>
                            </MTBoxHeader>
                            <MTBoxBody>
                                <Row>
                                    <Col>
                                        <Typography>

                                            <ul className="list-unstyled">
                                            {categories.map(function (object, i) {

                                                return <li key={categories[i].id}>
                                                        <MTCheckbox type="checkbox" namecheck="blogPostCategory"
                                                                    id={"blogPostCategory" + categories[i].id} text={categories[i].title} name={categories[i].id} />
                                                    </li>

                                            })}
                                            </ul>
                                        </Typography>
                                    </Col>
                                </Row>
                            </MTBoxBody>
                        </MTBox>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default PostsAddComponent;
