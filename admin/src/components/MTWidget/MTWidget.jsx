import React from 'react';
import { Link, IndexLink } from 'react-router';

const MTWidget = ({size, colorClass, icon, title, count, counticon, progress, progressPercent, progressText, link, linkText}) => {
	let colorClassPrefix = "mt-widget-color-";
	let colors = ["success", "warning", "error", "invert", "invert-success", "invert-warning", "invert-error"];

	let countIcon = "fa fa-" + counticon;

	if (typeof linkText === "undefined") {
		linkText = "More info";
	}

	if (size === "lg") {
		colorClassPrefix = "mt-widget-lg-color-";
		colors.map(function(object, i) {
			if (colorClass === colors[i]) {
				colorClass = colorClassPrefix + colors[i];
			}
		});
		return (
		 <div className={"mt-widget-lg " + colorClass}>
			 <div className="mt-widget-lg-top">
				 <h2>{count} {counticon !== "" ? <i className={countIcon}></i> : null}</h2>
				 <h3>{title}</h3>
				 <i className={"fa fa-" + icon} />
			 </div>
			 <div className="mt-widget-lg-bottom">
				 <Link to={link}>
            <span>
              <span>{linkText}</span>
              <i className="fa fa-chevron-right" />
            </span>
				 </Link>
			 </div>
		 </div>
		)
	}
	colors.map(function(object, i) {
		if (colorClass === colors[i]) {
			colorClass = colorClassPrefix + colors[i];
		}
	});

	return (
		<div className={"mt-widget " + colorClass}>
			<div className="mt-widgetIcon">
				<i className={"fa fa-" + icon} />
			</div>
			<div className="mt-widgetContent">
				<h2>{title}</h2>
				<h3>{count} {counticon !== "" ? <i className="fa fa-:counticon"></i> : null}</h3>
				{progress === "true" ?
					<div className="mt-widgetProgress">
						<div className="progress">
							<div className="progress-bar" role="progressbar" aria-valuenow={progressPercent} aria-valuemin={0} aria-valuemax={100} style={{width: progressPercent + '%'}} />
						</div>
						<span>{progressText}</span>
					</div>
					: null
				}
			</div>
		</div>
	)
};

export default MTWidget;