import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const BlogListComponent = ({home, root,typerout,handleChange,value,pagescroll,page,max}) => (
 <div>
	 <ContentWrapTop name={["Blog"]} root={root} />
	 <div className="contentWrap">
		 <Row>
			 <Col>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <div>
								 <div className="btn-group btn-group-sm nowrap">
									 <Link to="/blog/add" title="Add" className="btn btn-flat btn-info"><i className="fa fa-plus"></i></Link>
									 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
								 </div>
							 </div>
						 </MTBoxHeaderLeft>
						 <MTBoxHeaderRight>
							 <div className="boxStatus boxHeaderRightSelectWrap-md">
								 <label for="blogStatus" className="hidden" hidden></label>
								 <select name="blogStatus" id="blogStatus" className="selectize selectize-sm" defaultValue="">
									 <option value="" disabled hidden>Select Status</option>
									 <option value="-1">All</option>
									 <option value="0">Draft</option>
									 <option value="1">Publish</option>
								 </select>
							 </div>
							 <div className="boxCategory boxHeaderRightSelectWrap-md">
								 <label for="blogCategory" className="hidden" hidden></label>
								 <select name="blogCategory" id="blogCategory" className="selectize selectize-sm" defaultValue="">
									 <option value="" disabled hidden>Select Category</option>
									 <option value="-1">All</option>
									 <option value="1">Uncategorized</option>
									 <option value="4">Movie News</option>
									 <option value="6">----Trailers</option>
									 <option value="5">Movie articles</option>
									 <option value="7">----Posters</option>
									 <option value="8">----Reviews</option>
									 <option value="9">Blog</option>
								 </select>
							 </div>
							 <div className="boxLang boxHeaderRightSelectWrap-sm">
								 <label for="blogLang" className="hidden" hidden></label>
								 <select name="blogLang" id="blogLang" className="selectize selectize-sm">
									 <option value="1">English</option>
									 <option value="2">Русский</option>
								 </select>
							 </div>
							 <div className="boxSearch">
								 <div className="input-group input-group-sm">
									 <label for="blogSearch" className="hidden" hidden></label>
									 <input className="form-control pull-right" id="blogSearch" name="blogSearch" type="text" placeholder="Search"/>
									 <div className="input-group-btn">
										 <button type="submit" className="btn btn-flat color-2-bg color-2-hover-bg color-text-white">
											 <i className="fa fa-search"></i>
										 </button>
									 </div>
								 </div>
							 </div>
						 </MTBoxHeaderRight>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <MTTable classnames={["bordered", "hover", "valignM"]}>
							 <thead>
							 <tr>
								 <th className="minimal-width">
									 <MTCheckbox type="checkbox" name="blogCheckbox" id="blogCheckboxAll"/>
								 </th>
								 <th>Title</th>
								 <th>Author</th>
								 <th>Status</th>
								 <th>Comment Count</th>
								 <th colSpan="2">Date</th>
							 </tr>
							 </thead>
							 <tbody>
							 {typerout.map(function (object, i) {
								 let statusClassName = "";
								 let statusText = "";
								 if (typerout[i].status === 0) {
									 statusClassName = "danger";
									 statusText = "Deleted";
								 }
								 if (typerout[i].status === 1) {
									 statusClassName = "success";
									 statusText = "Publish";
								 }
								 if (typerout[i].status === 2) {
									 statusClassName = "warning";
									 statusText = "Pending";
								 }
								 if (typerout[i].status === 3) {
									 statusClassName = "danger";
									 statusText = "Denied";
								 }
								 return <tr key={i}>
									 <td className="minimal-width">
										 <MTCheckbox type="checkbox" name="tripCheckbox" id="blogCheckbox1"/>
									 </td>
									 <td><a href={root[1].replace('/index','/') +'add' }>{typerout[i].name}</a></td>
									 <td>{typerout[i].user_id}</td>
									 <td><span className={"label label-" + statusClassName}>{statusText}</span></td>
									 <td>{typerout[i].commentCount}</td>
									 <td>{typerout[i].date}</td>
									 <td className="minimal-width">
										 <div className="btn-group btn-group-sm nowrap">
											 <a href={root[1].replace('/index','/') +'add' } title="Edit" className="btn btn-flat btn-info"><i className="fa fa-pencil"></i></a>
											 <button type="button" title="Delete" className="btn btn-flat btn-info"><i className="fa fa-trash-o"></i></button>
										 </div>
									 </td>
								 </tr>
							 })}
							 </tbody>
						 </MTTable>
					 </MTBoxBody>
					 <MTBoxFooter type="paginationFooter" handleChange={handleChange} page={page} pagescroll={pagescroll} root={root} max={max}/>
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default BlogListComponent;
