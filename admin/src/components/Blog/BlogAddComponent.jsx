import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import Typography from '../Typography/Typography.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import Publish from '../Publish.jsx'
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import ContentWrapTop from '../ContentWrapTop.jsx';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const BlogAddComponent = ({home, root}) => (
 <div>
	 <ContentWrapTop name={["Add Blog Post"]} root={root} />
	 <div className="contentWrap">
		 <Row>
			 <Col sm={9}>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <span>Info</span>
							 </h3>
						 </MTBoxHeaderLeft>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <Row>
							 <Col>
								 <FormGroup>
									 <label htmlFor="blogPostAlias">Alias</label>
									 <input type="text" id="blogPostAlias" name="blogPostAlias" className="form-control"/>
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostTitle">Title</label>
									 <input type="text" id="blogPostTitle" name="blogPostTitle" className="form-control"/>
								 </FormGroup>
							 </Col>
						 </Row>
						 <Row>
							 <Col sm={3} md={3}>
								 <FormGroup>
									 <label htmlFor="blogPostLang">Language</label>
									 <select name="blogPostLang" id="blogPostLang" className="selectize">
										 <option value="1">English</option>
										 <option value="2">Русский</option>
									 </select>
								 </FormGroup>
							 </Col>
							 <Col sm={3} md={3}>
								 <FormGroup>
									 <label htmlFor="blogPostAllowComments">Allow comments</label>
									 <select name="blogPostAllowComments" id="blogPostAllowComments" className="selectize">
										 <option value="1">Yes</option>
										 <option value="2">No</option>
									 </select>
								 </FormGroup>
							 </Col>
							 <Col sm={3} md={3}>
								 <FormGroup>
									 <label htmlFor="blogPostTop">Top</label>
									 <select name="blogPostTop" id="blogPostTop" className="selectize">
										 <option value="1">Yes</option>
										 <option value="2">No</option>
									 </select>
								 </FormGroup>
							 </Col>
							 <Col sm={3} md={3}>
								 <FormGroup>
									 <label htmlFor="blogPostDate">Date</label>
									 <input type="text" id="blogPostDate" name="blogPostDate" className="form-control"/>
								 </FormGroup>
							 </Col>
						 </Row>
						 <Row>
							 <Col>
								 <FormGroup>
									 <label htmlFor="blogPostTakeAway">TakeAway</label>
									 <textarea className="form-control" name="blogPostTakeAway" id="blogPostTakeAway" cols="" rows="4"></textarea>
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostTags">Tags</label>
									 <input type="text" name="blogPostTags" id="blogPostTags" value="" />
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostEditor">Content</label>
									 <textarea name="" id="blogPostEditor" cols="" rows=""></textarea>
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostBrief">Brief</label>
									 <textarea className="form-control" name="blogPostBrief" id="blogPostBrief" cols="" rows="4"></textarea>
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostAuthor">Author</label>
									 <input type="text" id="blogPostAuthor" name="blogPostAuthor" className="form-control"/>
								 </FormGroup>
								 <FormGroup>
									 <label htmlFor="blogPostPhotograph">Photograph</label>
									 <input type="text" id="blogPostPhotograph" name="blogPostPhotograph" className="form-control"/>
								 </FormGroup>
							 </Col>
						 </Row>
					 </MTBoxBody>
				 </MTBox>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <span>Cover Image</span>
							 </h3>
						 </MTBoxHeaderLeft>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <Row>
							 <Col>

							 </Col>
						 </Row>
					 </MTBoxBody>
				 </MTBox>
			 </Col>
			 <Col sm={3}>
				 <Publish/>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <span>Author settings</span>
							 </h3>
						 </MTBoxHeaderLeft>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <Row>
							 <Col>
								 <div className="input-group input-group-sm">
									 <span className="input-group-addon noradius">
										 <span>User:</span>
									 </span>
									 <label htmlFor="blogPostAuthorUser" className="hidden" hidden></label>
									 <input type="text" id="blogPostAuthorUser" name="blogPostAuthorUser" className="form-control" />
								 </div>
							 </Col>
						 </Row>
					 </MTBoxBody>
				 </MTBox>
				 <MTBox>
					 <MTBoxHeader>
						 <MTBoxHeaderLeft>
							 <h3>
								 <span>Categories</span>
							 </h3>
						 </MTBoxHeaderLeft>
					 </MTBoxHeader>
					 <MTBoxBody>
						 <Row>
							 <Col>
								 <Typography>
									 <ul className="list-unstyled">
										 <li>
											 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory1" text="Uncategorized" />
										 </li>
										 <li>
											 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory2" text="Movie News" />
											 <ul className="list-unstyled">
												 <li>
													 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory3" text="Trailers" />
												 </li>
											 </ul>
										 </li>
										 <li>
											 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory4" text="Movie articles" />
											 <ul className="list-unstyled">
												 <li>
													 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory5" text="Posters" />
												 </li>
												 <li>
													 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory6" text="Reviews" />
												 </li>
											 </ul>
										 </li>
										 <li>
											 <MTCheckbox type="checkbox" name="blogPostCategory" id="blogPostCategory7" text="Blog" />
										 </li>
									 </ul>
								 </Typography>
							 </Col>
						 </Row>
					 </MTBoxBody>
				 </MTBox>
			 </Col>
		 </Row>
	 </div>
 </div>
);

export default BlogAddComponent;
