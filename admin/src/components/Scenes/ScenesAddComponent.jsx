import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import Publish from '../Publish.jsx'
import ContentWrapTop from '../ContentWrapTop.jsx';
import MapService from '../../services/MapService';
import {Link, IndexLink} from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const ScenesAddComponent = ({bootStrapArray, scene, disabledState, handleChange, handleBlur,onMapLoad,onSelect}) => {
	let disabled = "";
	if (disabledState === 1) {
		disabled = "disabled";
	}
	return (
	 <div>
		 <ContentWrapTop name={["Add Scene"]} bootStrapArray={bootStrapArray}/>
		 <div className="contentWrap">
			 <Row>
				 <Col sm={9}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Info</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="sceneMovieName">Movie Name</label>
										 <select name="sceneMovieName" id="sceneMovieName" defaultValue="" className="selectize">
											 <option value="" disabled hidden>Select Movie</option>
										 </select>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="sceneLocation">Location</label>
										 <select name="sceneLocation" id="sceneLocation" defaultValue="" className="selectize">
											 <option value="" disabled hidden>Select Location</option>
										 </select>
									 </FormGroup>
									 <FormGroup>
									
										 <label htmlFor="sceneAddress">Address</label>
										 <input type="text" disabled={disabled} id="sceneAddress" name="sceneAddress" className="form-control"
											

												value={ (scene.location!==null && (scene.location.address!==null || scene.location.address!== undefined) &&
												 (scene.location.address.name!==undefined || scene.location.address.name!==null) )  ?  scene.location.address.name: "" }
												onChange={handleChange} onBlur={handleBlur}/>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="sceneDescription">Description</label>
										 <textarea disabled={disabled} className="form-control" name="sceneDescription" id="sceneDescription" cols="" rows="4"></textarea>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="sceneCopyright">Copyright</label>
										 <input disabled={disabled} type="text" id="sceneCopyright"
										        className="form-control"
												name="sceneCopyright" value={ scene.copyright!==null? scene.copyright: "" }
												onChange={handleChange} onBlur={handleBlur}/>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="sceneTags">Tags</label>
										 <input disabled={disabled} type="text" name="sceneTags" id="sceneTags" value=""/>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft >
								 <h3>
									 <span>Cover Image</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col>
									 <div className="coverImage dragleave" id="MTDropBox">
										 <input type="file" hidden className="hidden" accept="image/*" id="MTDropBoxInput"/>
										 <div className="coverImageIn">
											 <span>No files selected!<br/><a href="#" className="coverImageChoose">Choose file(s)</a></span>
										 </div>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Map</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col>
									 <div id="map_canvas" style={{height: "500px"}}>
										 <MapService onMapLoad={onMapLoad}/>
										
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
				 <Col sm={3}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Editor's choice</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col>
									 <label htmlFor="sceneEditorsChoice" className="hidden" hidden></label>
									 <select  id="sceneEditorsChoice" className="selectize selectize-sm" onChange= {onSelect}>
										 <option value="1">Yes</option>
										 <option value="2">No</option>
									 </select>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Check-In Text</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <label htmlFor="sceneCheckinText" className="hidden" hidden></label>
									 <input disabled={disabled} type="text" name="sceneCheckinText" id="sceneCheckinText"
									        className="form-control input-sm"  value={ scene.check_in_text!==null? scene.check_in_text: "" }
											onChange={handleChange} onBlur={handleBlur}/>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
			 </Row>
		 </div>
	 </div>
	)
};

export default ScenesAddComponent;
