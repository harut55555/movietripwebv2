import React from 'react';
import {Link, IndexLink} from 'react-router';

import Row from './Row/Row.jsx';
import Col from './Col/Col.jsx';
import MTWidget from './MTWidget/MTWidget.jsx';
import MTBox from './MTBox/MTBox.jsx';
import MTBoxHeader from './MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from './MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from './MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from './MTBox/MTBoxBody.jsx';
import FormGroup from './FormGroup/FormGroup.jsx';

const Publish = ({onSaveAndClose, onSave, onSaveDraft, publishDisabled}) => (
    <div>
        <MTBox>
            <MTBoxHeader>
                <MTBoxHeaderLeft>
                    <h3>
                        <span>Publish</span>
                    </h3>
                </MTBoxHeaderLeft>
            </MTBoxHeader>
                <MTBoxBody>
                    <Row>
                        <Col md={6}>
                            <FormGroup>
                                {/*<Link to={bootStrapArray}>*/}
                                <button type="button" title="Save & Close"
                                        className="btn btn-sm btn-block btn-flat btn-success" name="1" onClick={onSaveAndClose} disabled={((publishDisabled) ? "disabled" : '')}>{"Save & Close"}</button>
                                    {/*</Link>*/}
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <button type="button" title="Save"  className="btn btn-sm btn-block btn-flat btn-success" name="1"  onClick={onSave} disabled={((publishDisabled) ? "disabled" : '')}>{"Save"}</button>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <FormGroup>
                                <button type="button" title="Save Draft"
                                        className="btn btn-sm btn-block btn-flat btn-warning" name="0"  onClick={onSaveDraft}>{"Save Draft"}</button>
                            </FormGroup>
                        </Col>
                        {/*<Col md={6}>*/}
                            {/*<button type="button" title="Discard"*/}
                                    {/*className="btn btn-sm btn-block btn-flat btn-danger" id="4" onClick={handleClick}>{"Discard"}</button>*/}
                        {/*</Col>*/}
                    </Row>
                </MTBoxBody>
        </MTBox>
    </div>
);

export default Publish;