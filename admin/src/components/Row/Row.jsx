import React from 'react';
import { Link, IndexLink } from 'react-router';

const Row = ({children}) => {
	return (
	 <div className="row">
		 {children}
	 </div>
	)
};

export default Row;