import React from 'react';
import Row from '../Row/Row.jsx';
import Col from '../Col/Col.jsx';
import FormGroup from '../FormGroup/FormGroup.jsx';
import MTWidget from '../MTWidget/MTWidget.jsx';
import MTBox from '../MTBox/MTBox.jsx';
import MTBoxHeader from '../MTBox/MTBoxHeader.jsx';
import MTBoxHeaderLeft from '../MTBox/MTBoxHeaderLeft.jsx';
import MTBoxHeaderRight from '../MTBox/MTBoxHeaderRight.jsx';
import MTBoxBody from '../MTBox/MTBoxBody.jsx';
import MTBoxFooter from '../MTBox/MTBoxFooter.jsx';
import MTBoxFooterLeft from '../MTBox/MTBoxFooterLeft.jsx';
import MTBoxFooterRight from '../MTBox/MTBoxFooterRight.jsx';
import MTTable from '../MTTable/MTTable.jsx';
import MTCheckbox from '../MTCheckbox/MTCheckbox.jsx';
import Publish from '../Publish.jsx'
import ContentWrapTop from '../ContentWrapTop.jsx';
import MapService from '../../services/MapService';
import { Link, IndexLink } from 'react-router';

//{homePopul,homePopulCous,homeMovies,homeSpecial,homePopularSecond,promoBlocklocation,promoBlockscene,promoBlockscene1}
const MoviesAddComponent = ({bootStrapArray, disabledState, movie, handleChange, handleBlur}) => {
	let disabled = "";
	if (disabledState === 1) {
		disabled = "disabled";
	}
	return (
	 <div>
		 <ContentWrapTop name={["Add Movie"]} bootStrapArray={bootStrapArray}/>
		 <div className="contentWrap">
			 <Row>
				 <Col sm={9}>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Info</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="movieName">Name</label>
										 <input type="text" id="movieName" name="movieName" className="form-control" value={movie.name} onChange={handleChange} onBlur={handleBlur}/>
									 </FormGroup>
								 </Col>
							 </Row>
							 <Row>
								 <Col sm={4}>
									 <FormGroup>
										 <label htmlFor="movieYear">Year</label>
										 <input disabled={disabled} type="text" id="movieYear" name="movieYear" className="form-control" value={movie.year} onChange={handleChange} onBlur={handleBlur} />
									 </FormGroup>
								 </Col>
							 </Row>
							 <Row>
								 <Col>
									 <FormGroup>
										 <label htmlFor="movieGenre">Genre(s)</label>
										 <select disabled={disabled} name="movieGenre" id="movieGenre" defaultValue={[]} multiple className="selectize">
											 <option value="" disabled hidden>Select Genre</option>
										 </select>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="movieCountry">Country(s)</label>
										 <select disabled={disabled} name="movieCountry" id="movieCountry" multiple defaultValue={[]} className="selectize">
											 <option value="" disabled hidden>Select Country</option>
										 </select>
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="movieDirector">Director(s)</label>
										 <input disabled={disabled} type="text" id="movieDirector" name="movieDirector" />
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="movieActor">Actor(s)</label>
										 <input disabled={disabled} type="text" id="movieActor" name="movieActor" />
									 </FormGroup>
									 <FormGroup>
										 <label htmlFor="movieTags">Tags</label>
										 <input disabled={disabled} type="text" id="movieTags" name="movieTags" className="selectize" />
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <h3>
								 <span>Description</span>
							 </h3>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col> 
									 <FormGroup>
										 <label htmlFor="movieDescription" hidden className="hidden" />
										 <textarea name="movieDescription" id="movieDescription" cols="" rows=""></textarea>
									 </FormGroup>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
					 <MTBox>
						 <MTBoxHeader>
							 <MTBoxHeaderLeft>
								 <h3>
									 <span>Cover Image</span>
								 </h3>
							 </MTBoxHeaderLeft>
						 </MTBoxHeader>
						 <MTBoxBody classname={disabled}>
							 <Row>
								 <Col>
									 <div className="coverImage dragleave" id="MTDropBox">
										 <input type="file" hidden className="hidden" accept="image/*" id="MTDropBoxInput" />
										 <div className="coverImageIn">
											 <span>No files selected!<br/><label htmlFor="MTDropBoxInput">Choose file(s)</label></span>
										 </div>
									 </div>
								 </Col>
							 </Row>
						 </MTBoxBody>
					 </MTBox>
				 </Col>
				 <Col sm={3}>
					 <Publish label={movie}/>
				 </Col>
			 </Row>
		 </div>
	 </div>
	)
};

export default MoviesAddComponent;
