import ImgService from '../services/ImgService';

class Page {

    id = null;
    user_id = null;
    user_name = null;
    lang = null;
    translate = null;
    status = null;
    title = null;
    content = null;
    alias = null;
    date = null;
    
    constructor(obj) {
        if(obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.user_id = obj.user_id;
        this.user_name = obj.user_name;
        this.lang = obj.lang;
        this.translate = obj.translate;
        this.status = obj.status;
        this.title = obj.title;
        this.content = obj.content;
        this.alias = obj.alias;
        this.date = obj.date;
    }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

    static initRandom() {

    }

    static isComplete(obj) {
        let status = false;
        if (obj.id !== null && obj.title !== null && obj.content !== null)
            status = true;

        return status;
    }
}

export default Page;