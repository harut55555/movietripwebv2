import ImgService from '../services/ImgService';

class GeoCountries {

    id = null;
    parent_id = null;
    name = null;
    coverImg = null;
    top=0;
    postCode=null;

    constructor(obj) {
        if(obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.parent_id = obj.parent_id;
        this.name = obj.name;
        this.coverImg = obj.coverImg;
        this.top=obj.top;
        this.postCode = obj.postCode;

    }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

    static initRandom() {

    }
}

export default GeoCountries;