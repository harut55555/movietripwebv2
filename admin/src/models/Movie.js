import ImgService from '../services/ImgService';

class Movie {

    id = null;
    user_id = null;
    user_name = null;
    genres = null;
    name = null;
    year = null;
    coverImg = null;
    countries = null;
    directors = null;
    actors = null;
    description = null;
    tags = null;
    review = null;
    actorlist = null;
    directorlist = null;
    status = null;
    date = null;
    count = null;
    ownerId = null;
    approved = null;


    constructor(obj) {
        if (obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.user_id = obj.user_id;
        this.user_name = obj.user_name;
        this.genres = obj.genres;
        this.name = obj.name;
        this.year = obj.year;
        this.coverImg = obj.coverImg;
        this.countries = obj.countries;
        this.directors = obj.directors;
        this.actors = obj.actor;
        this.description = obj.description;
        this.tags = obj.tags;
        this.review = obj.review;
        this.actorlist = obj.actorlist;
        this.directorlist = obj.directorlist;
        this.status = obj.status;
        this.date = obj.date;
        this.count = obj.count;
        this.ownerId = obj.ownerId;
        this.approved = obj.approved;
    }

    // init object with image background loading
    initImageLoad(obj, ref, callback) {
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function () {
            callback(true, ref, self);
        };
        bgImg.onerror = function () {
            callback(false, ref, self);
        };
    }

    static initRandom() {

    }
}

export default Movie;