class Slider {

  id = null;
  user_id = null;
  user_name = null;
  title = null;
  cover = null;
  link = null;
  status = null;
  date = null;

  constructor() {

  }

  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.title = obj.title;
      this.cover = obj.cover;
      this.link = obj.link;
      this.status = obj.status;
      this.date = obj.date;
  }

  static initRandom() {

  }
}

export default Slider;