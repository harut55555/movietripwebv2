import ImgService from '../services/ImgService';

class Trip {

    id = null;
    user_id = null;
    user_name = null;
    title = null;
    description = null;
    coverImg = null;
    editorsChoice = null;
    status = null;
    date = null;
    tags = null;
    trip_point = null;
    trips = null;
    count = null;


    constructor(obj) {
        if (obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.user_id = obj.user_id;
        this.user_name = obj.user_name;
        this.title = obj.title;
        this.description = obj.description;
        this.coverImg = obj.coverImg;
        this.editorsChoice = obj.editorsChoice;
        this.tags = obj.tags;
        this.trip_point = obj.trip_point;
        this.trips = obj.trips;
        this.status = obj.status;
        this.date = obj.date;
        this.count = obj.count;
    }

    // init object with image background loading
    initImageLoad(obj, ref, callback) {
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function () {
            callback(true, ref, self);
        };
        bgImg.onerror = function () {
            callback(false, ref, self);
        };
    }

    static initRandom() {
    }
}

export default Trip;