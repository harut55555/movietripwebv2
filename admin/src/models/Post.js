/**
 * Created by Arsen on 15.03.2017.
 */
import ImgService from '../services/ImgService';

class Post {

    id = null;
    termID = null;
    user_ID = null;
    title = null;
    content = null;
    lang = null;
    translate = null;
    author = null;
    commentStatus = null;
    commentCount = null;
    alias = null;
    brief = null;
    takeAway = null;
    lastContent = null;
    custom = null;
    coverImg = null;
    comment = null;
    comments = null;
    top = null;
    date = null;
    tags = null;
    viewCount = null;
    like = null;
    disLike = null;
    hook = null;
    settings = null;
    postType = null;
    posts = null;
    bind = null;
    count = null;
    feed = null;
    status = null;


    constructor(obj) {
        if (obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.termID = obj.termID;
        this.user_ID = obj.user_id;
        this.title = obj.title;
        this.content = obj.content;
        this.lang = obj.lang;
        this.translate = obj.translate;
        this.author = obj.author;
        this.commentStatus = obj.commentStatus;
        this.commentCount = obj.commentCount;
        this.alias = obj.alias;
        this.brief = obj.brief;
        this.takeAway = obj.takeAway;
        this.lastContent = obj.lastContent;
        this.custom = obj.custom;
        this.coverImg = obj.coverImg;
        this.comment = obj.comment;
        this.comments = obj.comments;
        this.top = obj.top;
        this.date = obj.date;
        this.tags = obj.tags;
        this.viewCount = obj.viewCount;
        this.like = obj.like;
        this.disLike = obj.disLike;
        this.hook = obj.hook;
        this.settings = obj.settings;
        this.postType = obj.postType;
        this.posts = obj.posts;
        this.bind = obj.bind;
        this.count = obj.count;
        this.feed = obj.feed;
        this.status = obj.status;

    }

    // init object with image background loading
    initImageLoad(obj, ref, callback) {
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));
        bgImg.onload = function () {
            callback(true, ref, self);
        };
        bgImg.onerror = function () {
            callback(false, ref, self);
        };
    }

    static initRandom() {

    }
}

export default Post;