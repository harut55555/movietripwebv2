import ImgService from '../services/ImgService';

class Scene {

    id = null;
    user_id = null;
    user_name = null;
    coverImg = null;
    location = null;
    movie = null;
    brief = null;
    copyright = null;
    check_in_text = null;
    editors_choice = null;
    tags = null;
    status = null;
    date = null;
    scenes = null;
    been = null;
    count = null;
    address = null;

    
  constructor(obj) {
	  if(obj !== null && typeof obj !== "undefined") {
		  this.init(obj);
	  }
  }

  init(obj) {
      this.id = obj.id;
      this.user_id = obj.user_id;
      this.user_name = obj.user_name;
      this.coverImg = obj.coverImg;
      this.location = obj.location;
      this.movie = obj.movie;
      this.brief = obj.brief;
      this.copyright = obj.copyright;
      this.check_in_text = obj.check_in_text;
      this.editors_choice = obj.editors_choice;
      this.tags = obj.tags;
      this.status = obj.status;
      this.date = obj.date;
      this.scenes = obj.scenes;
      this.been = obj.been;
      this.count = obj.count;
      this.address = obj.address;
    
  }

    // init object with image background loading
    initImageLoad(obj, ref, callback){
        this.init(obj);

        let self = this;
        let bgImg = new Image();
        bgImg.src = ImgService.getLink((typeof this.coverImg !== undefined ? this.coverImg : null));

        bgImg.onload = function(){
            callback(true, ref, self);
        };
        bgImg.onerror = function(){
            callback(false, ref, self);
        };
    }

  static initRandom() {

  }
}

export default Scene;