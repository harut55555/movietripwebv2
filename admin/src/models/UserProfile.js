class UserProfile {

    email = null;
    firstName = null;
    lastName = null;
    site = null;
    biography = null;
    avatar = null;
    country = null;
    city = null;
    birthDate = null;
    phoneNumber = null;
    address = null;
    fbProfile = null;
    googleProfile = null;
    user_Id = null;
    processingErrorId = null;

    constructor(obj) {
        if (obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.email = obj.email;
        this.firstName = obj.firstName;
        this.lastName = obj.lastName;
        this.site = obj.site;
        this.biography = obj.biography;
        this.avatar = obj.avatar;
        this.country = obj.country;
        this.city = obj.city;
        this.birthDate = obj.birthDate;
        this.phoneNumber = obj.phoneNumber;
        this.address = obj.address;
        this.fbProfile = obj.fbProfile;
        this.googleProfile = obj.googleProfile;
        this.user_Id = obj.user_Id;
        this.processingErrorId = obj.processingErrorId;
    }

    static initRandom() {

    }
}

export default UserProfile;