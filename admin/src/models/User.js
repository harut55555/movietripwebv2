class User {

    id = null;
    userName = null;
    email = null;
    creationDate = null;
    lastLogin = null;
    faceBookId = null;
    googleId = null;
    lastLogout = null;
    userActivation = null;
    passwordChangeDate = null;
    userRole = null;
    favoriteGenres = null;
    users = null;
    count = null;
    status = null;
    processingErrorId = null;


    constructor(obj) {
        if (obj !== null && typeof obj !== "undefined") {
            this.init(obj);
        }
    }

    init(obj) {
        this.id = obj.id;
        this.userName = obj.userName;
        this.email = obj.email;
        this.creationDate = obj.date;
        this.lastLogin = obj.lastLogin;
        this.faceBookId = obj.faceBookId;
        this.googleId = obj.googleId;
        this.lastLogout = obj.lastLogout;
        this.userActivation = obj.userActivation;
        this.passwordChangeDate = obj.passwordChangeDate;
        this.userRole = obj.userRole;
        this.processingErrorId = obj.processingErrorId;
        this.favoriteGenres = obj.favoriteGenres;
        this.users = obj.users;
        this.status = obj.status;
        this.lastLogout = obj.lastLogout;
        this.count = obj.count;
        this.processingErrorId = obj.processingErrorId;
    }

    static initRandom() {

    }
}

export default User;